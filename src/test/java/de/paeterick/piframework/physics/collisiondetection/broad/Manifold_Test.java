package de.paeterick.piframework.physics.collisiondetection.broad;

import org.junit.Test;

import de.paeterick.piframework.math.Vector2;
import static org.junit.Assert.*;

public class Manifold_Test {

	@Test
	public void positionalCorrectionTest() {

		final float PENETRATION_ALLOWANCE = 0.05f;
		final float PENETRATION_CORRETION  = 0.4f;
		
		final float penetration = 0.38363647f;
		final float aInvMass = 0.0f;
		final float bInvMass = 8.92702E-4f;
		
		final Vector2 aPos = new Vector2(240.0f, 300.0f);
		final Vector2 bPos = new Vector2(367.99792f, 279.0686f);
		final Vector2 normal = new Vector2(-0.0f, -0.99999994f);
		
		
		
		final float correction = 
				Math.max(penetration - PENETRATION_ALLOWANCE, 0.0f) /
				(aInvMass + bInvMass) * PENETRATION_CORRETION;
		
		if (correction != 149.4951f) {
			fail("correction has the wrong value!");
		}
		
		aPos.add(normal.addNew(-aInvMass * correction)); // mul = 0
		bPos.add(normal.addNew(bInvMass * correction));  // mul = 0.13345458
		
		assertTrue((aPos.x == 240.0f && aPos.y == 300.0f) && (bPos.x == 367.99792f && bPos.y == 278.93515));
	}
	
}
