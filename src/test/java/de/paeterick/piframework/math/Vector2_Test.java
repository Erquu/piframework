package de.paeterick.piframework.math;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Vector2_Test {
	
	private Vector2 v1; 
	
	@Before
	public void init() {
		v1 = Vector2.emptyVector();
	}
	
	
	@Test
	public void emptyTest() {
		assertTrue(v1.x == 0 && v1.y == 0);
	}
	
	@Test
	public void lengthTest() {
		v1.x = 10.0f;
		v1.y = 15.0f;
		
		assertTrue(MathHelper.equals(v1.length(), 18.027756f));
	}
	
	@Test
	public void lengthSqTest() {
		v1.x = 10.0f;
		v1.y = 15.0f;
		
		assertTrue(MathHelper.equals(v1.lengthSq(), 325.0f));
	}
	
	@Test
	public void cloneTest() {
		final Vector2 v2 = v1.clone();
		assertFalse(v1 == v2);
	}
	
	@Test
	public void addTest() {
		v1.add(10.5f);
		assertTrue((v1.x == 10.5f && v1.y == 10.5f));
	}
	@Test
	public void add_vTest() {
		v1.x += 1.23f;
		v1.y += 4.56f;
		
		final Vector2 toAdd = new Vector2(10.0f, 20.0f);
		v1.add(toAdd);
		
		assertTrue((v1.x == 11.23f && v1.y == 24.56f));
	}
	
	@Test
	public void addNewTest() {
		final float diff = 10.1234f;
		final Vector2 v2 = v1.addNew(diff);
		assertTrue((v1.x == 0.0f && v1.y == 0.0f) && (v2.x == diff && v2.y == diff) && v1 != v2);
	}
	@Test
	public void addNew_vTest() {
		v1.x += 1.23f;
		v1.y += 4.56f;
		
		final Vector2 toAdd = new Vector2(10.0f, 20.0f);
		final Vector2 v3 = v1.addNew(toAdd);
		
		assertTrue((v1.x == 1.23f && v1.y == 4.56f) && (v3.x == 11.23f && v3.y == 24.56f) && v3 != v1);
	}
	
	@Test
	public void subtractTest() {
		final Vector2 toSubtract = new Vector2(10.0f, 20.0f);
		v1.subtract(toSubtract);
		
		assertTrue((v1.x == -toSubtract.x && v1.y == -toSubtract.y));
	}
	
	@Test
	public void subtractNewTest() {
		final Vector2 toSubtract = new Vector2(10.0f, 20.0f);
		final Vector2 v2 = v1.subtractNew(toSubtract);
		
		assertTrue((v1.x == 0.0f && v1.y == 0.0f) && (v2.x == -toSubtract.x && v2.y == -toSubtract.y) && v1 != v2);
	}
	
	@Test
	public void multiplyTest() {
		v1.x = 10.0f;
		v1.y = 20.0f;
		
		v1.multiply(.5f);
		
		assertTrue((v1.x == 5.0f && v1.y == 10.0f));
	}
	
	@Test
	public void multiplyNewTest() {
		v1.x = 10.0f;
		v1.y = 20.0f;
		
		final Vector2 v2 = v1.multiplyNew(.5f);
		
		assertTrue((v1.x == 10.0f && v1.y == 20.0f) && (v2.x == 5.0f && v2.y == 10.0f) && v2 != v1);
	}
	
	@Test
	public void divideTest() {
		v1.x = 10.0f;
		v1.y = 20.0f;
		
		v1.divide(2.0f);
		
		assertTrue((v1.x == 5.0f && v1.y == 10.0f));
	}
	
	@Test
	public void divideNewTest() {
		v1.x = 10.0f;
		v1.y = 20.0f;
		
		final Vector2 v2 = v1.divideNew(2.0f);
		
		assertTrue((v1.x == 10.0f && v1.y == 20.0f) && (v2.x == 5.0f && v2.y == 10.0f) && v2 != v1);
	}
	
	@Test
	public void negateTest() {
		v1.x = 1.0f;
		v1.y = 1.0f;
		
		v1.negate();
		
		assertTrue((v1.x == -1.0f && v1.y == -1.0f));
	}
	
	@Test
	public void negateNewTest() {
		v1.x = 1.0f;
		v1.y = 1.0f;
		
		final Vector2 v2 = v1.negateNew();
		
		assertTrue((v1.x == 1.0f && v1.y == 1.0f) && (v2.x == -1.0f && v2.y == -1.0f) && v1 != v2);
	}
	
	@Test
	public void normalizeTest() {
		v1.x = 10.0f;
		v1.y = 15.0f;
		
		v1.normalize();
		
		assertTrue(MathHelper.equals(v1.x, 0.5547002f) && MathHelper.equals(v1.y, 0.8320503f));
	}
	
	@Test
	public void dotProductTest() {
		v1.x = 10.0f;
		v1.y = 15.0f;
		
		final Vector2 v2 = new Vector2(15.0f, 10.0f);
		final float dot = v1.dotProduct(v2);
		
		assertTrue(dot == 300.0f);
	}
	
	@Test
	public void crossProductTest() {
		v1.x = 10.0f;
		v1.y = 15.0f;
		
		final Vector2 v2 = new Vector2(15.0f, 10.0f);
		final float cross = v1.crossProduct(v2);
		
		assertTrue(cross == -125.0f);
	}
	
	@Test
	public void transformTest() {
		v1.x = 10.0f;
		v1.y = 10.0f;
		
		final Matrix2 m = new Matrix2(1.0f, 2.0f, 3.0f, 4.0f);
		
		v1.transform(m);
		
		assertTrue((v1.x == 30.0f) && (v1.y == 70.0f));
	}
	
	@Test
	public void randomAlgoTest() {
		// :)
		
		Vector2 aVelo = new Vector2(0.0f, 0.0f);
		Vector2 bVelo = new Vector2(0.0f, 40.41667f);
		
		float aAngVelo = 0.0f;
		float bAngVelo = 0.0f;
		
		Vector2 ra = new Vector2(172.02182f, -10.0f);
		Vector2 rb = new Vector2(27.02182f, 22.99997f);
		
		
		final Vector2 rv = bVelo.clone();
		rv.add(Vector2.cross(bAngVelo, rb));
		rv.subtract(aVelo);
		rv.subtract(Vector2.cross(aAngVelo, ra));
		
		assertTrue(rv.x == 0.0f && rv.y == 40.41667f);
	}
	
	@Test
	public void randomAlgoTest_2() {
		// :)
		
		Vector2 aVelo = new Vector2(0.0f, 0.0f);
		Vector2 bVelo = new Vector2(0.0f, 44.58335f);
		
		float aAngVelo = 0.0f;
		float bAngVelo = 0.0f;
		
		Vector2 ra = new Vector2(128.9437f, -10.0f);
		Vector2 rb = new Vector2(20.943695f, 25.493073f);
		
		
		final Vector2 rv = bVelo.clone();
		rv.add(Vector2.cross(bAngVelo, rb));
		rv.subtract(aVelo);
		rv.subtract(Vector2.cross(aAngVelo, ra));
		
		assertTrue(rv.x == 0.0f && rv.y == 44.58335f);
	}
}
