package test;

import java.awt.Graphics2D;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameFactory;
import de.paeterick.piframework.game.graphics.Scene;

public class ConsoleTestII extends Game {

	public ConsoleTestII() {
		
	}

	@Override
	public void init() {
		getEngine().enableSynchronizedRendering(true);
		getEngine().setResizable(true);
	}

	@Override
	public void load() {
		getEngine().getConsole().addMessage("[^7] Loaded Example \"ConsoleTestII\"");
	}

	public void update(final long deltaTime, final long totalTime) { }

	public void render(Graphics2D context, final Scene scene) { }

	@Override
	public void unload() { }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GameFactory.start(new ConsoleTestII());
	}
}
