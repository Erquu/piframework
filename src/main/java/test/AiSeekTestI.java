package test;

import java.awt.Color;
import java.awt.Graphics2D;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameFactory;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;

public class AiSeekTestI extends Game {
	
	public static void main(String[] args) {
		GameFactory.start(new AiSeekTestI());
	}
	
	Vector2 pos = Vector2.emptyVector();
	
	Vector2 enemy = Vector2.emptyVector();
	Vector2 velo = Vector2.emptyVector();
	
	float maxSpeed = 2.0f;
	Vector2 maxForce = new Vector2(3.5f, 3.5f);
	float mass = 30.0f;

	public void update(long deltaTime, long totalTime) {
		pos = this.getMouseState().getPosition();
		
		Vector2 desVelo;
		if (MathHelper.distance(enemy, pos) < 50) {
			desVelo = enemy.subtractNew(pos);
		} else {
			desVelo = pos.subtractNew(enemy);
		}
		desVelo.normalize();
		desVelo.multiply(maxSpeed);
		
		Vector2 steering = desVelo.subtract(velo);
		this.getEngine().addDebugString("Steering: %s", steering);
		steering.x = MathHelper.clamp(steering.x, -maxForce.x, maxForce.x);
		steering.y = MathHelper.clamp(steering.y, -maxForce.y, maxForce.y);
		
		steering.divide(mass);
		
		velo.x = Math.min(velo.x + steering.x, maxSpeed);
		velo.y = Math.min(velo.y + steering.y, maxSpeed);
		
		enemy.add(velo);
	}

	public void render(Graphics2D context, final Scene scene) {
		context.setColor(Color.red);
		context.fillRect((int)enemy.x - 4, (int)enemy.y - 4, 8, 8);
		context.setColor(Color.green);
		context.fillRect((int)pos.x - 4, (int)pos.y - 4, 8, 8);
	}

	@Override
	public void init() {
		getEngine().setGameName("AiSeekTestI");
	}

	@Override
	public void load() {
		enemy.x = (float)this.getClientRect().width * 0.5f;
		enemy.y = (float)this.getClientRect().height * 0.5f;
	}

	@Override
	public void unload() {
		
	}
}
