package test;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameEngine;
import de.paeterick.piframework.game.exceptions.EngineAlreadyInitializedException;
import de.paeterick.piframework.game.exceptions.EngineAlreadyStartedException;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.math.Matrix4;
import de.paeterick.piframework.math.Vector3;

public class TestEntryPoint extends Game {
	
	Vector3[] vectorList;
	Matrix4 rotate;
	Matrix4 translate;
	Color[] colorList;
	
	float[] vectorStartLength;
	float highestStartPoint = 0.0f;
	List<float[]> lengthValues;
	int addFrame = 0;

	final int addEveryXFrame = 1;
	final boolean perspective = false;
	final boolean useCenteredVectors = false;
	final float chartLineGap = 0.51f;
	final float chartsGap = 0.0f;
	final int maxEntries = 1700;
	final float maxEntriesFloat = (float)maxEntries;
	
	float rotation = 0.301f;
	
	
	public void update(final long deltaTime, final long totalTime) {
		float[] lengthValue = new float[vectorList.length];
		
		createVectors();
		rotate = Matrix4.identityMatrix();
		
		rotate.rotateX(rotation);
		//rotate.rotateY(rotation);
		//rotate.rotateZ(rotation);
		//rotate.rotate(rotation, new Vector3(0.0f, 1.0f, 0.0f));
		
		//rotate.rotateXYZ(rotation, rotation, rotation);
		rotation += 0.007f;
		
		for (int i = 0; i < vectorList.length; i++) {
			//getEngine().addDebugString("   - Length: %s", vectorList[i].length());
			
			vectorList[i].transformNormal(rotate);
			
			lengthValue[i] = vectorList[i].magnitude();
			getEngine().addDebugString("vectorList[%s].z = %s", i, vectorList[i].z);
			
			vectorList[i].translateNormal(translate);
		}
		if (++addFrame >= addEveryXFrame) {
			lengthValues.add(lengthValue);
			addFrame = 0;
		}
	}

	public void render(Graphics2D context, final Scene scene) {
		
		for (int i = 0; i < vectorList.length; i++) {
			context.setColor(colorList[i]);
			Vector3 v = vectorList[i];
			context.drawRect((int)(v.x - 1.5f), (int)(v.y - 1.5f), 3, 3);
			if (vectorList.length > 1) {
			int prev = i - 1;
			if (prev == -1)
				prev = vectorList.length - 1;
			context.setColor(Color.white);
			context.drawLine(
				(int)(v.x),
				(int)(v.y),
				(int)(vectorList[prev].x),
				(int)(vectorList[prev].y));
			}
		}
		
		//float centerX = ((float)getEngine().getClientRect().width / 2.0f);
		float centerX = (float)getEngine().getClientRect().width - 70.0f;
		//float centerY = ((float)getEngine().getClientRect().height / 2.0f);
		float centerY = (float)getEngine().getClientRect().height - 100.0f;
		
		// Draw baseline
		context.setColor(Color.white);
		context.drawLine(
			(int)(centerX - maxEntriesFloat * chartLineGap - chartsGap),
			(int)centerY,
			(int)centerX,
			(int)centerY);
		context.drawLine(
			(int)(centerX - maxEntriesFloat * chartLineGap - chartsGap),
			(int)(centerY - highestStartPoint),
			(int)(centerX - maxEntriesFloat * chartLineGap - chartsGap),
			(int)centerY);
		
		for (int i = 1, l = Math.min(lengthValues.size(), maxEntries); i < l; i++) {
			float[] prevLengthValue = lengthValues.get(lengthValues.size() - i - 1);
			float[] lengthValue = lengthValues.get(lengthValues.size() - i);
						
			for (int j = 0; j < vectorList.length; j++) {
				context.setColor(colorList[j]);
				context.drawLine(
					(int)(centerX - i * chartLineGap - chartLineGap),
					(int)(centerY - prevLengthValue[j] + j * chartsGap),
					(int)(centerX - i * chartLineGap),
					(int)(centerY - lengthValue[j] + j * chartsGap));
				
				if (i == 1) {
					float m = lengthValue[j];
					context.drawString(Float.toString(m), centerX, centerY - m + i * chartsGap + 2.5f);
				}
			}
		}		
	}

	@Override
	public void init() {

		getEngine().showDebugOuput(true);
		getEngine().enableSynchronizedRendering(true);
		
		if (perspective)
			rotate = Matrix4.perspectiveMatrix(1.0f, 100.0f);
		else
			rotate = Matrix4.identityMatrix();
		
		lengthValues = new Vector<float[]>();
		
		createColors();
		createVectors();

		translate = Matrix4.identityMatrix();
		translate.translate(new Vector3(300, 300, 0));
		for (int i = 0; i < vectorList.length; i++) {
			vectorList[i].translateNormal(translate);
		}
	}
	
	private void createVectors() {
		if (useCenteredVectors) {
			vectorList = new Vector3[] {
				new Vector3(-100, 100, 0),
				new Vector3(-100, -100, 0),
				new Vector3(100, -100, 0),
				new Vector3(100, 100, 0)
			};
		} else {
			vectorList = new Vector3[] {
				new Vector3(100, 100, 0),
				new Vector3(0, 100, 0),
				new Vector3(0, 0, 0),
				new Vector3(100, 0, 0)
			};
		}
		
		vectorStartLength = new float[vectorList.length];
		for (int i = 0; i < vectorList.length; i++) {
			vectorStartLength[i] = vectorList[i].magnitude();
			if (vectorStartLength[i] > highestStartPoint) {
				highestStartPoint = vectorStartLength[i];
			}
		}
	}
	
	private void createColors() {
		colorList = new Color[4];
		colorList[0] = Color.red;
		colorList[1] = Color.blue;
		colorList[2] = Color.green;
		colorList[3] = Color.pink;
	}

	@Override
	public void load() {
		getEngine().setGameName("Test");
	}

	@Override
	public void unload() {
		
	}
	
	public static void main(String[] args) throws EngineAlreadyStartedException, EngineAlreadyInitializedException {
		GameEngine engine = new GameEngine();
        engine.init(new TestEntryPoint());
        engine.start();
	}
}
