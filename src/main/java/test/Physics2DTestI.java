package test;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameFactory;
import de.paeterick.piframework.game.console.ConsoleActionArguments;
import de.paeterick.piframework.game.console.IConsoleAction;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.hid.Mouse;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.Physics2;
import de.paeterick.piframework.physics.collisiondetection.AABB;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;
import de.paeterick.piframework.physics.collisiondetection.broad.quadtree.RenderingDynamicQuadTree;
import de.paeterick.piframework.physics.shapes.Polygon;

public class Physics2DTestI extends Game implements IConsoleAction {

	private RenderingDynamicQuadTree tree;
	private Physics2 physics;

	private boolean createObject = false;
	private final Vector2 start = Vector2.emptyVector();
	private final Vector2 end = Vector2.emptyVector();
	private final Vector2 mouse = Vector2.emptyVector();

	@Override
	public void init() {
		getEngine().enableSynchronizedRendering(true);
	}

	@Override
	public void load() {
		getEngine().setGameName("Physics2DTestI");

		tree = RenderingDynamicQuadTree.createInstance(getClientRect());
		physics = new Physics2(1, tree);
		Physics2.GRAVITY.y = 15f;

		Body b;

		b = physics.add(new Polygon(200, 10), 400, 500);
		b.setStatic();
		b.setOrientation(0);
	}

	public void update(final long deltaTime, final long totalTime) {

		mouse.x = getMouseState().getX();
		mouse.y = getMouseState().getY();
		
		if (getMouseState().isPressed(Mouse.LEFT)) {
			if (!createObject) {
				createObject = true;
				start.x = mouse.x;
				start.y = mouse.y;
			} else {
				end.x = mouse.x;
				end.y = mouse.y;	
			}
		} else {
			if (createObject) {
				createObject = false;
				System.out.printf("w:%s h:%s x:%s y:%s", end.x - start.x, end.y - start.y, mouse.x, mouse.y);
				System.out.println();
				
				final Body b = physics.add(new Polygon(end.x - start.x, end.y - start.y), (int)mouse.x, (int)mouse.y);
				b.setOrientation(0);
			}
		}
		
		physics.update(deltaTime, totalTime);
	}

	public void render(Graphics2D context, final Scene scene) {
		//tree.render(context, scene);
		
		if (physics.lastManifolds != null) {
			final Manifold[] ms = physics.lastManifolds;
			for (final Manifold m : ms) {
				context.setColor(Color.red);
				context.drawLine(
					(int)m.a.position.x,
					(int)m.a.position.y,
					(int)m.b.position.x,
					(int)m.b.position.y
				);
				for (int i = 0; i < m.contactCount; i++) {
					final Vector2 contact = m.contacts[i];
					context.setColor(Color.yellow);
					context.drawLine(
						(int)contact.x,
						(int)contact.y - 2,
						(int)contact.x,
						(int)contact.y + 2);
				}
			}
		}

		context.setColor(Color.white);
		if (createObject) {
			context.drawRect((int)(start.x), (int)(start.y), (int)(end.x - start.x), (int)(end.y - start.y));
		}
		context.drawRect((int)(mouse.x) - 5, (int)(mouse.y) - 5, 9, 9);

		for (final Body b : physics.bodies) {
			if (b.shape instanceof Polygon) {
				final Polygon p = (Polygon) b.shape;

				Path2D.Float path = new Path2D.Float();
				for (int i = 0; i < p.vertexCount; i++) {
					final Vector2 v = p.vertices[i].clone();
					v.transform(b.shape.u);
					v.add(b.position);

					if (i == 0) {
						path.moveTo(v.x, v.y);
					} else {
						path.lineTo(v.x, v.y);
					}
				}
				path.closePath();
				context.setColor(Color.white);
				context.draw(path);
				
				final AABB aabb = b.computeAABB();
				context.setColor(Color.red);
				context.drawRect(
					(int)aabb.getLeft(),
					(int)aabb.getTop(),
					(int)(aabb.getHalfDimension().width * 2.0f),
					(int)(aabb.getHalfDimension().height * 2.0f));
			}
		}
	}

	@Override
	public void unload() {
	}

	public static void main(String[] args) {
		GameFactory.start(new Physics2DTestI());
	}

	public void doAction(final ConsoleActionArguments args) throws Exception {

	}
}
