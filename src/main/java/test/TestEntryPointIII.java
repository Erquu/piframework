package test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameCanvas;
import de.paeterick.piframework.game.GameEngine;
import de.paeterick.piframework.game.PresentParameters;
import de.paeterick.piframework.game.exceptions.EngineAlreadyInitializedException;
import de.paeterick.piframework.game.exceptions.EngineAlreadyStartedException;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.hid.Mouse;
import de.paeterick.piframework.math.LineSegment2;

public class TestEntryPointIII extends Game {
	
	private static GameEngine engine;
	
	public static void main(String[] args) {
		final GameCanvas canvas = new GameCanvas();
		final JButton btn = new JButton("Start");
		final PresentParameters p = new PresentParameters();
		p.setRenderingTarget(canvas);
		
		engine = new GameEngine(p);
		btn.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				try {
					engine.init(new TestEntryPointIII());
					engine.start();
				} catch (EngineAlreadyInitializedException e) {
					e.printStackTrace();
				} catch (EngineAlreadyStartedException e) {
					e.printStackTrace();
				}
			}
		});
		
		final JFrame frm = new JFrame();
		frm.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("Closing");
				if (engine != null)
					engine.stop();
			}
		});
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JPanel pane = new JPanel(new BorderLayout());
		pane.setPreferredSize(new Dimension(400, 400));
		pane.add(btn, BorderLayout.PAGE_START);
		pane.add(canvas, BorderLayout.CENTER);
		
		frm.getContentPane().add(pane);
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frm.pack();
				frm.setVisible(true);
			}
		});
	}
	
	//
	// RENDERING STUFF
	//
	LineSegment2 lineA = new LineSegment2();
	LineSegment2 lineB = new LineSegment2(100, 300, 300, 100);

	public void update(final long deltaTime, final long totalTime) {
		if (getMouseState().isPressed(Mouse.RIGHT)) {
			lineA.x1 = getMouseState().getX();
			lineA.y1 = getMouseState().getY();
		} else {
			lineA.x2 = getMouseState().getX();
			lineA.y2 = getMouseState().getY();
		}
	}

	public void render(Graphics2D context, final Scene scene) {
		context.setColor(Color.WHITE);
		context.drawLine((int)lineA.x1, (int)lineA.y1, (int)lineA.x2, (int)lineA.y2);
		
		//final Vector2 p = lineA.intersectPoint(lineB);
		if (lineA.intersects(lineB)) {
			context.setColor(Color.RED);
		}
		context.drawLine((int)lineB.x1, (int)lineB.y1, (int)lineB.x2, (int)lineB.y2);
		//context.drawOval((int)p.x - 2, (int)p.y - 2, 4, 4);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

}
