package test;

import java.awt.Graphics2D;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameFactory;
import de.paeterick.piframework.game.graphics.Scene;

public class JavaScriptTest extends Game {
	
	public static void main(String[] args) {
		GameFactory.start(new JavaScriptTest());
	}

	@Override
	public void init() {
		
	}

	@Override
	public void load() {
	}

	public void update(long deltaTime, long totalTime) {
		
	}

	public void render(Graphics2D context, Scene scene) {
		
	}

	@Override
	public void unload() {
		
	}

}
