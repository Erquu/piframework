package test;

import javax.xml.stream.XMLStreamException;

import piresources.PiResourceLoader;
import de.paeterick.piframework.game.console.parameters.ConsoleParameterTree;
import de.paeterick.piframework.game.console.parameters.ConsoleParameterTreeFactory;

@SuppressWarnings("restriction")
public class ConsoleTestI {
	public static void main(String[] args) throws XMLStreamException {
		PiResourceLoader.instance.setFile("console.xml");
		
		ConsoleParameterTree ct = ConsoleParameterTreeFactory.fromXml(PiResourceLoader.instance.getResourceAsStream());
		System.out.println("Suggestion: " + ct.getSuggestion("en d"));
		for (String s : ct.getHints("en debug ")) {
			System.out.println("Hint: " + s);
		}
	}
}
