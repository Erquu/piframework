package piresources;

import de.paeterick.piframework.resources.SWrapper;

public class PiResourceLoader extends SWrapper {
	
	public static final PiResourceLoader instance = new PiResourceLoader("piresources/");
	
	private String rootFolder = "";
	
	private PiResourceLoader() {
		
	}
	private PiResourceLoader(String rootFolder) {
		this.rootFolder = rootFolder;
	}
	
	@Override
	public void setFile(String path) {
		super.setFile(rootFolder.concat(path));
	}
}
