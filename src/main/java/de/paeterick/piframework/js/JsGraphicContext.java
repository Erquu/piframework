package de.paeterick.piframework.js;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;

import org.mozilla.javascript.Scriptable;

public class JsGraphicContext {
	private Graphics2D fGraphicsContext;
	
	public void updateGraphicsContext(final Graphics2D context) {
		this.fGraphicsContext = context;
	}
	
	
	public void clearRect(int x, int y, int width, int height) {
		this.fGraphicsContext.clearRect(x, y, width, height);
	}
	
	public void clipRect(int x, int y, int width, int height) {
		this.fGraphicsContext.clipRect(x, y, width, height);
	}
	
	public void copyArea(int x, int y, int width, int height, int dx, int dy) {
		this.fGraphicsContext.copyArea(x, y, width, height, dx, dy);
	}
	
	public void draw3DRect(int x, int y, int width, int height, boolean raised) {
		this.fGraphicsContext.draw3DRect(x, y, width, height, raised);
	}
	
	public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
		this.fGraphicsContext.drawArc(x, y, width, height, startAngle, arcAngle);
	}
	
	public void drawBytes(byte[] data, int offset, int length, int x, int y) {
		this.fGraphicsContext.drawBytes(data, offset, length, x, y);
	}
	
	public void drawChars(char[] data, int offset, int length, int x, int y) {
		this.fGraphicsContext.drawChars(data, offset, length, x, y);
	}
	
	public void drawImage(Image img, int x, int y, ImageObserver observer) {
		this.fGraphicsContext.drawImage(img, x, y, observer);
	}
	public void drawImage(Image img, int x, int y, Color bgcolor, ImageObserver observer) {
		this.fGraphicsContext.drawImage(img, x, y, bgcolor, observer);
	}
	public void drawImage(Image img, int x, int y, int width, int height, ImageObserver observer) {
		this.fGraphicsContext.drawImage(img, x, y, width, height, observer);
	}
	public void drawImage(Image img, int x, int y, int width, int height, Color bgcolor, ImageObserver observer) {
		this.fGraphicsContext.drawImage(img, x, y, width, height, bgcolor, observer);
	}
	public void drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2, ImageObserver observer) {
		this.fGraphicsContext.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, observer);
	}
	public void drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2, Color bgcolor, ImageObserver observer) {
		this.fGraphicsContext.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, bgcolor, observer);
	}
	
	public void drawLine(int x1, int y1, int x2, int y2) {
		this.fGraphicsContext.drawLine(x1, y1, x2, y2);
	}
	
	public void drawOval(int x, int y, int width, int height) {
		this.fGraphicsContext.drawOval(x, y, width, height);
	}
	
	public void drawPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		this.fGraphicsContext.drawPolygon(xPoints, yPoints, nPoints);
	}
	
	public void drawPolyline(int[] xPoints, int[] yPoints, int nPoints) {
		this.fGraphicsContext.drawPolyline(xPoints, yPoints, nPoints);
	}
	
	public void drawRect(int x, int y, int width, int height) {
		this.fGraphicsContext.drawRect(x, y, width, height);
	}
	
	public void drawRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
		this.fGraphicsContext.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
	}
	
	public void drawString(String str, int x, int y) {
		this.fGraphicsContext.drawString(str, x, y);
	}
	
	public void drawString(String str, float x, float y) {
		this.fGraphicsContext.drawString(str, x, y);
	}
	
	public void fill3DRect(int x, int y, int width, int height, boolean raised) {
		this.fGraphicsContext.fill3DRect(x, y, width, height, raised);
	}
	
	public void fillArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
		this.fGraphicsContext.fillArc(x, y, width, height, startAngle, arcAngle);
	}
	
	public void fillOval(int x, int y, int width, int height) {
		this.fGraphicsContext.fillOval(x, y, width, height);
	}
	
	public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		this.fGraphicsContext.fillPolygon(xPoints, yPoints, nPoints);
	}
	
	public void fillRect(int x, int y, int width, int height) {
		this.fGraphicsContext.fillRect(x, y, width, height);
	}
	
	public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
		this.fGraphicsContext.fillRoundRect(x, y, width, height, arcWidth, arcHeight);
	}
	
	public void setColor(Color c) {
		this.fGraphicsContext.setColor(c);
	}


}
