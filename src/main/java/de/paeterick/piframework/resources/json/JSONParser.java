package de.paeterick.piframework.resources.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class JSONParser {
    /// <summary>The document to be parsed</summary>
    private String doc;
    /// <summary>The length of the document</summary>
    private int length;
    /// <summary>The index pointer to the current char in the document</summary>
    private int index = 0;

    /// <summary>Parses a json document</summary>
    /// <param name="stream">Document input stream to read from</param>
    /// <returns>A JSONObject representing the root node</returns>
    public JSONObject parse(final InputStream stream) throws IOException
    {
	    final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
	    final StringBuilder builder = new StringBuilder();
	    String line;
		while ((line = reader.readLine()) != null) {
		    builder.append(line);
		}
	    return parse(builder.toString());
    }
    /// <summary>Parses a json document</summary>
    /// <param name="doc">The document to be parsed</param>
    /// <returns>A JSONObject representing the root node</returns>
    public JSONObject parse(final String doc)
    {
	    return parse(doc, null, false);
    }
    /// <summary>
    /// Parses a json document
    /// </summary>
    /// <param name="doc">The json document to be parsed</param>
    /// <param name="parent">The parent node for the found child-node</param>
    /// <param name="isArray">Should the following node be parsed as an array</param>
    /// <returns>The create JSONObject representing the node</returns>
    private JSONObject parse(final String doc, final JSONObject parent, final boolean isArray) {
	    this.doc = doc;
	    this.length = doc.length();
	
	    JSONObject currentObj = isArray ? (new JSONArray()) : (new JSONObject());
	    currentObj.parent = parent;
	    
	    boolean isKey = true;
	    String lastValueKey = null;
	
	    while (index < length) {
		    final char current = doc.charAt(++index);
		    switch (current) {
			    case '/':
				    if (doc.charAt(index) == '/') {
					    lineEnd();
				    } else if (doc.charAt(index) == '*') {
					    commentEnd();
				    }
				    break;
			    case '\'':
			    case '"':
				    final String nodeValue = stringEnd(current);
				    if (isArray) {
					    ((JSONArray)currentObj).add(new JSONTextNode(nodeValue));
				    } else {
					    if (isKey) {
						    lastValueKey = nodeValue;
					    } else {
						    currentObj.add(lastValueKey, new JSONTextNode(nodeValue));
					    }
				    }
				    break;
			    case ':':
				    isKey = !isKey;
				    break;
			    case ',':
				    isKey = true;
				    lastValueKey = null;
				    break;
			    case '[':
                    JSONObject value = parse(doc, currentObj, true);
                    if (lastValueKey == null)
                        currentObj = value;
                    else
                        currentObj.add(lastValueKey, value);
				    break;
			    case '{':
				    JSONObject obj = parse(doc, currentObj, false);
				    if (isArray) {
					    ((JSONArray)currentObj).add(obj);
				    } else {
                        if (lastValueKey == null)
                            return obj;
					    currentObj.add(lastValueKey, obj);
				    }
				    break;
			    case ']':
			    case '}':
				    return currentObj;
			    default:
				    if (!Character.isWhitespace(current)) {
                        if (isArray)
                        {
                            ((JSONArray)currentObj).add(new JSONLiteralNode(literalEnd()));
                        }
                        else
                        {
                            if (!isKey)
                            {
                                if (currentObj.containsKey(lastValueKey))
                                {
                                    currentObj.remove(lastValueKey);
                                }
                                currentObj.add(lastValueKey, new JSONLiteralNode(literalEnd()));
                            }
                        }
				    }
				    break;
		    }
	    }
	
	    return currentObj;
    }

    /// <summary>
    /// Goes to the end of a literal and returns it as a string
    /// Literals can be true, false, null or numbers and decimals like 1, 123, 3.1314
    /// </summary>
    /// <returns>The literal as a string</returns>
    private String literalEnd() {
	    final StringBuilder builder = new StringBuilder();
	    index--;
	    while (index < length) {
		    final char currentChar = doc.charAt(index);
		    if (!isLiteralChar(currentChar))
			    break;
		    builder.append(currentChar);
		    index++;
	    }
	    return builder.toString();
    }

    /// <summary>Checks if a character is a valid literal json character</summary>
    /// <param name="c">The character to be checked</param>
    /// <returns>true if the character is valid</returns>
    private boolean isLiteralChar(char c)
    {
        final char[] validChars = new char[] { '+', '-', '.' };
        return (Character.isLetterOrDigit(c) || Arrays.binarySearch(validChars, c) > -1);
    }

    /// <summary>Goes to the end of a string and returns the string content</summary>
    /// <param name="initChar">The character of the string start identifier like " or '</param>
    /// <returns>The content of the string without quotes</returns>
    private String stringEnd(char initChar) {
	    final StringBuilder builder = new StringBuilder();
	    while (index < length) {
		    final char currentChar = doc.charAt(++index);
		    if (currentChar == '\\') {
			    builder.append(doc.charAt(index));
			    index++;
			    continue;
		    }
		    if (currentChar == initChar) {
			    break;
		    }
		    builder.append(currentChar);
	    }
	    return builder.toString();
    }

    /// <summary>Sets the index pointer to the end of the current line</summary>
    private void lineEnd() {
	    while (index < length) {
		    final char currentChar = doc.charAt(++index);
		    if (currentChar == '\n') {
			    break;
		    }
	    }
    }

    /// <summary>Sets the index pointer to the end of a multiline comment</summary>
    private void commentEnd() {
	    while (index < length) {
		    final char currentChar = doc.charAt(++index);
		    if (currentChar == '*' && doc.charAt(index) == '/') {
			    index ++;
			    break;
		    }
	    }
    }
}
