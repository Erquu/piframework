package de.paeterick.piframework.resources.json;

public class JSONTextNode extends JSONLiteralNode {
	public JSONTextNode(final String content) {
		super(content);
	}
	
	@Override
	public JSONObjectType getType() {
		return JSONObjectType.TEXT;
	}
	
	@Override
	public String toString() {
		return content;
	}
	
	@Override
	public String toJSONString(int depth, boolean minified) {
		return String.format("\"%s\"", JSONObject.toValidJSONString(content));
	}
}
