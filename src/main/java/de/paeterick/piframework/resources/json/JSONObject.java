package de.paeterick.piframework.resources.json;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JSONObject {
	protected static String INDENT_SEQUENCE =  "  ";
	
	protected JSONObject parent;
	
	private final Map<String, JSONObject> values = new HashMap<String, JSONObject>();
	
	public JSONObject() {
	}
	
	public boolean isEmpty() {
		return values.isEmpty();
	}
	
	public JSONObjectType getType() {
		return JSONObjectType.OBJECT;
	}
	
	public boolean containsKey(final String key) {
		return values.containsKey(key);
	}
	
	public void remove(final String key) {
		if (values.containsKey(key))
			values.remove(key);
	}
	
	public JSONObject get(final String key) {
		if (values.containsKey(key))
			return values.get(key);
		return null;
	}
	public JSONObject get(final String... keys) {
		JSONObject returnValue = this;
		for (int i = 0; i < keys.length; i++) {
			returnValue = returnValue.get(keys[i]);
			if (returnValue == null)
				break;
		}
		return returnValue;
	}
	
	public String getString(final String key) {
		return get(key).toString();
	}
	public String getString(final String... keys) {
		final JSONObject obj = get(keys);
		if (obj != null)
			return obj.toString();
		return null;
	}

	public double getDouble(final String key) throws NumberFormatException {
		return Double.parseDouble(getString(key));
	}
	public double getDouble(final String... keys) throws NullPointerException, NumberFormatException {
		final String value = getString(keys);
		if (value != null)
			return Double.parseDouble(getString(keys));
		throw new NullPointerException();
	}

	public float getFloat(final String key) throws NumberFormatException {
		return Float.parseFloat(getString(key));
	}
	public float getFloat(final String... keys) throws NullPointerException, NumberFormatException {
		final String value = getString(keys);
		if (value != null)
			return Float.parseFloat(getString(keys));
		throw new NullPointerException();
	}

	public byte getByte(final String key) throws NumberFormatException {
		return Byte.parseByte(getString(key));
	}
	public byte getByte(final String... keys) throws NullPointerException, NumberFormatException {
		final String value = getString(keys);
		if (value != null)
			return Byte.parseByte(getString(keys));
		throw new NullPointerException();
	}

	public short getShort(final String key) throws NumberFormatException {
		return Short.parseShort(getString(key));
	}
	public short getShort(final String... keys) throws NullPointerException, NumberFormatException {
		final String value = getString(keys);
		if (value != null)
			return Short.parseShort(getString(keys));
		throw new NullPointerException();
	}

	public int getInt(final String key) throws NumberFormatException {
		return Integer.parseInt(getString(key));
	}
	public int getInt(final String... keys) throws NullPointerException, NumberFormatException {
		final String value = getString(keys);
		if (value != null)
			return Integer.parseInt(getString(keys));
		throw new NullPointerException();
	}

	public long getLong(final String key) throws NumberFormatException {
		return Long.parseLong(getString(key));
	}
	public long getLong(final String... keys) throws NullPointerException, NumberFormatException {
		final String value = getString(keys);
		if (value != null)
			return Long.parseLong(getString(keys));
		throw new NullPointerException();
	}

	public boolean getBoolean(final String key) {
		return Boolean.parseBoolean(getString(key));
	}
	public boolean getBoolean(final String... keys) {
		final String value = getString(keys);
		if (value != null)
			return Boolean.parseBoolean(getString(keys));
		throw new NullPointerException();
	}
	
	public JSONObject add(final String key, final JSONObject value) {
		values.put(key, value);
		return this;
	}
	public JSONObject add(final String key, final String value) {
		if (value == null)
			return add(key, new JSONLiteralNode(value));
		return add(key, new JSONTextNode(value));
	}
	public JSONObject add(final String key, final boolean value) {
		return add(key, new JSONLiteralNode(Boolean.toString(value).toLowerCase()));
	}
	public JSONObject add(final String key, final byte value) {
		return add(key, new JSONLiteralNode(Byte.toString(value)));
	}
	public JSONObject add(final String key, final short value) {
		return add(key, new JSONLiteralNode(Short.toString(value)));
	}
	public JSONObject add(final String key, final int value) {
		return add(key, new JSONLiteralNode(Integer.toString(value)));
	}
	public JSONObject add(final String key, final long value) {
		return add(key, new JSONLiteralNode(Long.toString(value)));
	}
	public JSONObject add(final String key, final double value) {
		return add(key, new JSONLiteralNode(Double.toString(value)));
	}
	public JSONObject add(final String key, final float value) {
		return add(key, new JSONLiteralNode(Float.toString(value)));
	}
	
	public String toJSONString(final int depth, final boolean minified) {
		final String depthString = getDepthString(depth);
	    final StringBuilder builder = new StringBuilder();
	
	    builder.append("{");
	    if (!minified) {
		    builder.append("\n");
		    builder.append(depthString);
	    }
	    Iterator<String> keys = values.keySet().iterator();
	    int i = 0;
	    while (keys.hasNext()) {
		    final String key = keys.next();
		    builder.append(String.format("\"%s\": ", JSONObject.toValidJSONString(key)));
		    builder.append(values.get(key).toJSONString(depth + 1, minified));
		    if (++i < values.size())
			    builder.append(",");
		    if (!minified) {
			    builder.append("\n");
			    if (i < values.size()) {
				    builder.append(depthString);
			    }
		    }
	    }
	    if (!minified)
		    builder.append(getDepthString(depth - 1));
	    builder.append("}");
	    return builder.toString();
	}
	
	@Override
	public String toString() {
		return toString(false);
	}
	public String toString(final boolean minified) {
		return toJSONString(1, minified);
	}
	
	protected String getDepthString(final int depth) {
		final StringBuilder depthString = new StringBuilder();
		for (int i = 0; i < depth; i++)
			depthString.append(JSONObject.INDENT_SEQUENCE);
		return depthString.toString();
	}
	
	public static String toValidJSONString(final String str) {
		if (str == null)
			return str;
		return str.replace("\\", "\\\\").replace("\"", "\\\"");
	}
}
