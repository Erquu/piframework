package de.paeterick.piframework.resources.json;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class JSONArray extends JSONObject {
	private final List<JSONObject> objects = new LinkedList<JSONObject>();
	
	public int size() {
		return objects.size();
	}
	
	@Override
	public boolean isEmpty() {
		return objects.isEmpty();
	}
	
	@Override
	public JSONObjectType getType() {
		return JSONObjectType.ARRAY;
	}

	public Iterator<JSONObject> iterator() {
		return objects.iterator();
	}
	
	public JSONObject get(final int index) throws IndexOutOfBoundsException {
		return objects.get(index);
	}	

	public JSONObject add(final boolean value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final byte value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final double value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final float value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final int value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final JSONObject value) {
		objects.add(value);
		return this;
	}
	public JSONObject add(final long value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final short value) {
		objects.add(new JSONLiteralNode(value));
		return this;
	}
	public JSONObject add(final String value) {
		objects.add(new JSONTextNode(value));
		return this;
	}
	@Override
	public JSONObject add(final String key, final boolean value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final byte value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final double value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final float value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final int value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final JSONObject value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final long value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final short value) {
		return add(value);
	}
	@Override
	public JSONObject add(final String key, final String value) {
		return add(value);
	}

	@Override
	public String toJSONString(int depth, boolean minified) {
		final String depthString = getDepthString(depth);
		final StringBuilder builder = new StringBuilder();

        builder.append("[");
        if (!minified) {
            builder.append("\n");
            builder.append(depthString);
        }
        int i = 0;
        for (final JSONObject obj : objects) {
            builder.append(obj.toJSONString(depth + 1, minified));
            if (++i < objects.size())
                builder.append(",");
            if (!minified) {
                builder.append("\n");
                if (i < objects.size()) {
                    builder.append(depthString);
                }
            }
        }
        if (!minified)
            builder.append(getDepthString(depth - 1));
        builder.append("]");
        return builder.toString();
	}
}
