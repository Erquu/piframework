package de.paeterick.piframework.resources.json;

public class JSONLiteralNode extends JSONObject {
	protected String content;
	
	public JSONLiteralNode(final String content) {
		this.content = content;
	}
	public JSONLiteralNode(final double content) {
		this.content = Double.toString(content);
	}
	public JSONLiteralNode(final float content) {
		this.content = Float.toString(content);
	}
	public JSONLiteralNode(final boolean content) {
		this.content = Boolean.toString(content);
	}
	public JSONLiteralNode(final byte content) {
		this.content = Byte.toString(content);
	}
	public JSONLiteralNode(final short content) {
		this.content = Short.toString(content);
	}
	public JSONLiteralNode(final int content) {
		this.content = Integer.toString(content);
	}
	public JSONLiteralNode(final long content) {
		this.content = Long.toString(content);
	}
	
	public double toDouble() {
		return Double.parseDouble(content);
	}
	public float toFloat() {
		return Float.parseFloat(content);
	}
	public boolean toBoolean() {
		return Boolean.parseBoolean(content);
	}
	public byte toByte() {
		return Byte.parseByte(content);
	}
	public short toShort() {
		return Short.parseShort(content);
	}
	public int toInt() {
		return Integer.parseInt(content);
	}
	public long toLong() {
		return Long.parseLong(content);
	}
	
	@Override
	public JSONObjectType getType() {
		return JSONObjectType.LITERAL;
	}
	
	@Override
	public String toJSONString(int depth, boolean minified) {
		return content;
	}
}
