package de.paeterick.piframework.resources.json;

import java.io.IOException;
import java.io.InputStream;

public class JSON {
	private JSON() { }
	
	/// <summary>Parses a json document</summary>
    /// <param name="stream">Document input stream to read from</param>
    /// <returns>A JSONObject representing the root node</returns>
    public static JSONObject Parse(final InputStream stream) throws IOException
    {
        return (new JSONParser()).parse(stream);
    }
    /// <summary>Parses a json document</summary>
    /// <param name="doc">The document to be parsed</param>
    /// <returns>A JSONObject representing the root node</returns>
    public static JSONObject Parse(final String doc)
    {
        return (new JSONParser()).parse(doc);
    }
}
