package de.paeterick.piframework.resources.json;

public enum JSONObjectType {
	OBJECT,
	ARRAY,
	LITERAL,
	TEXT
}
