package de.paeterick.piframework.resources;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to load and manage images
 * @author Erquu
 */
public class ImageLoader extends SWrapper {
	/** Error image displayed when an image could not be loaded */
	private static final BufferedImage ERROR_IMAGE;
	
	static {
		ERROR_IMAGE = new BufferedImage(50, 20, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = (Graphics2D)ERROR_IMAGE.getGraphics();
		g.setBackground(Color.BLACK);
		g.setColor(Color.RED);
		g.drawString("ERROR", 5, 15);
	}
	
	/** Stores all images */
	private final Map<String, BufferedImage> imageStore = new HashMap<String, BufferedImage>();;
	
	/** Constructor */
	public ImageLoader() { }
	
	/** Loads an image into the store */
	public void loadImage(final String path) {
		setFile(path);
		final String name = getAbsoluteName();
		if (!imageStore.containsKey(name)) {
			try {
				imageStore.put(name, getResourceAsImage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Returns an image
	 * @param path the path of the image
	 * @return the loaded image
	 */
	public BufferedImage getImage(final String path) {
		//setFile(path);
		loadImage(path);
		final String name = getAbsoluteName();
		if (imageStore.containsKey(name)) {
			return imageStore.get(name);
		}
		return ERROR_IMAGE;
	}
}
