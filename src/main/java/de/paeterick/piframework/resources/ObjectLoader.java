package de.paeterick.piframework.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;

public class ObjectLoader extends SWrapper {
	
	public void load(final String path) throws IOException {
		setFile(path);
		final InputStream is = new GZIPInputStream(getResourceAsStream());
		final File file = File.createTempFile("lolz", "test");
		final OutputStream tmpOs = new FileOutputStream(file);
		
		final byte[] buffer = new byte[8192]; 
		
		try {
		    for (int length; (length = is.read(buffer)) != -1; ) {
		    	tmpOs.write( buffer, 0, length ); 
		    }
		} catch (IOException ex) {
			is.close();
		    tmpOs.close();
		    throw ex;
		}
	    
	    System.out.println(file.getAbsolutePath());
	    
	    is.close();
	    tmpOs.close();
	}
}
