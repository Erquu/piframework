package de.paeterick.piframework.resources;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * Loads resources from a resourcefolder
 * @author Erquu
 */
public class SWrapper {
	/** file separator string */
    public static final String SEP = "/";
    /** file separator character */
    public static final char SEPC = '/';
    /**
     * Contains all needed Fields
     */
    private final Data data = new Data();
    
    /**
     * Class for storing needed Data
     * @author csp8fe
     */
    private class Data {
    	/**
    	 * Filename to be loaded
    	 */
    	public String fileName;
    	/**
    	 * Internal path to the file
    	 */
    	public String absPath;
    	
    	/**
    	 * Sets all fields to null
    	 */
    	public void reset() {
    		fileName = null;
    		absPath = null;
    	}
    }

    /**
     * Updates the SWrapper and loads the given file
     * @param path file to be loaded
     */
    public void setFile(String path) {
    	path = path.trim().replace('/', SEPC).replace('\\', SEPC);
    	final int index = path.lastIndexOf("/");
    	if (index > -1)
    	{
    		setFile(path.substring(index+1), path.substring(0, index));
    	} else {
    		setFile(path, "");
    	}
    }
    /**
     * Updates the SWrapper and loads the given file
     * @param fileName filename of the file to be loaded
     * @param absPath path to the file
     */
	public void setFile(final String fileName, final String absPath) {
		
		data.reset();
		
		data.fileName = fileName.trim();
        String path = absPath.trim().replace('/', SEPC).replace('\\', SEPC);
        if (path.length() == 0 || (path.charAt(0) != SEPC))
        {
            path = SEPC + path;
        }
        if (path.length() > 1 && path.charAt(path.length() - 1) == SEPC)
        {
            path.substring(0, path.length() - 2);
        }

        data.absPath = path;
	}
	
	/**
	 * Returns the filename of the current set file
	 * @return filename of the current set file
	 */
	public String getFileName() {
		return data.fileName;
	}
	
	/**
	 * Returns the absolute name of the current set file
	 * @return the absolute name of the current set file
	 */
	public String getAbsoluteName() {
		if (data.absPath.equals(SEP))
        {
            return data.absPath.concat(data.fileName);
        }

        return data.absPath.concat(SEP).concat(data.fileName);
	}
	
	/**
	 * Returns the content of the current set file as a InputStream
	 * @see InputStream
	 * @return content as a InputStream
	 */
	public InputStream getResourceAsStream() {
		return getClass().getResourceAsStream(getAbsoluteName());
	}
	
	/**
	 * Returns the URL to the current set file
	 * @return
	 */
	public URL getResourceURL() {
		return getClass().getResource(getAbsoluteName());
	}
	
	/**
	 * Returns the current set resource as a <code>BufferedImage</code> 
	 * @return the current set resource as a <code>BufferedImage</code>
	 * @throws IOException 
	 */
	public BufferedImage getResourceAsImage() throws IOException {
		final InputStream in = getResourceAsStream();
		if (in != null)
			return ImageIO.read(in);
		else 
			throw new IOException();
		// TODO: Check if stream needs to be closed
	}

	/**
	 * Returns the content of the current set file as a String
	 * @return content as a String
	 */
	public String getResourceAsString() {
		try {
			final BufferedReader reader = new BufferedReader(new InputStreamReader(getResourceAsStream()));
			final StringBuffer buffer = new StringBuffer();
			if (reader.ready()) {
				String line;
				while ((line = reader.readLine()) != null) {
					buffer.append(line);
				}
			}
			reader.close();
			return buffer.toString();
		} catch (IOException e) {
			return "";
		}
	}
}
