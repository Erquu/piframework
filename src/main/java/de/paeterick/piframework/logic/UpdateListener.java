package de.paeterick.piframework.logic;

import java.util.EventListener;

/**
 * Update Listener used for the update loop
 * @author Erquu
 */
public interface UpdateListener extends EventListener {
	/**
	 * Will be called when the update loop iterated one time
 	 * @param time the current time
	 */
	void update(final long deltaTime, final long totalTime);
	void render(final long deltaTime, final long totalTime);
}
