package de.paeterick.piframework.logic;

import javax.swing.event.EventListenerList;

/**
 * Implementation of an update loop
 * @author Erquu
 */
public class UpdateLoop {
	/** The resoulution of the loop (timer); value of one second */
	public static final int TIMER_RESOLUTION = 1000;
	
	/**
	 * Returns the current time (in relation to the <code>TIMER_RESOLUTION</code>
	 */
	public static long getCurrentTime() {
		return System.currentTimeMillis();
	}
	
	/**
	 * All states of the loop (thread)
	 * @author Erquu
	 */
	private enum LoopState {
		Running, Paused, Stopped, Disposed, Error
	}
	
	/** The main loop thread */
	private Thread fLoopThread = null;
	/** The main loop state */
	private LoopState fLoopState = LoopState.Stopped;
	/** The timeout of one period */
	private int fPeriodTimeout;

	/** Listener list for update listeners */
	protected EventListenerList fUpdateListeners = new EventListenerList();
	/** Listener list for render listeners */
	protected EventListenerList fRenderListeners = new EventListenerList();
	
	/**
	 * Updates the fps limit
	 * @param maxFps the fps limit
	 */
	public void setFps(final int maxFps) {
		fPeriodTimeout = (UpdateLoop.TIMER_RESOLUTION / (maxFps - 1));
	}
	
	/** Stops the loop */
	public void Stop() {
		fLoopState = LoopState.Stopped;
	}
	
	/**
	 * Starts the loop
	 * @param maxFps sets the fps limit
	 */
	public void Start(final int maxFps) {
		
		setFps(maxFps);
		fLoopState = LoopState.Running;
		fLoopThread = new Thread(new Runnable() {
			public void run() {
				
				long currentTime = getCurrentTime();
				
				while (fLoopState == LoopState.Running) {
					final long newTime = getCurrentTime();
					long frametime = newTime - currentTime;
					currentTime = newTime;
					
					while (frametime > 0) {
						final long deltaTime = Math.min(frametime, fPeriodTimeout);
						fireUpdateEvent(deltaTime, currentTime);
						frametime -= deltaTime;
					}
					
					fireRenderEvent(frametime, currentTime);
				}
				
				
				
				/*
				long beforeTime;
				long afterTime;
				long timeDiff = 0;
				long sleepTime;
				
				long oversleepTime = 0;
				int noDelays = 0;
				long excess = 0;
				
				beforeTime = UpdateLoop.getCurrentTime();
				fLoopState = LoopState.Running;
				
				while (fLoopState == LoopState.Running) {
					fireUpdateEvent(timeDiff); //TODO: Check if beforeTime can be used here
					// Test for sync rendering
					fireRenderEvent(timeDiff);
					
					afterTime = UpdateLoop.getCurrentTime();
					timeDiff = afterTime - beforeTime;
					sleepTime = (fPeriodTimeout - timeDiff) - oversleepTime;
					
					if (sleepTime > 0) {
						try {
							Thread.sleep(sleepTime);
						} catch (InterruptedException e) {
							fLoopState = LoopState.Error;
							e.printStackTrace();
						}
						oversleepTime = (UpdateLoop.getCurrentTime() - afterTime) - sleepTime;
					} else {
						excess -= sleepTime;
						oversleepTime = 0;
						
						if (++noDelays >= NO_DELAYS_PER_YIELD) {
							try {
								Thread.sleep(1);
							} catch (InterruptedException e) {
								fLoopState = LoopState.Error;
								e.printStackTrace();
							}
							noDelays = 0;
						}
					}
					beforeTime = UpdateLoop.getCurrentTime();
					
					int skips = 0;
					while ((excess > fPeriodTimeout) && (skips < MAX_FRAME_SKIPS)) {
						excess -= fPeriodTimeout;
						fireUpdateEvent(timeDiff); //TODO: Check if beforeTime can be used here
						skips++;
					}
				}*/
			}
		});
		fLoopThread.setName("piEngine-Main Updateloop-thread");
		fLoopThread.start();
	}
	
	/** 
	 * Adds an UpdateListener 
	 * @param listener the listener to be added
	 */
	public void addUpdateEventListener(final UpdateListener listener) {
		fUpdateListeners.add(UpdateListener.class, listener);
	}
	
	/**
	 * Removes an UpdateListener
	 * @param listener the listener to be removed
	 */
	public void removeUpdateEventListener(final UpdateListener listener) {
		fUpdateListeners.remove(UpdateListener.class, listener);
	}
	
	/**
	 * Fires all UpdateListeners
	 * @param time the current time
	 */
	protected void fireUpdateEvent(final long deltaTime, final long totalTime) {
		Object[] listeners = fUpdateListeners.getListenerList();
		for (int i = 0; i < listeners.length; i+=2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener)listeners[i+1]).update(deltaTime, totalTime);
			}
		}
	}
	
	/** 
	 * Adds an UpdateListener 
	 * @param listener the listener to be added
	 */
	public void addRenderEventListener(final UpdateListener listener) {
		fRenderListeners.add(UpdateListener.class, listener);
	}
	
	/**
	 * Removes an UpdateListener
	 * @param listener the listener to be removed
	 */
	public void removeRenderEventListener(final UpdateListener listener) {
		fRenderListeners.remove(UpdateListener.class, listener);
	}
	
	/**
	 * Fires all UpdateListeners
	 * @param time the current time
	 */
	protected void fireRenderEvent(final long deltaTime, final long totalTime) {
		Object[] listeners = fRenderListeners.getListenerList();
		for (int i = 0; i < listeners.length; i+=2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener)listeners[i+1]).render(deltaTime, totalTime);
			}
		}
	}
}
