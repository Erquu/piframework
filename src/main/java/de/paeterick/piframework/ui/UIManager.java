package de.paeterick.piframework.ui;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.paeterick.piframework.game.IRenderable;
import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.game.graphics.Viewport;

public class UIManager implements IUpdateable, IRenderable {
	private final List<UI> fUiList = new LinkedList<UI>();
	private boolean fBlocking = false;
	
	public void addUiToRenderingQueue(final UI ui) {
		ui.fUiManager = this;
		
		if (!this.fUiList.contains(ui))
			this.fUiList.add(ui);
	}
	
	void removeUiFromRenderQueue(final UI ui) {
		if (!this.fUiList.contains(ui))
			this.fUiList.remove(ui);
	}

	public void setVisible(final boolean visible) {
		for (final UI ui : this.fUiList) {
			if (!visible) {
				ui.fInitialVisibility = ui.isVisible();
				ui.setVisible(false);
			} else {
				ui.setVisible(ui.fInitialVisibility);
			}
		}
	}
	
	public boolean isBlocking() {
		return this.fBlocking;
	}
	
	public void update(final long deltaTime, final long totalTime) {
		boolean blockGameUpdate = false;
		
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					ui.update(deltaTime, totalTime);
					
					if (ui.isBlocking()) {
						blockGameUpdate = true;
						break;
					}
				}
			}
		}
		
		this.fBlocking = blockGameUpdate;
	}

	public void render(final Graphics2D context, final Scene scene) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					ui.render(context, scene);
					
					if (ui.isBlocking())
						break;
				}
			}
		}
	}

	public void updateViewport(final Viewport view) {
		for (final UI ui : this.fUiList) {
			ui.updateViewport(view);
		}
	}
	
	public void onKeyPress(final int keyCode) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (Arrays.binarySearch(ui.getCapturedKeys(), keyCode) >= 0) {
						ui.onKeyPress(keyCode);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	public void onKeyDown(final int keyCode) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (Arrays.binarySearch(ui.getCapturedKeys(), keyCode) >= 0) {
						ui.onKeyDown(keyCode);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	public void onKeyUp(final int keyCode) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (Arrays.binarySearch(ui.getCapturedKeys(), keyCode) >= 0) {
						ui.onKeyUp(keyCode);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	
	public void onMouseMove(final MouseEvent e) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (ui.captureMouse()) {
						ui.onMouseMove(e);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	public void onMouseClick(final MouseEvent e) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (ui.captureMouse()) {
						ui.onMouseClick(e);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	public void onMouseDown(final MouseEvent e) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (ui.captureMouse()) {
						ui.onMouseDown(e);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	public void onMouseUp(final MouseEvent e) {
		if (this.fUiList.size() > 0) {
			for (final UI ui : this.fUiList) {
				if (ui.isVisible()) {
					if (ui.captureMouse()) {
						ui.onMouseUp(e);
						break;
					}
					if (ui.isBlocking())
						break;
				}
			}
		}
	}
	
	public void onMouseWheel() { }
}
