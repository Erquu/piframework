package de.paeterick.piframework.ui;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.GameEngine;
import de.paeterick.piframework.game.IRenderable;
import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.math.Vector2;

public class HUDElement implements IUpdateable, IRenderable {
	protected float left, right, top, bottom;
	protected Vector2 center;
	protected boolean visible = true;
	
	UIManager fUiManager;
	
	final Viewport fViewport = GameEngine.getInstance().getScene().viewport;
	private final List<HUDElement> fElements = new Vector<HUDElement>();
	
	public HUDElement() {
		
	}

	public boolean captureMouse() {
		for (final HUDElement hud : this.fElements) {
			if (hud.captureMouse()) {
				return true;
			}
		}
		return false;
	}
	
	public int[] getCapturedKeys() {
		if (fElements.isEmpty())
			return null;
		
		final List<Integer> capturedKeys = new Vector<Integer>();
		for (final HUDElement hud : this.fElements) {
			final int[] cKeys = hud.getCapturedKeys();
			for (final int i : cKeys) {
				capturedKeys.add(new Integer(i));
			}
		}
		if (capturedKeys.isEmpty())
			return null;
		else {
			final int[] returnValue = new int[capturedKeys.size()];
			for (int i = 0; i < capturedKeys.size(); i++) {
				returnValue[i] = capturedKeys.get(i).intValue();
			}
			return returnValue;
		}
	}
	
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}
	public boolean isVisible() {
		return this.visible;
	}
	
	public void addHudElement(final HUDElement element) {
		element.fUiManager = this.fUiManager;
		
		this.updateHudVars(element);
		if (!this.fElements.contains(element))
			this.fElements.add(element);
	}
	
	public void removeHudElement(final HUDElement element) {
		if (this.fElements.contains(element))
			this.fElements.remove(element);
	}
	
	public void update(final long deltaTime, final long totalTime) {
		for (final HUDElement e : this.fElements) {
			e.update(deltaTime, totalTime);
		}
	}
	
	public void render(final Graphics2D context, final Scene scene) {
		for (final HUDElement e : this.fElements) {
			e.render(context, scene);
		}
	}

	void updateViewport(final Viewport view) {
		for (final HUDElement hud : this.fElements) {
			this.updateHudVars(hud);
		}
	}
	
	private void updateHudVars(final HUDElement hud) {
		hud.left = fViewport.x;
		hud.top = fViewport.y;
		hud.right =  fViewport.width;
		hud.bottom = fViewport.height;
		hud.center = new Vector2(
			(fViewport.width - fViewport.x) * .5f,
			(fViewport.height - fViewport.y) * .5f
		);
	}
	
	protected void onKeyPress(final int keyCode) { }
	protected void onKeyDown(final int keyCode) { }
	protected void onKeyUp(final int keyCode) { }
	
	protected void onMouseMove(final MouseEvent e) { }
	protected void onMouseClick(final MouseEvent e) { }
	protected void onMouseDown(final MouseEvent e) { }
	protected void onMouseUp(final MouseEvent e) { }
	
	protected void onMouseWheel() { }
}
