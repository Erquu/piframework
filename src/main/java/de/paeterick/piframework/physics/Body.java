package de.paeterick.piframework.physics;

import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.collisiondetection.AABB;
import de.paeterick.piframework.physics.shapes.Shape;

public class Body {
	public float mass;
	public float inverseMass;
	
	public float inertia;
	public float inverseInertia;

	public final Vector2 position = Vector2.emptyVector();
	public final Vector2 velocity = Vector2.emptyVector();
	public final Vector2 force = Vector2.emptyVector();
	
	public float angularVelocity;
	public float torque;
	public float orient;
	public float staticFriction;
	public float dynamicFriction;
	public float restitution;
	
	public final Shape shape;
	
	private int fLayer = 1;
	
	public Body(final Shape shape, int x, int y) {
		this.shape = shape;
		
		position.x = x;
		position.y = y;
		
		angularVelocity = 0;
		torque = 0;
		orient = 0;
		staticFriction = 0.5f;
		dynamicFriction = 0.3f;
		restitution = 0.2f;
		
		shape.body = this;
		shape.initialize();
	}
	
	public void applyForce(final Vector2 force) {
		// force += f;
		this.force.add(force);
	}
	
	public void applyImpulse(final Vector2 impulse, final Vector2 contactVector) {
		// velocity += im * impulse;
		//velocity.add(impulse.multiplyNew(this.inverseMass));
		velocity.addsi(impulse, inverseMass);
		// angularVelocity += iI * Cross( contactVector, impulse );
		angularVelocity += inverseInertia * contactVector.crossProduct(impulse);
	}
	
	public void setOrientation(final float radians) {
		orient = radians;
		shape.setOrient(radians);
	}
	
	public void setStatic() {
		inertia = 0.0f;
		inverseInertia = 0.0f;
		mass = 0.0f;
		inverseMass = 0.0f;
	}
	
	public AABB computeAABB() {
		final AABB shapeBB = shape.computeAABB();
		
		return new AABB(
			shapeBB.getCenter().add(position),
			shapeBB.getHalfDimension()
		);
	}
	
	/*public void addLayer(final int layer) {
		if (layer > 0 && layer <= 16 && (layer == 1 || layer % 2 == 0) && (fLayer & layer) == 0)
			fLayer += layer;
	}
	
	public void removeLayer(final int layer) {
		if (layer > 0 && layer <= 16 && (layer == 1 || layer % 2 == 0) && (fLayer & layer) == layer)
			fLayer -= layer;
	}*/
	
	public void setLayer(final int layer) {
		fLayer = layer;
	}
	
	public int getLayer() {
		return fLayer;
	}
}
