package de.paeterick.piframework.physics;

import java.util.ArrayList;

import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;
import de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree;
import de.paeterick.piframework.physics.shapes.Shape;

public class Physics2 implements IUpdateable {
	public int iterations;
	
	public static final Vector2 GRAVITY = new Vector2(0.0f, 0.01f);
	public static final float RESTING = GRAVITY.multiply(1.0f / 60.0f).lengthSq() + MathHelper.EPSILON;
	
	public static final float PENETRATION_ALLOWANCE = .05f;
	public static final float PENETRATION_CORRECTION = .4f;
	
	public ArrayList<Body> bodies = new ArrayList<Body>();
	public ArrayList<Manifold> contacts = new ArrayList<Manifold>();
	
	// FIXME: REMOVE THIS
	public Manifold[] lastManifolds;
	
	private final IQuadTree tree;
	
	public Physics2(final int iterations, final IQuadTree tree) {
		this.iterations = iterations;
		this.tree = tree;
	}

	public Body add(final Shape shape, final int x, final int y) {
		return this.add(new Body(shape, x, y));
	}
	public Body add(final Body body) {
		bodies.add(body);
		tree.insert(body);
		
		return body;
	}
	
	public void clear() {
		contacts.clear();
		bodies.clear();
		tree.clear();
	}
	
	public void update(long deltaTime, long totalTime) {
		
		float dt = (float)deltaTime * 0.001f;
		//float dt = 1f / 60f;
		
		tree.update(deltaTime, totalTime);
		contacts.clear();
		
		// TODO: Remove, this is just a test cd system
		/**/
		for (int i = 0; i < bodies.size(); i++) {
			Body a = bodies.get(i);
			
			for (int j = i+1; j < bodies.size(); j++) {
				Body b = bodies.get(j);
				
				if (a.inverseMass == 0 && b.inverseMass == 0) {
					continue;
				}
				
				Manifold m = new Manifold(a, b);
				m.solve();
				
				if (m.contactCount > 0) 
				{
					contacts.add(m);
				}
			}
		}
		/**/
		/**/
		final Manifold[] manifolds = lastManifolds = tree.generatePairs();
		for (final Manifold m : manifolds) {
			m.solve();
			if (m.contactCount > 0)
				contacts.add(m);
		}
		/**/
		
		for (int i = 0; i < bodies.size(); i++) {
			integrateForces(bodies.get(i), dt);
		}
		
		for (int i = 0; i < contacts.size(); i++) {
			contacts.get(i).initialize();
		}
		
		for (int j = 0; j < iterations; j++) {
			for (int i = 0; i < contacts.size(); i++) {
				contacts.get(i).applyImpulse();
			}
		}
		
		for (int i = 0; i < bodies.size(); i++) {
			integrateVelocity(bodies.get(i), dt);
		}
		
		for (int i = 0; i < contacts.size(); i++) {
			contacts.get(i).positionalCorrection();
		}
		
		for (int i = 0; i < bodies.size(); i++) {
			Body b = bodies.get(i);
			
			b.force.x = 0;
			b.force.y = 0;
			b.torque = 0;
		}
	}
	
	private void integrateForces(final Body b, final float dt) {
		if (b.inverseMass == 0.0f)
			return;
		
		//b.velocity = b.velocity + b.force * invMass * dts
		//b.velocity.add(b.force.multiplyNew(b.inverseMass * dt));
		//b.velocity.add(GRAVITY.multiplyNew(dt));
		
		b.velocity.addsi(b.force, b.inverseMass * dt);
		b.velocity.addsi(GRAVITY, dt);
		
		b.angularVelocity += b.torque * b.inverseInertia * dt;
	}
	
	private void integrateVelocity(final Body b, final float dt) {
		if (b.inverseMass == 0.0f)
			return;
		
		//b.position.add(b.velocity.multiplyNew(dt));
		b.position.addsi(b.velocity, dt);
		b.orient += b.angularVelocity * dt;
		b.setOrientation(b.orient);
		
		integrateForces(b, dt);
	}
}
