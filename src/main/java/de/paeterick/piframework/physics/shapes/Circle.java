package de.paeterick.piframework.physics.shapes;

import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.collisiondetection.AABB;

public class Circle extends Shape {
	
	public Circle(float r) {
		radius = r;
	}

	@Override
	public Shape clone() {
		return new Circle(radius);
	}

	@Override
	public void initialize() {
		computeMass(1.0f);
	}

	@Override
	public void computeMass(float density) {
		body.mass = (float)Math.PI * radius * radius * density;
		body.inverseMass = (body.mass != 0.0f) ? 1.0f / body.mass : 0.0f;
		body.inertia = body.mass * radius * radius;
		body.inverseInertia = (body.inertia != 0.0f) ? 1.0f / body.inertia : 0.0f;
	}

	@Override
	public void setOrient(float radians) {
		// TODO Auto-generated method stub

	}

	@Override
	public Type getType() {
		return Type.CIRCLE;
	}

	@Override
	public AABB computeAABB() {
		return new AABB(new Vector2(radius, radius), new SizeF(radius * 2.0f, radius * 2.0f));
	}

}
