package de.paeterick.piframework.physics.shapes;

import de.paeterick.piframework.math.Matrix2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.AABB;

public abstract class Shape {
	public enum Type {
		CIRCLE,
		POLYGON,
		COUNT
	}
	
	public Body body;
	public float radius;
	public Matrix2 u = Matrix2.identityMatrix();
	
	public abstract Shape clone();
	public abstract void initialize();
	public abstract void computeMass(final float density);
	public abstract void setOrient(float radians);
	public abstract Type getType();
	public abstract AABB computeAABB();
}
