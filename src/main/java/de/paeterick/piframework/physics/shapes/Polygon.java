package de.paeterick.piframework.physics.shapes;

import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.collisiondetection.AABB;

public class Polygon extends Shape {
	
	public static final int MAX_POLYGON_VERTEX_COUNT = 64;
	
	public int vertexCount;
	public Vector2[] vertices = Vector2.arrayOf(MAX_POLYGON_VERTEX_COUNT);
	public Vector2[] normals = Vector2.arrayOf(MAX_POLYGON_VERTEX_COUNT);

	
	public Polygon() {
		
	}
	public Polygon(final Vector2... vertices) {
		set(vertices);
	}
	public Polygon(final float hw, final float hh) {
		setBox(hw, hh);
	}
	
	
	@Override
	public Shape clone() {
		final Polygon p = new Polygon();
		p.u.set(u);
		
		for (int i = 0; i < vertexCount; i++) {
			p.vertices[i] = vertices[i].clone();
			p.normals[i] = normals[i].clone();
		}
		p.vertexCount = vertexCount;
		
		return p;
	}

	@Override
	public void initialize() {
		computeMass(1.0f);
	}

	@Override
	public void computeMass(float density) {
		final Vector2 centroid = Vector2.emptyVector();
		float area = 0.0f;
		float I = 0.0f;
		final float kInv3 = 1.0f / 3.0f;
		
		for (int i = 0; i < vertexCount; ++i) {
			final Vector2 p1 = vertices[i];
			final Vector2 p2 = vertices[((i+1) % vertexCount)];
			
			float D = p1.crossProduct(p2);
			float triangleArea = 0.5f * D;
			
			area += triangleArea;
			
			float weight = triangleArea * kInv3;
			centroid.x += (p1.x * weight);
			centroid.y += (p1.y * weight);
			centroid.x += (p2.x * weight);
			centroid.y += (p2.y * weight);
			
			float intx2 = p1.x * p1.x + p2.x * p1.x + p2.x * p2.x;
			float inty2 = p1.y * p1.y + p2.y * p1.y + p2.y * p2.y;
			
			I += (0.25f * kInv3 * D) * (intx2 + inty2);
		}
		centroid.multiply(1.0f / area);
		
		for (int i = 0; i < vertexCount; ++i) {
			vertices[i].subtract(centroid);
		}
		
		body.mass = density * area;
		body.inverseMass = (body.mass != 0.0f) ? 1.0f / body.mass : 0.0f;
		body.inertia = I * density;
		body.inverseInertia = (body.inertia != 0.0f) ? 1.0f / body.inertia : 0.0f;
	}

	@Override
	public void setOrient(float radians) {
		u.set(radians);
	}

	@Override
	public Type getType() {
		return Type.POLYGON;
	}
	
	public void setBox(final float hw, final float hh) {
		vertexCount = 4;
		
		vertices[0].x = -hw;
		vertices[0].y = -hh;
		
		vertices[1].x = hw;
		vertices[1].y = -hh;
		
		vertices[2].x = hw;
		vertices[2].y = hh;
		
		vertices[3].x = -hw;
		vertices[3].y = hh;
		
		normals[0].x = 0.0f;
		normals[0].y = -1.0f;
		
		normals[1].x = 1.0f;
		normals[1].y = 0.0f;
		
		normals[2].x = 0.0f;
		normals[2].y = 1.0f;
		
		normals[3].x = -1.0f;
		normals[3].y = 0.0f;
	}
	
	public void set(final Vector2... vertices) {
		int rightMost = 0;
		
		float highestXCoord = vertices[0].x;
		
		for (int i = 0; i < vertices.length; ++i) {
			float x = vertices[i].x;
			
			if (x > highestXCoord) {
				highestXCoord = x;
				rightMost = i;
			} else if (x == highestXCoord) {
				if (vertices[i].y < vertices[rightMost].y) {
					rightMost = i;
				}
			}
		}
		
		final int[] hull = new int[MAX_POLYGON_VERTEX_COUNT];
		int outCount = 0;
		int indexHull = rightMost;
		
		for (;;) {
			hull[outCount] = indexHull;
			
			int nextHullIndex = 0;
			for (int i = 0; i < vertices.length; ++i) {
				if (nextHullIndex == indexHull) {
					nextHullIndex = i;
					continue;
				}
				
				final Vector2 e1 = vertices[nextHullIndex].subtractNew(vertices[hull[outCount]]);
				final Vector2 e2 = vertices[i].subtractNew(vertices[hull[outCount]]);
				float c = e1.crossProduct(e2);
				if (c < 0.0f) {
					nextHullIndex = i;
				}
				
				if (c == 0.0f && e2.lengthSq() > e1.lengthSq()) {
					nextHullIndex = i;
				}				
			}
			outCount++;
			indexHull = nextHullIndex;
			
			if (nextHullIndex == rightMost) {
				vertexCount = outCount;
				break;
			}
		}
		
		for (int i = 0; i < vertexCount; ++i) {
			this.vertices[i] = vertices[hull[i]].clone();
		}
		
		for (int i = 0; i < vertexCount; ++i) {
			Vector2 face = this.vertices[(i+1) % vertexCount].subtractNew(this.vertices[i]);
			
			normals[i].x = face.y;
			normals[i].y = -face.x;
			normals[i].normalize();
		}
	}

	public Vector2 getSupport(final Vector2 dir) {
		float bestProjection = -Float.MAX_VALUE;
		Vector2 bestVertex = null;
		
		for (int i = 0; i < vertexCount; i++) {
			final Vector2 v = vertices[i];
			final float projection = v.dotProduct(dir);
			
			if (projection > bestProjection) {
				bestVertex = v;
				bestProjection = projection;
			}
		}
		
		if (bestVertex == null) {
			System.out.println("vin");
		}
		
		return bestVertex;
	}
	@Override
	public AABB computeAABB() {
		float minX = Float.MAX_VALUE;
		float minY = Float.MAX_VALUE;
		float maxX = -Float.MAX_VALUE;
		float maxY = -Float.MAX_VALUE;
		
		for (int i = 0; i < vertices.length; i++) {
			final Vector2 vertex = vertices[i].transformNew(u);
			
			if (vertex.x > maxX)
				maxX = vertex.x;
			if (vertex.x < minX)
				minX = vertex.x;
			
			if (vertex.y > maxY)
				maxY = vertex.y;
			if (vertex.y < minY)
				minY = vertex.y;
		}
		
		return AABB.fromCoords(
			new Vector2(minX, minY),
			new Vector2(maxX, maxY)
		);
	}
}
