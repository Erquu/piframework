package de.paeterick.piframework.physics;

public class Material {
	
	public static final Material ROCK = new Material(0.6f, 0.1f);
	public static final Material WOOD = new Material(0.3f, 0.2f);
	public static final Material METAL = new Material(1.2f, 0.05f);
	public static final Material BOUNCYBALL = new Material(0.3f, 0.8f);
	public static final Material SUPERBALL = new Material(0.3f, 0.95f);
	public static final Material PILLOW = new Material(0.1f, 0.2f);
	public static final Material STATIC = new Material(0, 0.4f);
	
	public final float density;
	public final float restitution;
	
	public Material(final float density, final float restitution) {
		this.density = density;
		this.restitution = restitution;
	}
}
