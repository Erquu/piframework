package de.paeterick.piframework.physics.collisiondetection;

import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;

public interface CollisionCallback {
	void handleCollision(final Manifold m, final Body a, final Body b);
}
