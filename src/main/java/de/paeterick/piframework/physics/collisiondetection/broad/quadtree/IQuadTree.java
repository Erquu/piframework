package de.paeterick.piframework.physics.collisiondetection.broad.quadtree;

import java.util.List;

import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;

public interface IQuadTree extends IUpdateable {
	/**
	 * Removes all objects from the tree
	 */
	void clear();
	/**
	 * Updates the size and location of the tree
	 * @param center The center location of the tree
	 * @param halfDimension The half dimension of the tree
	 */
	void updateBoundaries(final Vector2 center, final SizeF halfDimension);
	/**
	 * Checks if the tree or any subtrees contain elements
	 * @return true if the tree or any subtrees contain elements
	 */
	boolean containsElements();
	/**
	 * Inserts an object into the tree or one of it's subtrees
	 * @param collidable The object to insert
	 * @return true if the object could be inserted
	 */
	boolean insert(final Body collidable);
	/**
	 * Removes an object from the tree or from a subtree
	 * @param collidable The object to remove
	 * @return true if the object could be removed
	 */
	boolean remove(final Body collidable);
	/**
	 * Queries a given range and returns all colliding objects that are in the tree
	 * @param range The range to query
	 * @return All objects that intersect with the given object
	 */
	List<Body> queryRange(final Body range);
	/**
	 * Creates an array of body-pairs which might collide
	 * @return An array of body-pairs which might collide
	 */
	Manifold[] generatePairs();
}
