package de.paeterick.piframework.physics.collisiondetection.broad;

import de.paeterick.piframework.physics.collisiondetection.CollisionCallback;
import de.paeterick.piframework.physics.collisiondetection.CollisionCircleCircle;
import de.paeterick.piframework.physics.collisiondetection.CollisionCirclePolygon;
import de.paeterick.piframework.physics.collisiondetection.CollisionPolygonCircle;
import de.paeterick.piframework.physics.collisiondetection.CollisionPolygonPolygon;

public class Collisions {
	public static CollisionCallback[][] dispatch = {
		{CollisionCircleCircle.instance, CollisionCirclePolygon.instance},
		{CollisionPolygonCircle.instance, CollisionPolygonPolygon.instance}
	};
	
	private Collisions(){}
}
