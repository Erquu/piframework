package de.paeterick.piframework.physics.collisiondetection.broad.quadtree;

import java.awt.Rectangle;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.AABB;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;

public class QuadTree extends AABB implements IQuadTree {
	
	/** The maximum depth for dividing into subtrees */
	protected static final int MAX_DEPTH = 6;
	
	/** NW ne <br />sw se */
	protected static final int NW = 0;
	/** nw NE <br />sw se */
	protected static final int NE = 1;
	/** nw ne<br />SW se */
	protected static final int SW = 2;
	/** nw ne<br />sw SE */
	protected static final int SE = 3;
	
	protected QuadTree[] fSubTrees = null;
	protected List<Body> fCollidableList = null;
	protected int fCurrentDepth = 0;
	
	public static QuadTree createInstance(final Vector2 center, final SizeF halfDimension) {
		return new QuadTree(center, halfDimension, 0);
	}
	public static QuadTree createInstance(final float x, final float y, final float width, final float height) {
		final SizeF halfDimension = new SizeF(width * .5f, height * .5f);
		final Vector2 center = new Vector2(x + halfDimension.width, y + halfDimension.height);
		return createInstance(center, halfDimension);
	}
	public static QuadTree createInstance(final int x, final int y, final int width, final int height) {
		return createInstance((float)x, (float)y, (float)width, (float)height);
	}
	public static QuadTree createInstance(final Rectangle bounds) {
		return createInstance(bounds.x, bounds.y, bounds.width, bounds.height);
	}
	
	protected QuadTree(final Vector2 center, final SizeF halfDimension, final int depth) {
		super(center, halfDimension);
		
		fCollidableList = new Vector<Body>();
		fCurrentDepth = depth;
		
		if (depth < MAX_DEPTH) {
			divide();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree#clear()
	 */
	public void clear() {
		fCollidableList.clear();
		if (fSubTrees != null) {
			for (int i = 0; i < fSubTrees.length; i++) {
				fSubTrees[i].clear();
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree#insert(de.paeterick.piframework.physics.IPhysicsObject)
	 */
	public boolean insert(final Body collidable) {
		if (!contains(collidable.computeAABB())) {
			return false;
		}
		
		if (fSubTrees != null) {
			for (int i = 0; i < fSubTrees.length; i++) {
				if (fSubTrees[i].insert(collidable)) {
					return true;
				}
			}
		}
		
		// If no sub tree contains the whole object, add it to the smallest tree
		// which contains the object (which is at this time the current instance
		fCollidableList.add(collidable);
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree#remove(de.paeterick.piframework.physics.IPhysicsObject)
	 */
	public boolean remove(final Body collidable) {
		if (fCollidableList.remove(collidable))
			return true;
		if (fSubTrees != null) {
			for (final IQuadTree subTree : fSubTrees) {
				if (subTree.remove(collidable))
					return true;
			}
		}
		return false;
	}
	/**
	 * Creates 4 subtrees for each corner
	 */
	protected void divide() {
		if (fSubTrees != null)
			return;
		
		final Vector2 currentCenter = Vector2.emptyVector();
		final SizeF currentSize = new  SizeF(fHalfDimension.width * .5f, fHalfDimension.height * .5f);
		
		fSubTrees = new QuadTree[4];
		
		currentCenter.x = getCenter().x - currentSize.width;
		currentCenter.y = getCenter().y - currentSize.height;
		fSubTrees[NW] = new QuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1);

		currentCenter.x += fHalfDimension.width;
		fSubTrees[NE] = new QuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1);

		currentCenter.y += fHalfDimension.height;
		fSubTrees[SE] = new QuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1);
		
		currentCenter.x -= fHalfDimension.width;
		fSubTrees[SW] = new QuadTree(currentCenter, currentSize, fCurrentDepth+1);
	}

	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree#containsElements()
	 */
	public boolean containsElements() {
		boolean contains = fCollidableList.size() > 0;
		if (!contains && fSubTrees != null) {
			for (int i = 0; i < fSubTrees.length; i++)
				contains = contains || fSubTrees[i].containsElements();
		}
		return contains;
	}

	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree#queryRange(de.paeterick.piframework.physics.IPhysicsObject)
	 */
	public List<Body> queryRange(final Body range) {
		final List<Body> foundObjects = new Vector<Body>();
		final AABB rangeAABB = range.computeAABB();
		
		if (intersectsWith(rangeAABB)) {
			for (final Body c : fCollidableList) {
				if (range.inverseMass == 0.0f && c.inverseMass == 0.0f)
					continue;
				
				if (!range.equals(c) && rangeAABB.intersectsWith(c.computeAABB())) {
					// Check if layers overlap
					if ((range.getLayer() & c.getLayer()) != 0)
						foundObjects.add(c);
				}
			}
			
			if (fSubTrees != null) {
				for (int i = 0; i < fSubTrees.length; i++) {
					foundObjects.addAll(fSubTrees[i].queryRange(range));
				}
			}
		}
		
		return foundObjects;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.physics.collisiondetection.broad.quadtree.IQuadTree#updateBoundaries(de.paeterick.piframework.math.Vector2, de.paeterick.piframework.math.SizeF)
	 */
	public void updateBoundaries(final Vector2 center, final SizeF halfDimension) {
		fCenter = center;
		fHalfDimension = halfDimension;
		
		if (fSubTrees == null)
			return;
		
		final Vector2 currentCenter = Vector2.emptyVector();
		final SizeF currentSize = new  SizeF(fHalfDimension.width * .5f, fHalfDimension.height * .5f);
		
		currentCenter.x = getCenter().x - currentSize.width;
		currentCenter.y = getCenter().y - currentSize.height;
		fSubTrees[NW].updateBoundaries(currentCenter.clone(), currentSize);

		currentCenter.x += fHalfDimension.width;
		fSubTrees[NE].updateBoundaries(currentCenter.clone(), currentSize);

		currentCenter.y += fHalfDimension.height;
		fSubTrees[SE].updateBoundaries(currentCenter.clone(), currentSize);
		
		currentCenter.x -= fHalfDimension.width;
		fSubTrees[SW].updateBoundaries(currentCenter, currentSize);
	}
	
	private List<Manifold> generatePairsNoArray() {
		// Find pairs
		final List<Manifold> foundPairs = new Vector<Manifold>();
		for (final Body aabb : fCollidableList) {
			final List<Body> rel = queryRange(aabb);
			for (final Body r : rel) {
				if (r != aabb) {
					foundPairs.add(new Manifold(aabb, r));
				}
			}
		}
		if (fSubTrees != null) {
			for (final QuadTree subTree : fSubTrees) {
				foundPairs.addAll(subTree.generatePairsNoArray());
			}
		}
		
		// Remove duplicates
		final Set<Manifold> s = new TreeSet<Manifold>(new Comparator<Manifold>() {
			public int compare(final Manifold o1, final Manifold o2) {
				return 
					(o1.a == o2.a && o1.b == o2.b) ||
					(o1.a == o2.b && o1.b == o2.a) ? 0 : 1;
			}
		});
		s.addAll(foundPairs);
		
		return foundPairs;
	}
	
	public Manifold[] generatePairs() {
		final List<Manifold> foundPairs = generatePairsNoArray();
		return foundPairs.toArray(new Manifold[foundPairs.size()]);
	}
	
	public void update(long deltaTime, long totalTime) {
		// TODO Auto-generated method stub
		
	}
	
}