package de.paeterick.piframework.physics.collisiondetection.broad.quadtree;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import de.paeterick.piframework.game.IRenderable;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.AABB;

public class RenderingQuadTree extends QuadTree implements IRenderable {
	
	private static final Color[] colors;
	static {
		colors = new Color[MAX_DEPTH+1];
		for (int i = 0; i < MAX_DEPTH+1; i++) {
			colors[i] = new Color(
				MathHelper.randomFloat(0, 1),
				MathHelper.randomFloat(0, 1),
				MathHelper.randomFloat(0, 1)
			);
		}
	}
	
	public static RenderingQuadTree createInstance(final Vector2 center, final SizeF halfDimension) {
		return new RenderingQuadTree(center, halfDimension, 0);
	}
	public static RenderingQuadTree createInstance(final float x, final float y, final float width, final float height) {
		final SizeF halfDimension = new SizeF(width * .5f, height * .5f);
		final Vector2 center = new Vector2(x + halfDimension.width, y + halfDimension.height);
		return createInstance(center, halfDimension);
	}
	public static RenderingQuadTree createInstance(final int x, final int y, final int width, final int height) {
		return createInstance((float)x, (float)y, (float)width, (float)height);
	}
	public static RenderingQuadTree createInstance(final Rectangle bounds) {
		return createInstance(bounds.x, bounds.y, bounds.width, bounds.height);
	}
	
	protected RenderingQuadTree(final Vector2 center, final SizeF halfDimension, final int depth) {
		super(center, halfDimension, depth);
		
	}
	
	/**
	 * @see QuadTree#divide()
	 */
	protected void divide() {
		if (fSubTrees != null)
			return;
		
		final Vector2 currentCenter = Vector2.emptyVector();
		final SizeF currentSize = new  SizeF(fHalfDimension.width * .5f, fHalfDimension.height * .5f);
		
		fSubTrees = new RenderingQuadTree[4];
		
		currentCenter.x = getCenter().x - currentSize.width;
		currentCenter.y = getCenter().y - currentSize.height;
		fSubTrees[NW] = new RenderingQuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1);

		currentCenter.x += fHalfDimension.width;
		fSubTrees[NE] = new RenderingQuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1);

		currentCenter.y += fHalfDimension.height;
		fSubTrees[SE] = new RenderingQuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1);
		
		currentCenter.x -= fHalfDimension.width;
		fSubTrees[SW] = new RenderingQuadTree(currentCenter, currentSize, fCurrentDepth+1);
	}
	
	public void render(final Graphics2D context, final Scene scene) {
		if (fSubTrees != null) {
			for (int i = 0; i < fSubTrees.length; i++) {
				if (fSubTrees[i] != null)
					((RenderingQuadTree)fSubTrees[i]).render(context, scene);
			}
		}
		if (containsElements()) {
			context.setColor(colors[fCurrentDepth]);
			context.drawRect(
				(int)getLeft(),
				(int)getTop(),
				(int)(fHalfDimension.width * 2.0f),
				(int)(fHalfDimension.height * 2.0f));
			
			for (final Body o : fCollidableList) {
				context.setColor(Color.white);
				
				final AABB oAABB = o.computeAABB();
				
				context.drawRect(
					(int)oAABB.getLeft(),
					(int)oAABB.getTop(),
					(int)(oAABB.getHalfDimension().width) * 2,
					(int)(oAABB.getHalfDimension().height) * 2);
				
				context.drawLine(
					(int)oAABB.getCenter().x,
					(int)oAABB.getCenter().y,
					(int)fCenter.x,
					(int)fCenter.y);
			}
		}
	}
}
