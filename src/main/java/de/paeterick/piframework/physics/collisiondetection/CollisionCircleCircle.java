package de.paeterick.piframework.physics.collisiondetection;

import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;
import de.paeterick.piframework.physics.shapes.Circle;

public class CollisionCircleCircle implements CollisionCallback {
	
	public static final CollisionCircleCircle instance = new CollisionCircleCircle();

	private CollisionCircleCircle(){}
	
	public void handleCollision(final Manifold m, final Body a, final Body b) {
		final Circle A = (Circle)a.shape;
		final Circle B = (Circle)b.shape;
		
		final Vector2 normal = b.position.subtractNew(a.position);
		float distanceSq = normal.lengthSq();
		float radius = A.radius + B.radius;
		
		if (distanceSq >= radius * radius) {
			m.contactCount = 0;
			return;
		}
		
		float distance = (float)StrictMath.sqrt(distanceSq);
		m.contactCount = 1;
		if (distance == 0.0f) {
			m.penetration = A.radius;
			
			m.normal.x = 1.0f;
			m.normal.y = 0.0f;
			
			m.contacts[0].x = a.position.x;
			m.contacts[0].y = a.position.y;
		} else {
			m.penetration = radius - distance;
			m.normal.set(normal).divide(distance);
			m.contacts[0].set(m.normal).multiply(A.radius).add(a.position);
		}
	}
}
