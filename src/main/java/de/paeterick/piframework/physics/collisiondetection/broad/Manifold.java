package de.paeterick.piframework.physics.collisiondetection.broad;

import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.Physics2;

public class Manifold {
	public final Body a;
	public final Body b;
	
	public float penetration;
	public final Vector2 normal = Vector2.emptyVector();
	public final Vector2[] contacts = {Vector2.emptyVector(), Vector2.emptyVector()};
	public int contactCount;
	public float e;
	public float df;
	public float sf;
	
	public Manifold(final Body a, final Body b) {
		this.a = a;
		this.b = b;
	}
	
	public void solve() {
		int ia = a.shape.getType().ordinal();
		int ib = b.shape.getType().ordinal();
		
		Collisions.dispatch[ia][ib].handleCollision(this, a, b);
	}
	
	public void initialize() {
		e = Math.min(a.restitution, b.restitution);
		
		sf = (float)Math.sqrt(a.staticFriction * a.staticFriction);
		df = (float)Math.sqrt(a.dynamicFriction * a.dynamicFriction);
		
		for (int i = 0; i < contactCount; i++) {
			final Vector2 ra = contacts[i].subtractNew(a.position);
			final Vector2 rb = contacts[i].subtractNew(b.position);
			
			final Vector2 rv = b.velocity.clone();
			rv.add(Vector2.cross(b.angularVelocity, rb));
			rv.subtract(a.velocity);
			rv.subtract(Vector2.cross(a.angularVelocity, ra));
			
			if (rv.lengthSq() < Physics2.RESTING)
				e = 0.0f;
		}
	}
	
	public void applyImpulse() {
		if (MathHelper.equals(a.inverseMass + b.inverseMass, 0)) {
			a.velocity.x = a.velocity.y = 0;
			b.velocity.x = b.velocity.y = 0;
			return;
		}
		
		for (int i = 0; i < contactCount; i++) {
			final Vector2 ra = contacts[i].subtractNew(a.position);
			final Vector2 rb = contacts[i].subtractNew(b.position);
			
			Vector2 rv = b.velocity.clone();
			rv.add(Vector2.cross(b.angularVelocity, rb));
			rv.subtract(a.velocity);
			rv.subtract(Vector2.cross(a.angularVelocity, ra));
			
			final float contactVel = rv.dotProduct(normal);
			
			if (contactVel > 0)
				return;
			
			// TODO: Check this out
			float raCrossN = ra.crossProduct(normal);
			float rbCrossN = rb.crossProduct(normal);
			float invMassSum = a.inverseMass + b.inverseMass + (raCrossN * raCrossN) * a.inverseInertia + (rbCrossN * rbCrossN) * b.inverseInertia;
			
			float j = -(1.0f + e) * contactVel;
			j /= invMassSum;
			j /= contactCount;
			
			final Vector2 impulse = normal.multiplyNew(j);
			a.applyImpulse(impulse.negateNew(), ra);
			b.applyImpulse(impulse, rb);
			
			rv = b.velocity.clone();
			rv.add(Vector2.cross(b.angularVelocity, rb));
			rv.subtract(a.velocity);
			rv.subtract(Vector2.cross(a.angularVelocity, ra));
			
			final Vector2 t = rv.clone();
			//t.add(normal.multiplyNew(-rv.dotProduct(normal)));
			t.addsi(normal, -rv.dotProduct(normal));
			t.normalize();
			
			float jt = -rv.dotProduct(t);
			jt /= invMassSum;
			jt /= contactCount;
			
			if (MathHelper.equals(jt, 0.0f))
				return;
			
			final Vector2 tangentImpulse;
			if (Math.abs(jt) < j * sf) {
				tangentImpulse = t.multiplyNew(jt);
			} else {
				tangentImpulse = t.multiplyNew(j).multiply(-df);
			}
			
			a.applyImpulse(tangentImpulse.negateNew(), ra);
			b.applyImpulse(tangentImpulse, rb);
		}
	}
	
	public void positionalCorrection() {
		final float correction = 
				Math.max(penetration - Physics2.PENETRATION_ALLOWANCE, 0.0f) /
				(a.inverseMass + b.inverseMass) *
				Physics2.PENETRATION_CORRECTION;
		
		//a.position.add(normal.addNew(-a.inverseMass * correction));
		//b.position.add(normal.addNew(b.inverseMass * correction));
		
		a.position.addsi(normal, -a.inverseMass * correction);
		b.position.addsi(normal, b.inverseMass * correction);
	}
}
