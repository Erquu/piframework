package de.paeterick.piframework.physics.collisiondetection.broad.quadtree;

import java.awt.Rectangle;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;

public class DynamicQuadTree extends QuadTree implements IUpdateable {
	
	private DynamicQuadTree fParentTree = null;
	
	private final List<Body> rem = new Vector<Body>();
	private final List<Body> ins = new Vector<Body>();
	
	public static DynamicQuadTree createInstance(final Vector2 center, final SizeF halfDimension) {
		return new DynamicQuadTree(center, halfDimension, 0, null);
	}
	public static DynamicQuadTree createInstance(final float x, final float y, final float width, final float height) {
		final SizeF halfDimension = new SizeF(width * .5f, height * .5f);
		final Vector2 center = new Vector2(x + halfDimension.width, y + halfDimension.height);
		return createInstance(center, halfDimension);
	}
	public static DynamicQuadTree createInstance(final int x, final int y, final int width, final int height) {
		return createInstance((float)x, (float)y, (float)width, (float)height);
	}
	public static DynamicQuadTree createInstance(final Rectangle bounds) {
		return createInstance(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	protected DynamicQuadTree(final Vector2 center, final SizeF halfDimension, final int depth, final DynamicQuadTree parentTree) {
		super(center, halfDimension, depth);
		this.fParentTree = parentTree;
	}
	
	protected boolean insertUp(final Body collidable) {
		if (fParentTree != null) {
			return fParentTree.insertUp(collidable);
		} else {
			return insert(collidable);
		}
	}
	
	/**
	 * @see QuadTree#divide()
	 */
	protected void divide() {
		if (fSubTrees != null)
			return;
		
		final Vector2 currentCenter = Vector2.emptyVector();
		final SizeF currentSize = new  SizeF(fHalfDimension.width * .5f, fHalfDimension.height * .5f);
		
		fSubTrees = new DynamicQuadTree[4];
		
		currentCenter.x = getCenter().x - currentSize.width;
		currentCenter.y = getCenter().y - currentSize.height;
		fSubTrees[NW] = new DynamicQuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1, this);

		currentCenter.x += fHalfDimension.width;
		fSubTrees[NE] = new DynamicQuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1, this);

		currentCenter.y += fHalfDimension.height;
		fSubTrees[SE] = new DynamicQuadTree(currentCenter.clone(), currentSize, fCurrentDepth+1, this);
		
		currentCenter.x -= fHalfDimension.width;
		fSubTrees[SW] = new DynamicQuadTree(currentCenter, currentSize, fCurrentDepth+1, this);
	}

	@Override
	public void update(final long deltaTime, final long totalTime) {
		if (fSubTrees != null) {
			for (int i = 0; i < fSubTrees.length; i++) {
				((DynamicQuadTree)fSubTrees[i]).update(deltaTime, totalTime);
			}
		}
		
		rem.clear();
		ins.clear();
		
		final Iterator<Body> coll = fCollidableList.iterator();
		while (coll.hasNext()) {
			final Body obj = coll.next();
			if (contains(obj.computeAABB())) {
				if (fSubTrees != null) {
					for (final IQuadTree tree : fSubTrees) {
						// If a branch is able to take the object, remove it
						// if not, we leave it in this tree since it's the smallest that can contain the object
						if (tree.insert(obj)) {
							//fCollidableList.remove(obj);
							rem.add(obj);
						}
					}
				}
			} else {
				ins.add(obj);
			}
		}
		
		for (final Body obj : ins) {
			if (insertUp(obj)) {
				//rem.add(obj);
				fCollidableList.remove(obj);
			}
		}
		
		for (final Body obj : rem) {
			fCollidableList.remove(obj);
		}
	}
}
