package de.paeterick.piframework.physics.collisiondetection;

import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;

public class CollisionPolygonCircle implements CollisionCallback {
	
	public static final CollisionPolygonCircle instance = new CollisionPolygonCircle();
	
	private CollisionPolygonCircle(){};

	public void handleCollision(final Manifold m, final Body a, final Body b) {
		CollisionCirclePolygon.instance.handleCollision(m, b, a);
		
		if (m.contactCount > 0) {
			m.normal.negate();
		}
	}
}
