package de.paeterick.piframework.physics.collisiondetection;

import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;

public class AABB {
	/** Center of the object */
	protected Vector2 fCenter;
	/**
	 * Half dimension of the object<br />
	 * <br />
	 * The Rectangle to be calculated with<br />
	 * will be made like this<br />
	 * <br />
	 * x|y: center.x - halfDimension.width | center.y - halfDimension.height<br />
	 * width|height  - halfDimension.width * 2 | halfDimension.height * 2
	 */
	protected SizeF fHalfDimension;
	
	public AABB(final Vector2 center, final SizeF halfDimension) {
		this.fCenter = center;
		this.fHalfDimension = halfDimension;
	}
	
	public static AABB fromAbsolute(final Vector2 position, final SizeF size) {
		return new AABB(
				new Vector2(position.x + (size.width * .5f), position.y + (size.height * .5f)),
				new SizeF(size.width * .5f, size.height * .5f));
	}
	
	public static AABB fromCoords(final Vector2 edge, final Vector2 transverseEdge) {
		final float x1 = edge.x < transverseEdge.x ? edge.x : transverseEdge.x;
		final float y1 = edge.y < transverseEdge.y ? edge.y : transverseEdge.y;
		final float x2 = edge.x > transverseEdge.x ? edge.x : transverseEdge.x;
		final float y2 = edge.y > transverseEdge.y ? edge.y : transverseEdge.y;
		
		final float halfDimensionX = (x2 - x1) * .5f;
		final float halfDimensionY = (y2 - y1) * .5f;
		
		return new AABB(new Vector2(x1 + halfDimensionX, y1 + halfDimensionY), new SizeF(halfDimensionX, halfDimensionY));
	}
	
	public Vector2 getCenter() {
		return fCenter;
	}

	public SizeF getHalfDimension() {
		return fHalfDimension;
	}

	public float getTop() {
		return fCenter.y - fHalfDimension.height;
	}
	public float getLeft() {
		return fCenter.x - fHalfDimension.width;
	}
	public float getBottom() {
		return fCenter.y + fHalfDimension.height;
	}
	public float getRight() {
		return fCenter.x + fHalfDimension.width;
	}
	public float getWidth() {
		return fHalfDimension.width * 2.0f;
	}
	public float getHeight() {
		return fHalfDimension.height * 2.0f;
	}
	
	/**
	 * Checks if the collidable contains a point
	 * @param point the point to be checked
	 * @return true if collidable contains the point
	 */
	// This is a very fast method, it should be called more often
	// Its 1500 nanoseconds faster than a simple lt gt check (9000 ns in average on test system (i5))
	public boolean contains(final Vector2 point) {
		return ((point.x > getLeft()) &&
			(point.x < getRight()) &&
			(point.y > getTop()) &&
			(point.y < getBottom()));
	}
	/**
	 * Checks if one {@link AABB} contains another {@link AABB} 
	 * @param obj  The object to check if inside this object
	 * @return true if the given {@link AABB} is inside this object
	 */
	public boolean contains(final AABB obj) {
		return contains(new Vector2(obj.getLeft(), obj.getTop())) && contains(new Vector2(obj.getRight(), obj.getBottom()));
	}
	
	/**
	 * Checks if one collidable intersects with another
	 * @param collidable the Collidable to be checked for intersection
	 * @return true if the Collidables intersect
	 */
	public boolean intersectsWith(final AABB collidable) {
		return !(
			(collidable.getLeft()) > (getRight()) ||
			(collidable.getRight()) < (getLeft()) ||
			(collidable.getTop()) > (getBottom()) ||
			(collidable.getBottom()) < (getTop()));
	}
}
