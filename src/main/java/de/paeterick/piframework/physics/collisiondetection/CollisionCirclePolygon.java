package de.paeterick.piframework.physics.collisiondetection;

import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;
import de.paeterick.piframework.physics.shapes.Circle;
import de.paeterick.piframework.physics.shapes.Polygon;

public class CollisionCirclePolygon implements CollisionCallback {
	
	public static final CollisionCirclePolygon instance = new CollisionCirclePolygon();
	
	private CollisionCirclePolygon(){}

	public void handleCollision(final Manifold m, final Body a, final Body b) {
		final Circle A = (Circle)a.shape;
		final Polygon B = (Polygon)b.shape;
		
		m.contactCount = 0;
		
		final Vector2 center = a.position.subtractNew(b.position).transform(B.u.transposeNew());
		
		float separation = -Float.MAX_VALUE;
		int faceNormal = 0;
		
		for (int i = 0; i < B.vertexCount; ++i) {
			final float s = B.normals[i].dotProduct(center.subtractNew(B.vertices[i]));
			
			if (s > A.radius)
				return;
			
			if (s > separation) {
				separation = s;
				faceNormal = i;
			}
		}
		
		final Vector2 v1 = B.vertices[faceNormal];
		int i2 = faceNormal + 1 < B.vertexCount ? faceNormal + 1 : 0;
		final Vector2 v2 = B.vertices[i2];
		
		if (separation < MathHelper.EPSILON) {
			m.contactCount = 1;
			m.normal.set(B.normals[faceNormal].transformNew(B.u).negate());
			m.contacts[0].set(m.normal).multiply(A.radius).add(a.position);
			m.penetration = A.radius;
			return;
		}
		
		float dot1 = center.subtractNew(v1).dotProduct(v2.subtractNew(v1));
		float dot2 = center.subtractNew(v2).dotProduct(v1.subtractNew(v2));
		m.penetration = A.radius - separation;
		
		if (dot1 <= 0.0f) {
			if (MathHelper.distanceSq(center, v1) > A.radius * A.radius)
				return;
			
			m.contactCount = 1;
			m.normal.set(v1).subtract(center).transform(B.u).normalize();
			m.contacts[0].set(v1.transformNew(B.u)).add(b.position);
		} else if (dot2 <= 0.0f) {
			if (MathHelper.distanceSq(center, v2) > A.radius * A.radius)
				return;
			
			m.contactCount = 1;
			
			m.normal.set(v2).subtract(center).transform(B.u).normalize();
			m.contacts[0].set(v2.transformNew(B.u)).add(b.position);
		} else {
			final Vector2 n = B.normals[faceNormal];
			if (center.subtractNew(v1).dotProduct(n) > A.radius)
				return;
			
			m.contactCount = 1;
			m.normal.set(n.transformNew(B.u)).negate();
			m.contacts[0].set(a.position).add(m.normal.multiplyNew(A.radius));
		}
	}
}
