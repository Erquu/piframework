package de.paeterick.piframework.physics.collisiondetection;

import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Matrix2;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.Body;
import de.paeterick.piframework.physics.collisiondetection.broad.Manifold;
import de.paeterick.piframework.physics.shapes.Polygon;

public class CollisionPolygonPolygon implements CollisionCallback {
	
	public static final CollisionPolygonPolygon instance = new CollisionPolygonPolygon();
	
	private CollisionPolygonPolygon(){}

	public void handleCollision(final Manifold m, final Body a, final Body b) {
		final Polygon A = (Polygon)a.shape;
		final Polygon B = (Polygon)b.shape;
		m.contactCount = 0;
		
		int[] faceA = { 0 };
		float penetrationA = findAxisLeastPenetration(faceA, A, B);
		
		if (penetrationA >= 0.0f)
			return;
		
		int[] faceB = { 0 };
		float penetrationB = findAxisLeastPenetration(faceB, B, A);
		
		if (penetrationB >= 0.0f)
			return;
		
		int referenceIndex;
		final boolean flip;
		
		final Polygon refPoly;
		final Polygon incPoly;
		
		if (MathHelper.greaterThan(penetrationA, penetrationB)) {
			refPoly = A;
			incPoly = B;
			referenceIndex = faceA[0];
			flip = false;
		} else {
			refPoly = B;
			incPoly = A;
			referenceIndex = faceB[0];
			flip = true;
		}
		
		final Vector2[] incidentFace = Vector2.arrayOf(2);
		findIncidentFace(incidentFace, refPoly, incPoly, referenceIndex);
		
		Vector2 v1 = refPoly.vertices[referenceIndex];
		referenceIndex = referenceIndex + 1 == refPoly.vertexCount ? 0 : referenceIndex + 1;
		Vector2 v2 = refPoly.vertices[referenceIndex];
		
		v1 = v1.transformNew(refPoly.u).add(refPoly.body.position);
		v2 = v2.transformNew(refPoly.u).add(refPoly.body.position);
		
		final Vector2 sidePlaneNormal = v2.subtractNew(v1);
		sidePlaneNormal.normalize();
		
		final Vector2 refFaceNormal = new Vector2(sidePlaneNormal.y, -sidePlaneNormal.x);
		
		float refC = refFaceNormal.dotProduct(v1);
		float negSide = -sidePlaneNormal.dotProduct(v1);
		float posSide = sidePlaneNormal.dotProduct(v2);
		
		if (clip(sidePlaneNormal.negateNew(), negSide, incidentFace) < 2) {
			return;
		}
		
		if (clip(sidePlaneNormal, posSide, incidentFace) < 2) {
			return;
		}
		
		m.normal.set(refFaceNormal);
		
		if (flip)
			m.normal.negate();
		
		int cp = 0;
		float separation = refFaceNormal.dotProduct(incidentFace[0]) - refC;
		
		if (separation <= 0.0f) {
			m.contacts[cp] = incidentFace[0].clone(); // TODO: Check if a clone is needed
			m.penetration = -separation;
			cp++;
		} else {
			m.penetration = 0;
		}
		
		separation = refFaceNormal.dotProduct(incidentFace[1]) - refC;
		
		if (separation <= 0.0f) {
			m.contacts[cp] = incidentFace[1].clone(); // TODO: Check if a clone is needed
			// TODO: check -= separation
			m.penetration += -separation;
			cp++;
			
			m.penetration /= cp;
		}
		
		m.contactCount = cp;
	}

	public float findAxisLeastPenetration(final int[] faceIndex, final Polygon A, final Polygon B) {
		float bestDistance = -Float.MAX_VALUE;
		int bestIndex = 0;
		
		for (int i = 0; i < A.vertexCount; i++) {
			final Vector2 nw = A.normals[i].transformNew(A.u);
			final Matrix2 buT = B.u.transposeNew();
			final Vector2 n = nw.transformNew(buT);
			final Vector2 s = B.getSupport(n.negateNew());
			
			final Vector2 v = A.vertices[i].transformNew(A.u);
			v.add(A.body.position);
			v.subtract(B.body.position);
			v.transform(buT);

			float d = n.dotProduct(s.subtractNew(v));
			
			if (d > bestDistance) {
				bestDistance = d;
				bestIndex = i;
			}
		}
		faceIndex[0] = bestIndex;
		
		return bestDistance;
	}

	public void findIncidentFace(final Vector2[] v, final Polygon refPoly, final Polygon incPoly, int referenceIndex) {
		
		Vector2 referenceNormal = refPoly.normals[referenceIndex];
		referenceNormal = referenceNormal.transformNew(refPoly.u);
		referenceNormal = referenceNormal.transformNew(incPoly.u.transposeNew());
		
		int incidentFace = 0;
		float minDot = Float.MAX_VALUE;
		for (int i = 0; i < incPoly.vertexCount; ++i) {
			final float dot = referenceNormal.dotProduct(incPoly.normals[i]);
			
			if (dot < minDot) {
				minDot = dot;
				incidentFace = i;
			}
		}
		
		v[0] = incPoly.vertices[incidentFace].transformNew(incPoly.u).add(incPoly.body.position);
		incidentFace = incidentFace + 1 >= (int)incPoly.vertexCount ? 0 : incidentFace + 1;
		v[1] = incPoly.vertices[incidentFace].transformNew(incPoly.u).add(incPoly.body.position);
	}
	
	public int clip(final Vector2 n, final float c, final Vector2[] face) {
		int sp = 0;
		final Vector2[] out = {
				face[0].clone(),
				face[1].clone()
		};
		
		float d1 = n.dotProduct(face[0]) - c;
		float d2 = n.dotProduct(face[1]) - c;
		
		if (d1 <= 0.0f)
			out[sp++].set(face[0]);
		if (d2 <= 0.0f)
			out[sp++].set(face[1]);
		
		if (d1 * d2 < 0.0f) {
			final float alpha = d1 / (d1 - d2);
			out[sp++] = face[1].subtractNew(face[0]).multiply(alpha).add(face[0]);
		}
		
		face[0] = out[0];
		face[1] = out[1];
		
		return sp;
	}
}
