package de.paeterick.piframework.math;

/**
 * Size implementation containing width and height information
 * @author Erquu
 */
public class SizeF {
	/**
	 * Returns an empty SizeF object (width:0 height:0)
	 * @return an empty SizeF object (width:0 height:0)
	 */
	public static SizeF emptySize() { return new SizeF(); }
	
	/** The width */
	public float width = 0f;
	/** The height */
	public float height = 0f;
	
	/** Initializes an empty SizeF */ 
	public SizeF() { }
	/** Initializes a SizeF with the given values */
	public SizeF(final int width, final int height) {
		this.width = (float)width;
		this.height = (float)height;
	}
	/** Initializes a SizeF with the given values */
	public SizeF(final float width, final float height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Creates a copy of this object
	 * @return a copy of this object
	 */
	public SizeF copy() {
		return new SizeF(width, height);
	}
	
	@Override
	public String toString() {
		return String.format("width:%s height:%s", width, height);
	}
}
