package de.paeterick.piframework.math;

/**
 * Really simple Vector implementation
 * @author Erquu
 */
public class Vector2 {
	
	/**
	 * Returns an empty Vector2 (x:0 y:0)
	 * @return an empty Vector2 (x:0 y:0)
	 */
	public static Vector2 emptyVector() {
		return new Vector2(0, 0);
	}
	
	public static Vector2[] arrayOf(final int length) {
		final Vector2[] va = new Vector2[length];
		for (int i = 0; i < length; i++) {
			va[i] = Vector2.emptyVector();
		}
		return va;
	}
	
	/** X Coordinate */
	public float x;
	/** Y Coordinate */
	public float y;
		
	/**
	 * Initializes an empty Vector
	 */
	public Vector2() {
		x = y = 0;
	}
	/**
	 * Initializes a Vector with the given values
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public Vector2(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2 set(float x, float y) {
		this.x = x;
		this.y = y;
		
		return this;
	}
	
	public Vector2 set(final Vector2 v) {
		this.x = v.x;
		this.y = v.y;
		
		return this;
	}
	
	public Vector2 add(final float s) {
		this.x += s;
		this.y += s;
		
		return this;
	}
	public Vector2 add(final Vector2 s) {
		this.x += s.x;
		this.y += s.y;
		
		return this;
	}
	
	public Vector2 addNew(final float s) {
		return clone().add(s);
	}
	public Vector2 addNew(final Vector2 s) {
		return clone().add(s);
	}
	
	public Vector2 subtract(final Vector2 s) {
		this.x -= s.x;
		this.y -= s.y;
		
		return this;
	}
	public Vector2 subtractNew(final Vector2 s) {
		return clone().subtract(s);
	}
	
	public Vector2 multiply(final float s) {
		this.x *= s;
		this.y *= s;
		
		return this;
	}
	public Vector2 multiplyNew(final float s) {
		return clone().multiply(s);
	}
	
	public Vector2 multiply(final Vector2 s) {
		this.x *= s.x;
		this.y *= s.y;
		
		return this;
	}
	public Vector2 multiplyNew(final Vector2 s) {
		return clone().multiply(s);
	}
	
	public Vector2 divide(final float s) {
		this.x /= s;
		this.y /= s;
		
		return this;
	} 
	public Vector2 divideNew(final float s) {
		return clone().divide(s);
	}
	
	public Vector2 negate() {
		x = -x;
		y = -y;
		
		return this;
	}
	public Vector2 negateNew() {
		return clone().negate();
	}
	
	public float lengthSq() {
		return (x * x + y * y);
	}
	
	/**
	 * Computes the length of the vector
	 * @return the length of the vector
	 */
	public float length() {
		return (float)StrictMath.sqrt((x * x + y * y));
	}
	
	/**
	 * Normalizes the vector
	 */
	public void normalize() {
		final float lenSq = lengthSq();

		if (lenSq > MathHelper.EPSILON_SQ)
		{
			float invLen = 1.0f / (float)StrictMath.sqrt( lenSq );
			x *= invLen;
			y *= invLen;
		}
	}
	
	/**
	 * Returns the dot product of two vectors
	 * @param b The vector to calculate with
	 * @return The dot product of two vectors
	 */
	public float dotProduct(final Vector2 b) {
		return (this.x * b.x) + (this.y * b.y);
	}
	
	public float crossProduct(final Vector2 b) {
		return this.x * b.y - this.y * b.x;
	}
	
	public Vector2 transform(final Matrix2 m) {
		final float tmpX = x;
		final float tmpY = y;
		
		x = m.M11 * tmpX + m.M12 * tmpY;
		y = m.M21 * tmpX + m.M22 * tmpY;
		
		return this;
	}
	public Vector2 transformNew(final Matrix2 m) {
		return clone().transform(m);
	}
	
	public static Vector2 cross(final Vector2 v, final float a) {
		return new Vector2(
			v.y * a,
			v.x * -a
		);
	}
	public static Vector2 cross(final float a, final Vector2 v) {
		return new Vector2(
				v.y * -a,
				v.x * a
			);
	}
	
	public void clamp(final float max) {
		float i = max / this.length();
		i = i < 1.0f ? i : 1.0f;
		this.multiply(i);
	}
	
	/**
	 * Creates an exact copy of the current Vector2
	 * @return an exact copy of the current Vector2
	 */
	public Vector2 clone() {
		return new Vector2(x, y);
	}
	
	@Override
	public String toString() {
		return String.format("x:%s y:%s", x, y); 
	}
	
	
	
	
	public Vector2 addsi( Vector2 v, float s )
	{
		return adds( v, s, this );
	}

	/**
	 * Sets out to the addition of this vector and v * s and returns out.
	 */
	public Vector2 adds( Vector2 v, float s, Vector2 out )
	{
		out.x = x + v.x * s;
		out.y = y + v.y * s;
		return out;
	}
}
