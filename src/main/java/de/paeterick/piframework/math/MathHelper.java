package de.paeterick.piframework.math;

import java.util.Random;

/**
 * Collection of useful math functions
 * @author Erquu
 */
public class MathHelper {
	
	private static final Random random = new Random(System.nanoTime());

	public static final float E = 2.7182818284590451f;
	/** Own implementation of PI */
	public static final float PI = 3.1415926535897931f;
	
	public static final float EPSILON = 0.0001f;
	public static final float EPSILON_SQ = EPSILON * EPSILON;
	
	/** PI divided by two */
	public static final float PiOver2 = (MathHelper.PI / 2.0f);
	/** PI divided by three */
	public static final float PiOver4 = (MathHelper.PI / 4.0f);
	
	/** PI multiplied with 2 */
	public static final float TwoPI = (MathHelper.PI * 2.0f);
	/** PI multiplied with 3 */
	public static final float ThreePI = (MathHelper.PI * 3.0f);
	/** PI multiplied with 4 */
	public static final float FourPI = (MathHelper.PI * 4.0f);
	
	/**
	 * Checks if a value is between a minimum and a maximum range
	 * @param value the value to be checked
	 * @param min the minimum inclusive value
	 * @param max the maximum exclusive value
	 * @return true if value is in range
	 */
	public static boolean isBetween(final float value, final float min, final float max) {
		return (value >= min && value < max);
	}
	/**
	 * Checks if a value is between a minimum and a maximum range
	 * @param value the value to be checked
	 * @param min the minimum inclusive value
	 * @param max the maximum exclusive value
	 * @return true if value is in range
	 */
	public static boolean isBetween(final double value, final double min, final double max) {
		return (value >= min && value < max);
	}
	/**
	 * Checks if a value is between a minimum and a maximum range
	 * @param value the value to be checked
	 * @param min the minimum inclusive value
	 * @param max the maximum exclusive value
	 * @return true if value is in range
	 */
	public static boolean isBetween(final int value, final int min, final int max) {
		return (value >= min && value < max);
	}
	
	/**
	 * Calculates the distance between two vectors
	 * @param v1 the start point
	 * @param v2 the end point
	 * @return the distance between two vectors
	 */
	public static float distance(final Vector2 v1, final Vector2 v2) {
		return distance(v1.x, v1.y, v2.x, v2.y);
	}
	/**
	 * Calculates the distance between two points
	 * @param x1 the start x coordinate
	 * @param y1 the start y coordinate
	 * @param x2 the end x coordinate
	 * @param y2 the end y coordinate
	 * @return the distance between two points
	 */
	public static float distance(final float x1, final float y1, final float x2, final float y2) {
		final float a = x1 - x2;
		final float b = y1 - y2;
		return ((float)Math.sqrt(a * a + b * b));
	}
	
	public static float distanceSq(final Vector2 v1, final Vector2 v2) {
		return distanceSq(v1.x, v1.y, v2.x, v2.y);
	}
	public static float distanceSq(final float x1, final float y1, final float x2, final float y2) {
		final float dx = x1 - x2;
		final float dy = y1 - y2;
		
		return dx * dy + dy * dy;
	}
	
	public static boolean equals(final float left, final float right) {
		return Math.abs(left - right) <= EPSILON;
	}
	
	/**
	 * Calculates the angle between two vectors (in radians)
	 * @param v1 the start point
	 * @param v2 the end point
	 * @return the angle between two vectors (in radians)
	 */
	public static float angleBetween(final Vector2 v1, final Vector2 v2)
    {
		final float x = v2.x - v1.x;
		final float y = v2.y - v1.y;
        return ((float)Math.atan2(x, -y) - MathHelper.PiOver2);
    }
	
	/**
	 * Calculates the velocity from an angle
	 * @param angle the angle to be proceeded
	 * @return the velocity as a vector
	 */
	public static Vector2 angleToVelocity(final float angle)
    {
        return new Vector2((float)Math.cos((double)angle), (float)Math.sin((double)angle));
    }
	
	/**
	 * Calculates the velocity between two vectors
	 * @param v1 the start point
	 * @param v2 the end point
	 * @return the velocity between two vectors
	 */
	public static Vector2 velocityBetween(final Vector2 v1, final Vector2 v2)
    {
        return angleToVelocity(angleBetween(v1, v2));
    }
	
	/**
	 * Converts radians into degrees
	 * @param radians the radians to be converted
	 * @return the converted radians
	 */
	public static float radiansToDegrees(final float radians)
    {
        return radians * (180.0f / MathHelper.PI);
    }
	
	/**
	 * Converts degrees into radians
	 * @param degrees the degrees to be converted
	 * @return the converted degrees
	 */
	public static float degreesToRadians(final float degrees)
    {
        return degrees * (MathHelper.PI / 180.0f);
    }
	
	/**
	 * Returns a random floating value
	 * @param min the minimum value
	 * @param max the maximum value
	 * @return a random floating value
	 */
	public static float randomFloat(final float min, final float max)
    {
        return ((max - min) * ((float)random.nextDouble()) + min);
    }
	
	/**
	 * Returns a random integer
	 * @param min the minimum value
	 * @param max the maximum exclusive value
	 * @returna random integer value
	 */
	public static int randomInteger(final int min, final int max) {
		return min + random.nextInt(max);
	}
	
	/**
	 * Returns a vector with random x and y values
	 * @param min the minimum x and y
	 * @param max the maximum x and y
	 * @return a vector with random x and y values
	 */
	public static Vector2 randomVector2(final float min, final float max)
    {
        return randomVector2(min, max, min, max);
    }
	/**
	 * Returns a vector with random x and y values
	 * @param minX the minimum x value
	 * @param maxX the maximum x value
	 * @param minY the minimum y value
	 * @param maxY the maximum y value
	 * @return a vector with random x and y values
	 */
	public static Vector2 randomVector2(final float minX, final float maxX, final float minY, final float maxY)
    {
        return new Vector2(MathHelper.randomFloat(minX, maxX), MathHelper.randomFloat(minY, maxY));
    }
	
	/**
	 * Returns a value within a minimum and a maximum range
	 * @param value the value to be checked
	 * @param min the minumum possible value (inclusive)
	 * @param max the maximum possible value (inclusive)
	 * @return a value within a minimum and a maximum range
	 */
	public static float clamp(final float value, final float min, final float max)
    {
        return value < min ? min : (value > max ? max : value);
    }
	/**
	 * Returns a value within a minimum and a maximum range
	 * @param value the value to be checked
	 * @param min the minimum possible value (inclusive)
	 * @param max the maximum possible value (inclusive)
	 * @return a value within a minimum and a maximum range
	 */
	public static int clamp(final int value, final int min, final int max)
    {
        return value < min ? min : (value > max ? max : value);
    }

	/**
	 * Returns the cosinus of a given angle
	 * @param d the angle in radians
	 * @return the cosinus of d
	 */
	public static float cos(final float d) {
		return (float)Math.cos((double)d);
	}
	
	/**
	 * Returns the sinus of a given angle
	 * @param d the angle in radians
	 * @return the sinus of d
	 */
	public static float sin(final float d) {
		return (float)Math.sin((double)d);
	}
	
	/**
	 * Returns the tangens of a given angle
	 * @param d the angle in radians
	 * @return the tangens of d
	 */
	public static float tan(final float d) {
		return (float)Math.tan((double)d);
	}

	/**
	 * Checks if value a is greater than value b
	 * @param a
	 * @param b
	 * @return true if "a" is greater than "b"
	 */
	public static boolean greaterThan(final float a, final float b) {
		return a >= b * .95f + a * .01f;
	}
}
