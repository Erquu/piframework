package de.paeterick.piframework.math;

public class LineSegment2 {
	
	public static enum Orientation {
		COLINEAR,
		CLOCKWISE,
		COUNTERCLOCKWISE
	}
	
	/** Start point on X axis */
	public float x1;
	/** Start point on Y axis */
	public float y1;
	/** End point on X axis */
	public float x2;
	/** End point on Y axis */
	public float y2;
	
	public LineSegment2() {
		this.x1 = this.x2 = this.y1 = this.y2 = 0.0f;
	}
	public LineSegment2(final float x1, final float y1, final float x2, final float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	/**
	 * Checks if a line is parallel to another line segment
	 * @param line The line segment to check against
	 * @return true if line segments are parallel
	 */
	public boolean isParallel(final LineSegment2 line) {
		final float xa = this.x2 - this.x1;
		final float ya = this.y2 - this.y1;
		final float xb = line.x2 - line.x1;
		final float yb = line.y2 - line.y1;
		
		return ((xa == xb) && (ya == yb));
	}
	
	public boolean pointOnSegment(final Vector2 v) {
		return (this.x2 <= Math.max(this.x1, v.x) &&
				this.x2 >= Math.min(this.x1, v.x) &&
				this.y2 <= Math.max(this.y1, v.y) &&
				this.y2 >= Math.min(this.y1, v.y));
	}
	
	public boolean intersects(final LineSegment2 line) {
		final Orientation o1 = findOrientation(line.x1, line.y1);
		final Orientation o2 = findOrientation(line.x2, line.y2);
		final Orientation o3 = line.findOrientation(this.x1, this.y1);
		final Orientation o4 = line.findOrientation(this.x2, this.y2);
		
		if (o1 != o2 && o3 != o4)
			return true;
		
		return false;
	}
	
	public Orientation findOrientation(final float x, final float y) {
		final float val = (this.y2 - this.y1) * (x - this.x2) - (this.x2 - this.x1) * (y - this.y2);
		
		if (val == 0)
			return Orientation.COLINEAR;
		return (val > 0) ? Orientation.CLOCKWISE : Orientation.COUNTERCLOCKWISE;
	}
	public Orientation findOrientation(final Vector2 v) {
		return findOrientation(v.x, v.y);
	}
}
