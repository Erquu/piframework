package de.paeterick.piframework.math;

/**
 * Size implementation containing width and height information
 * @author Erquu
 */
public class Size {
	/**
	 * Returns an empty SizeF object (width:0 height:0)
	 * @return an empty SizeF object (width:0 height:0)
	 */
	public static Size emptySize() { return new Size(); }
	
	/** The width */
	public int width = 0;
	/** The height */
	public int height = 0;
	
	/** Initializes an empty SizeF */ 
	public Size() { }
	/** Initializes a SizeF with the given values */
	public Size(final int width, final int height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Creates a copy of this object
	 * @return a copy of this object
	 */
	public Size copy() {
		return new Size(width, height);
	}
	
	@Override
	public String toString() {
		return String.format("width:%s height:%s", width, height);
	}
}
