package de.paeterick.piframework.math;

/**
[11 12]<br />
[21 22]
*/
public class Matrix2 {
	
	public static Matrix2 identityMatrix() {
		return new Matrix2(1, 0, 0, 1);
	}
	
	public float M11, M12;
	public float M21, M22;
	
	public Matrix2() {
		
	}
	public Matrix2(final float radians) {
		set(radians);
	}
	public Matrix2(final float M11, final float M12, final float M21, final float M22) {
		set(M11, M12, M21, M22);
	}
	
	public void set(final float radians) {
		final float c = (float)StrictMath.cos(radians);
		final float s = (float)StrictMath.sin(radians);
		
		M11 = c;
		M12 = -s;
		M21 = s;
		M22 = c;
	}
	public void set(final float M11, final float M12, final float M21, final float M22) {
		this.M11 = M11;
		this.M12 = M12;
		this.M21 = M21;
		this.M22 = M22;
	}
	public void set(final Matrix2 m) {
		this.M11 = m.M11;
		this.M12 = m.M12;
		this.M21 = m.M21;
		this.M22 = m.M22;
	}

	public Matrix2 multiply(final Matrix2 m) {
		M11 = M11 * m.M11 + M12 * m.M21;
		M12 = M11 * m.M12 + M12 * m.M22;
		M21 = M21 * m.M11 + M22 * m.M21;
		M22 = M21 * m.M12 + M22 * m.M22;
		
		return this;
	}
	public Matrix2 multiplyNew(final Matrix2 m) {
		return clone().multiply(m);
	}
	
	public Matrix2 transpose() {
		float t = M12;
		M12 = M21;
		M21 = t;
		
		return this;
	}
	public Matrix2 transposeNew() {
		return clone().transpose();
	}
	
	public Matrix2 clone() {
		return new Matrix2(M11, M12, M21, M22);
	}
}
