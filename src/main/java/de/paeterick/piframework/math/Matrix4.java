package de.paeterick.piframework.math;

/**
 * Matrix (4x4)<br />
 * <br />
 * [M11 M12 M13 M14]<br />
 * [M21 M22 M23 M24]<br />
 * [M31 M32 M33 M34]<br />
 * [M41 M42 M43 M44]
 * 
 * @author csp8fe
 */
public class Matrix4 {
	/*
    [11 12 13 14]
    [21 22 23 24]
    [31 32 33 34]
    [41 42 43 44]
    */

	public float M11;
	public float M12;
	public float M13;
	public float M14;
	public float M21;
	public float M22;
	public float M23;
	public float M24;
	public float M31;
	public float M32;
	public float M33;
	public float M34;
	public float M41;
	public float M42;
	public float M43;
	public float M44;
	
	/**
	 * Returns an empty Matrix<br />
	 * [0 0 0 0]<br />
	 * [0 0 0 0]<br />
	 * [0 0 0 0]<br />
	 * [0 0 0 0]
	 * @return an empty Matrix
	 */
	public static Matrix4 zeroMatrix() {
		
		final Matrix4 zMatrix = new Matrix4();
		zMatrix.M11 = zMatrix.M12 = zMatrix.M13 = zMatrix.M14 = 0;
		zMatrix.M21 = zMatrix.M22 = zMatrix.M23 = zMatrix.M24 = 0;
		zMatrix.M31 = zMatrix.M32 = zMatrix.M33 = zMatrix.M34 = 0;
		zMatrix.M41 = zMatrix.M42 = zMatrix.M43 = zMatrix.M44 = 0;
		
		return zMatrix;
	}

	/**
	 * Returns an identity Matrix<br />
	 * [1 0 0 0]<br />
	 * [0 1 0 0]<br />
	 * [0 0 1 0]<br />
	 * [0 0 0 1]
	 * @return an identity Matrix
	 */
	public static Matrix4 identityMatrix() {
		
		final Matrix4 idM = Matrix4.zeroMatrix();
		idM.M11 = idM.M22 = idM.M33 = idM.M44 = 1.0f;
		
		return idM;
	}
	
	public static Matrix4 homogeneousMatrix() {
		
		final Matrix4 hM = Matrix4.zeroMatrix();
		hM.M11 = hM.M22 = hM.M33 = hM.M43 = 1.0f;
		
		return hM;
	}
	
	/**
	 * Unstable
	 * @param near the new viewing plane
	 * @param far the far viewing plane
	 * @return the perspective Matrix
	 */
	public static Matrix4 perspectiveMatrix(final float near, final float far) {
		final Matrix4 pMatrix = Matrix4.zeroMatrix();
		pMatrix.M11 = pMatrix.M22 = 1.0f;
		pMatrix.M33 = ((near + far) / near);
		pMatrix.M34 = -far;
		pMatrix.M43 = (1.0f / near);
		
		return pMatrix;
	}
	
	//
	// MATH OPERATIONS
	//
	
	/**
	 * Returns the determinant for the matrix
	 * @return the determinant for the matrix
	 */
	public float getDeterminant() {
		return (M11 * M22 * M33 * M44) + (M12 * M23 * M34 * M41) + (M13 * M24 * M31 * M42) + (M14 * M21 * M32 * M43)
			   - (M41 * M32 * M23 * M14) - (M42 * M33 * M24 * M11) - (M43 * M34 * M21 * M12) - (M44 * M31 * M22 * M13);
	}
	
	/**
	 * Adds the values of a given Matrix to this matrix
	 * @param right the Matrix to be added
	 */
	public void add(final Matrix4 right) {
		M11 += right.M11;
		M12 += right.M12;
		M13 += right.M13;
		M14 += right.M14;
		
		M21 += right.M21;
		M22 += right.M22;
		M23 += right.M23;
		M24 += right.M24;
		
		M31 += right.M31;
		M32 += right.M32;
		M33 += right.M33;
		M34 += right.M34;
		
		M41 += right.M41;
		M42 += right.M42;
		M43 += right.M43;
		M44 += right.M44;
	}

	/**
	 * Subtracts the values of a given Matrix to this matrix
	 * @param right the Matrix to be subtracted
	 */
	public void subtract(final Matrix4 right) {
		M11 -= right.M11;
		M12 -= right.M12;
		M13 -= right.M13;
		M14 -= right.M14;
		
		M21 -= right.M21;
		M22 -= right.M22;
		M23 -= right.M23;
		M24 -= right.M24;
		
		M31 -= right.M31;
		M32 -= right.M32;
		M33 -= right.M33;
		M34 -= right.M34;
		
		M41 -= right.M41;
		M42 -= right.M42;
		M43 -= right.M43;
		M44 -= right.M44;
	}
	
	/**
	 * Multiplies a matrix with this instance
	 * @param right the matrix to be multiplied withh
	 */
	public void multiply(final Matrix4 right) {
		
		final float M11 = this.M11 * right.M11 + this.M12 * right.M21 + this.M13 * right.M31 + this.M14 * right.M41;
		final float M12 = this.M11 * right.M12 + this.M12 * right.M22 + this.M13 * right.M32 + this.M14 * right.M42;
		final float M13 = this.M11 * right.M13 + this.M12 * right.M23 + this.M13 * right.M33 + this.M14 * right.M43;
		final float M14 = this.M11 * right.M14 + this.M12 * right.M24 + this.M13 * right.M34 + this.M14 * right.M44;

		final float M21 = this.M21 * right.M11 + this.M22 * right.M21 + this.M23 * right.M31 + this.M24 * right.M41;
		final float M22 = this.M21 * right.M12 + this.M22 * right.M22 + this.M23 * right.M32 + this.M24 * right.M42;
		final float M23 = this.M21 * right.M13 + this.M22 * right.M23 + this.M23 * right.M33 + this.M24 * right.M43;
		final float M24 = this.M21 * right.M14 + this.M22 * right.M24 + this.M23 * right.M34 + this.M24 * right.M44;

		final float M31 = this.M31 * right.M11 + this.M32 * right.M21 + this.M33 * right.M31 + this.M34 * right.M41;
		final float M32 = this.M31 * right.M12 + this.M32 * right.M22 + this.M33 * right.M32 + this.M34 * right.M42;
		final float M33 = this.M31 * right.M13 + this.M32 * right.M23 + this.M33 * right.M33 + this.M34 * right.M43;
		final float M34 = this.M31 * right.M14 + this.M32 * right.M24 + this.M33 * right.M34 + this.M34 * right.M44;

		final float M41 = this.M41 * right.M11 + this.M42 * right.M21 + this.M43 * right.M31 + this.M44 * right.M41;
		final float M42 = this.M41 * right.M12 + this.M42 * right.M22 + this.M43 * right.M32 + this.M44 * right.M42;
		final float M43 = this.M41 * right.M13 + this.M42 * right.M23 + this.M43 * right.M33 + this.M44 * right.M43;
		final float M44 = this.M41 * right.M14 + this.M42 * right.M24 + this.M43 * right.M34 + this.M44 * right.M44;
        
        /*
        float M11 = this.M11 * right.M11 + this.M21 * right.M12 + this.M31 * right.M13 + this.M41 * right.M14;
        float M12 = this.M12 * right.M11 + this.M22 * right.M12 + this.M32 * right.M13 + this.M42 * right.M14;
        float M13 = this.M13 * right.M11 + this.M23 * right.M12 + this.M33 * right.M13 + this.M43 * right.M14;
        float M14 = this.M14 * right.M11 + this.M24 * right.M12 + this.M34 * right.M13 + this.M44 * right.M14;

        float M21 = this.M11 * right.M21 + this.M21 * right.M22 + this.M31 * right.M23 + this.M41 * right.M24;
        float M22 = this.M12 * right.M21 + this.M22 * right.M22 + this.M32 * right.M23 + this.M42 * right.M24;
        float M23 = this.M13 * right.M21 + this.M23 * right.M22 + this.M33 * right.M23 + this.M43 * right.M24;
        float M24 = this.M14 * right.M21 + this.M24 * right.M22 + this.M34 * right.M23 + this.M44 * right.M24;

        float M31 = this.M11 * right.M31 + this.M21 * right.M32 + this.M31 * right.M33 + this.M41 * right.M34;
        float M32 = this.M12 * right.M31 + this.M22 * right.M32 + this.M32 * right.M33 + this.M42 * right.M34;
        float M33 = this.M13 * right.M31 + this.M23 * right.M32 + this.M33 * right.M33 + this.M43 * right.M34;
        float M34 = this.M14 * right.M31 + this.M24 * right.M32 + this.M34 * right.M33 + this.M44 * right.M34;

        float M41 = this.M11 * right.M41 + this.M21 * right.M42 + this.M31 * right.M43 + this.M41 * right.M44;
        float M42 = this.M12 * right.M41 + this.M22 * right.M42 + this.M32 * right.M43 + this.M42 * right.M44;
        float M43 = this.M13 * right.M41 + this.M23 * right.M42 + this.M33 * right.M43 + this.M43 * right.M44;
        float M44 = this.M14 * right.M41 + this.M24 * right.M42 + this.M34 * right.M43 + this.M44 * right.M44;
        */
        
        this.M11 = M11;
        this.M12 = M12;
        this.M13 = M13;
        this.M14 = M14;
        
        this.M21 = M21;
        this.M22 = M22;
        this.M23 = M23;
        this.M24 = M24;
        
        this.M31 = M31;
        this.M32 = M32;
        this.M33 = M33;
        this.M34 = M34;
        
        this.M41 = M41;
        this.M42 = M42;
        this.M43 = M43;
        this.M44 = M44;
	}
	/**
	 * Multiplies all values of this instance with the factor <i>right</i>
	 * @param right the factor
	 */
	public void multiply(final float right) {
		M11 *= right;
        M12 *= right;
        M13 *= right;
        M14 *= right;

        M21 *= right;
        M22 *= right;
        M23 *= right;
        M24 *= right;

        M31 *= right;
        M32 *= right;
        M33 *= right;
        M34 *= right;

        M41 *= right;
        M42 *= right;
        M43 *= right;
        M44 *= right;
	}
		
	//
	// TRANSFORM OPERATIONS
	//

	/**
	 * Sets the transformation values<br />
	 * [1    0      0    0]<br />
	 * [0  cos(a) sin(a) 0]<br />
	 * [0 -sin(a) cos(a) 0]<br />
	 * [0    0      0    1]
	 * @param angle the angle in radians
	 */
	public void rotateX(final float angle) {
		final Matrix4 mRoll = Matrix4.identityMatrix();

		mRoll.M22 = mRoll.M33 = MathHelper.cos(angle);
		//mRoll.M23 = MathHelper.sin(angle);
		//mRoll.M32 = -mRoll.M23;
		
		mRoll.M32 = MathHelper.sin(angle);
		mRoll.M23 = -mRoll.M32;
		
		multiply(mRoll);
	}

	/**
	 * Sets the transformation values<br />
	 * [ cos(a) 0 -sin(a) 0]<br />
	 * [   0    1    0    0]<br />
	 * [ sin(a) 0  cos(a) 0]<br />
	 * [   0    0    0    1]
	 * @param angle the angle in radians
	 */
	public void rotateY(final float angle) {
		final Matrix4 mYaw = Matrix4.identityMatrix();
		
		mYaw.M11 = mYaw.M33 = MathHelper.cos(angle);
		mYaw.M31 = MathHelper.sin(angle);
		mYaw.M13 = -mYaw.M31;
		
		multiply(mYaw);
	}

	/**
	 * Sets the transformation values<br />
	 * [ cos(a) sin(a) 0 0]<br />
	 * [-sin(a) cos(a) 0 0]<br />
	 * [   0      0    1 0]<br />
	 * [   0      0    0 1]
	 * @param angle the angle in radians
	 */
	public void rotateZ(final float angle) {
		final Matrix4 mPitch = Matrix4.identityMatrix();
		
		mPitch.M11 = mPitch.M22 = MathHelper.cos(angle);
		mPitch.M12 = MathHelper.sin(angle);
		mPitch.M21 = -mPitch.M12;
		
		multiply(mPitch);
	}
		
	/**
	 * Sets the transformation values for rotation about all three axis
	 * @param yaw The angle for the y axis
	 * @param pitch The angle for the x axis
	 * @param roll The angle for the z axis
	 */
	public void rotateXYZ(final float yaw, final float pitch, final float roll) {
		
		final Matrix4 mPitch = Matrix4.identityMatrix();
		mPitch.rotateX(pitch);
	
		final Matrix4 mYaw = Matrix4.identityMatrix();
		mYaw.rotateY(yaw);
	
		final Matrix4 mRoll = Matrix4.identityMatrix();
		mRoll.rotateZ(roll);
		
		final Matrix4 rotation = mRoll;
		rotation.multiply(mPitch);
		rotation.multiply(mYaw);
		
		multiply(rotation);
	}

	/**
	 * Sets the transformation values for rotation around a given axis
	 * @param angle the rotation angle
	 * @param axis the rotation axis
	 */
	public void rotate(final float angle, final Vector3 axis) {
		
		axis.normalize();
		
		final float c = MathHelper.cos(angle);
		final float s = MathHelper.sin(angle);
		final float oneminusc = 1.0f - c;
		final float xy = axis.x * axis.y;
		final float yz = axis.y * axis.z;
		final float xz = axis.x * axis.z;
		final float xs = axis.x * s;
		final float ys = axis.y * s;
		final float zs = axis.z * s;
		
		final float f11 = axis.x * axis.x * oneminusc + c;
		final float f12 = xy * oneminusc + zs;
		final float f13 = xz * oneminusc - ys;
		//float f14;
		
		final float f21 = xy * oneminusc - zs;
		final float f22 = axis.y * axis.y * oneminusc + c;
		final float f23 = yz * oneminusc + xs;
		//float f24;
		
		final float f31 = xz * oneminusc + ys;
		final float f32 = yz * oneminusc - xs;
		final float f33 = axis.z * axis.z * oneminusc + c;
		//float f34;
		
		final float t11 = this.M11 * f11 + this.M21 * f12 + this.M31 * f13;
		final float t12 = this.M12 * f11 + this.M22 * f12 + this.M32 * f13;
		final float t13 = this.M13 * f11 + this.M23 * f12 + this.M33 * f13;
		final float t14 = this.M14 * f11 + this.M24 * f12 + this.M34 * f13;
		final float t21 = this.M11 * f21 + this.M21 * f22 + this.M31 * f23;
		final float t22 = this.M12 * f21 + this.M22 * f22 + this.M32 * f23;
		final float t23 = this.M13 * f21 + this.M23 * f22 + this.M33 * f23;
		final float t24 = this.M14 * f21 + this.M24 * f22 + this.M34 * f23;
		
		final Matrix4 dest = Matrix4.identityMatrix();
		
		dest.M31 = this.M11 * f31 + this.M21 * f32 + this.M31 * f33;
		dest.M32 = this.M12 * f31 + this.M22 * f32 + this.M32 * f33;
		dest.M33 = this.M13 * f31 + this.M23 * f32 + this.M33 * f33;
		dest.M34 = this.M14 * f31 + this.M24 * f32 + this.M34 * f33;
		dest.M11 = t11;
		dest.M12 = t12;
		dest.M13 = t13;
		dest.M14 = t14;
		dest.M21 = t21;
		dest.M22 = t22;
		dest.M23 = t23;
		dest.M24 = t24;
		
		setMatrix(dest);
	}
	
	/**
	 * Sets the scaling values<br />
	 * [x 0 0 0]<br />
	 * [0 y 0 0]<br />
	 * [0 0 z 0]<br />
	 * [0 0 0 1]
	 * @param v the scaling values
	 */
	public void scale(final Vector3 v) {
		final Matrix4 m = Matrix4.identityMatrix();
		m.M11 = v.x;
		m.M22 = v.y;
		m.M33 = v.z;
		
		multiply(m);
	}
	
	/**
	 * Sets the translation values<br />
	 * [1 0 0 x]<br />
	 * [0 1 0 y]<br />
	 * [0 0 1 z]<br />
	 * [0 0 0 1]
	 * @param v the translation values
	 */
	public void translate(final Vector3 v) {
		final Matrix4 m = Matrix4.identityMatrix();
		//m.M14 = v.x;
		//m.M24 = v.y;
		//m.M34 = v.z;
		
		m.M42 = v.x;
		m.M43 = v.y;
		m.M44 = v.z;
		
		multiply(m);
	}
			
	//
	// OTHER STUFF
	//
	/**
	 * Returns an array with all values
	 * @return An array with all values
	 */
	public float[] toArray() {
		return new float[] {
			M11, M12, M13, M14,
			M21, M22, M23, M24,
			M31, M32, M33, M34,
			M41, M42, M43, M44
		};
	}
	
	/**
	 * Copies all values from a given Matrix to this matrix
	 * @param m the matrix to be copied
	 */
	private void setMatrix(final Matrix4 m) {
        this.M11 = m.M11;
        this.M12 = m.M12;
        this.M13 = m.M13;
        this.M14 = m.M14;
        
        this.M21 = m.M21;
        this.M22 = m.M22;
        this.M23 = m.M23;
        this.M24 = m.M24;
        
        this.M31 = m.M31;
        this.M32 = m.M32;
        this.M33 = m.M33;
        this.M34 = m.M34;
        
        this.M41 = m.M41;
        this.M42 = m.M42;
        this.M43 = m.M43;
        this.M44 = m.M44;
	}
	
	@Override
	public Matrix4 clone() throws CloneNotSupportedException {
		Matrix4 clone = new Matrix4();
		clone.setMatrix(this);
		return clone;
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof Matrix4) {
			final Matrix4 right = (Matrix4)obj;
			return (M11 == right.M11
					&& M12 == right.M12
					&& M13 == right.M13
					&& M14 == right.M14
					&& M21 == right.M21
					&& M22 == right.M22
					&& M23 == right.M23
					&& M24 == right.M24
					&& M31 == right.M31
					&& M32 == right.M32
					&& M33 == right.M33
					&& M34 == right.M34
					&& M41 == right.M41
					&& M42 == right.M42
					&& M43 == right.M43
					&& M44 == right.M44);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format(
				"Determinant: %s\r\n" +
				"M11: %s\r\nM12: %s\r\nM13: %s\r\nM14: %s\r\n" +
				"M21: %s\r\nM22: %s\r\nM23: %s\r\nM24: %s\r\n" +
				"M31: %s\r\nM32: %s\r\nM33: %s\r\nM34: %s\r\n" +
				"M41: %s\r\nM42: %s\r\nM43: %s\r\nM44: %s\r\n",
				getDeterminant(),
				M11, M12, M13, M14,
				M21, M22, M23, M24,
				M31, M32, M33, M34,
				M41, M42, M43, M44);
	}
}
