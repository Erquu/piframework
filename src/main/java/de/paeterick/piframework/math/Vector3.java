package de.paeterick.piframework.math;

public class Vector3 {

	/**
	 * Returns an empty Vector3 (x:0 y:0 z:0)
	 * @return an empty Vector3 (x:0 y:0 z:0)
	 */
	public static Vector3 emptyVector() {
		return new Vector3();
	}

	
	/** X Coordinate */
	public float x;
	/** Y Coordinate */
	public float y;
	/** Z Coordinate */
	public float z;

	/**
	 * Initializes an empty Vector
	 */
	public Vector3() {
		this(0);
	}
	/**
	 * Initializes a Vector with a given value
	 * @param value The value for all x, y and z
	 */
	public Vector3(float value) {
		x = y = z = value;
	}
	/**
	 * Initializes a Vector with the given values
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param z the z coordinate
	 */
	public Vector3(final float x, final float y, final float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	//
	// MATH OPERATIONS
	//
	public void add(final Vector3 source) {
		x += source.x;
		y += source.y;
		z += source.z;
	}
	
	public void subtract(final Vector3 source) {
		x -= source.x;
		y -= source.y;
		z -= source.z;
	}
	
	public void multiply(final Vector3 source) {
		x *= source.x;
		y *= source.y;
		z *= source.z;
	}
	public void multiply(final float scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
	}

	public float distance(final Vector3 v) {
		
		final float dx = v.x - this.x;
		final float dy = v.y - this.y;
		final float dz = v.z - this.z;
		
		return (float)Math.sqrt(dx * dx + dy * dy + dz * dz);
	}
	
	//
	// TRANSFORM OPERATIONS
	//
	public void transformNormal(final Matrix4 sourceMatrix) {
				
		// Aktuell - rowbased
        this.x = x * sourceMatrix.M11 + y * sourceMatrix.M12 + z * sourceMatrix.M13;
        this.y = x * sourceMatrix.M21 + y * sourceMatrix.M22 + z * sourceMatrix.M23;
        this.z = x * sourceMatrix.M31 + y * sourceMatrix.M32 + z * sourceMatrix.M33;
        
        // colbased
        //this.x = x * sourceMatrix.M11 + y * sourceMatrix.M21 + z * sourceMatrix.M31;
        //this.y = x * sourceMatrix.M12 + y * sourceMatrix.M22 + z * sourceMatrix.M32;
        //this.z = x * sourceMatrix.M13 + y * sourceMatrix.M23 + z * sourceMatrix.M33;
        
        // rowbased with translation
        //this.x = x * sourceMatrix.M11 + y * sourceMatrix.M12 + z * sourceMatrix.M13 + sourceMatrix.M42;
        //this.y = x * sourceMatrix.M21 + y * sourceMatrix.M22 + z * sourceMatrix.M23 + sourceMatrix.M43;
        //this.z = x * sourceMatrix.M31 + y * sourceMatrix.M32 + z * sourceMatrix.M33 + sourceMatrix.M44;
        
        // Test 1
        //this.x = x * sourceMatrix.M11 - z * sourceMatrix.M13;
        //this.z = x * sourceMatrix.M31 + z * sourceMatrix.M33;
        
	}
	
	public void translateNormal(final Matrix4 sourceMatrix) {
		//this.x += sourceMatrix.M14;
		//this.y += sourceMatrix.M24;
		//this.z += sourceMatrix.M34;
		
		this.x += sourceMatrix.M42;
		this.y += sourceMatrix.M43;
		this.z += sourceMatrix.M44;
	}
	
	//TODO: Test this method
	public void translateTransform(final Matrix4 sourceMatrix) {
		transformNormal(sourceMatrix);
		translateTransform(sourceMatrix);
	}
	
	
	/**
	 * Computes the magnitude (or length) of the vector
	 * @return the magnitude of the vector
	 */
	public float magnitude() {
		return (float)Math.sqrt((double)(x * x + y * y + z * z));
	}
	
	// Untested
	public float dotProduct(final Vector3 right) {
		return (this.x * right.x + this.y * right.y + this.z * right.z);
	}
	
	// Untested
	public Vector3 crossProduct(final Vector3 right) {
		final Vector3 v = new Vector3();
		v.x = right.z * this.y - right.y * this.z;
		v.y = right.x * this.z - right.z * this.x;
		v.z = right.y * this.x - right.x * this.y;
		return v;
	}
		
	/**
	 * Normalizes the vector into a unit vector
	 */
	public void normalize() {
		float m = magnitude(); // Magnitude
		
		if (m == 0 || m == 1)
			return;
		
		m = 1.0f / m;
		
		x *= m;
		y *= m;
		z *= m;
	}
	
	/**
	 * Creates an exact copy of the current Vector3
	 * @return an exact copy of the current Vector3
	 */
	public Vector3 copy() {
		return new Vector3(x, y, z);
	}
	
	@Override
	public String toString() {
		return String.format("x:%s y:%s z:%s", x, y, z); 
	}
	public String toString(final boolean rounded) {
		if (rounded)
			return String.format("x:%s y:%s z:%s", Math.round(x), Math.round(y), Math.round(z));
		return toString();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (super.equals(obj))
			return true;
		
		if (obj instanceof Vector3) {
			final Vector3 v = (Vector3)obj;
			return (this.x == v.x && this.y == v.y && this.z == v.z);
		}
		return false;
	}
}
