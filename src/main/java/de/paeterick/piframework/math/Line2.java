package de.paeterick.piframework.math;

/**
 * Class representing a straight line with two coordinate points to set the direction
 * 
 * @author Erquu
 */
public class Line2 {
	
	/** First point on X axis */
	public float x1;
	/** First point on Y axis */
	public float y1;
	/** Second point on X axis */
	public float x2;
	/** Second point on Y axis */
	public float y2;
	
	public Line2() {
		this.x1 = this.x2 = this.y1 = this.y2 = 0.0f;
	}
	public Line2(final float x1, final float y1, final float x2, final float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	/**
	 * Checks if a line is parallel to another line
	 * @param line The line to check against
	 * @return true if lines a parallel
	 */
	public boolean isParallel(final Line2 line) {
		final float xa = this.x2 - this.x1;
		final float ya = this.y2 - this.y1;
		final float xb = line.x2 - line.x1;
		final float yb = line.y2 - line.y1;
		
		return ((xa == xb) && (ya == yb));
	}
	
	/**
	 * Returns the point of intersection of two lines 
	 * @param line The line to check against
	 * @return A {@link Vector2} with the coordinates of the intersection point
	 */
	// TODO: This is a formula to get the intersection point of a line and a line segment!
	@Deprecated
	public Vector2 intersectPoint(final Line2 line) {
		final Vector2 collision = Vector2.emptyVector();
		
		if (!this.isParallel(line)) {
			final float x3 = line.x1;
			final float y3 = line.y1;
			final float x4 = line.x2;
			final float y4 = line.y2;
			
	        collision.x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
	        collision.y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
		}
        
        return collision;
	}
	
	/**
	 * Checks if a line intersects with another line
	 * @param line The line to check against
	 * @return True if lines intersect
	 */
	// TODO: This is a formula to get the intersection of a line and a line segment!
	@Deprecated
	public boolean intersects(final Line2 line) {
		
		final Vector2 p = this.intersectPoint(line);

		final float minX = Math.min(line.x1, line.x2);
        final float minY = Math.min(line.y1, line.y2);
        final float maxX = Math.max(line.x1, line.x2);
        final float maxY = Math.max(line.y1, line.y2);

        return (p.x > minX && p.y > minY && p.x < maxX && p.y < maxY);
	}
	
	/**
	 * Moves the line for the given distance
	 * @param x The x distance
	 * @param y The y distance
	 */
	public void move(final float x, final float y) {
		this.x1 += x;
		this.y1 += y;
		this.x2 += x;
		this.y2 += y;
	}
	
	@Override
	public String toString() {
		return String.format("Line2[x1:%s, y1:%s, x2:%s, y2:%s]", x1, y1, x2, y2);
	}
}
