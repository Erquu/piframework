package de.paeterick.piframework.game.console;

public class ConsoleActionArguments {

	private final GameConsole consoleInstance;
	private final String command;
	private final String[] args;
	
	public ConsoleActionArguments(final String command, final String[] args, final GameConsole consoleInstance) {
		this.command = command;
		this.args = args;
		this.consoleInstance = consoleInstance;
		
		System.out.println("Command: " + command);
		for (String s : args) {
			System.out.println("    " + s);
		}
	}
	
	public String[] getArgs() {
		return args;
	}
	
	public String getArg(int index) {
		if (index > -1 && index < args.length) {
			return args[index];
		}
		return null;
	}
	
	public int size() {
		return args.length;
	}
	
	public GameConsole getConsole() {
		return consoleInstance;
	}
	
	public String getCommand() {
		return command;
	}

}
