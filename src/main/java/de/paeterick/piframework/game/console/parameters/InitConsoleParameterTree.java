package de.paeterick.piframework.game.console.parameters;

public class InitConsoleParameterTree extends ConsoleParameterTree {

	public InitConsoleParameterTree(String commandName, ConsoleParameterTree[] subCommands) {
		super(commandName, subCommands);
	}
	
	@Override
	protected String getSuggestion(String[] in, int offset) {
		if (fSubCommands != null) {
			for (final ConsoleParameterTree t : fSubCommands) {
				final String suggestion = t.getSuggestion(in, offset);
				if (suggestion != null) {
					return suggestion;
				}
			}
		}
		return null;
	}
	
	@Override
	protected ConsoleParameterTree getTree(String[] in, int offset) {
		for (final ConsoleParameterTree t : fSubCommands) {
			final ConsoleParameterTree sT = t.getTree(in, offset);
			if (sT != null) {
				return sT;
			}
		}
		return null;
	}
}
