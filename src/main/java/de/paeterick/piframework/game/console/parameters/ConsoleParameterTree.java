package de.paeterick.piframework.game.console.parameters;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class ConsoleParameterTree {
	public static final int MAX_HINTS = 4;
	
	protected String fCommandName;
	protected ConsoleParameterTree[] fSubCommands;
	
	public ConsoleParameterTree(final String commandName, ConsoleParameterTree[] subCommands) {
		this.fCommandName = commandName;
		this.fSubCommands = subCommands;
	}
	
	public String getSuggestion(final String in) {
		final String[] inA = in.trim().split("\\s");
		return getSuggestion(inA, 0);
	}
	protected String getSuggestion(final String[] in, final int offset) {
		if (offset < in.length) {
			if (fCommandName.equals(in[offset]) || (offset == in.length - 1 && isCommand(in[offset]))) {
				if (fSubCommands != null) {
					for (final ConsoleParameterTree t : fSubCommands) {
						final String suggestion = t.getSuggestion(in, offset + 1);
						if (suggestion != null) {
							return fCommandName + " " + suggestion;
						}
					}
				}
				return fCommandName;
			}
		}
		return null;
	}
	
	public String[] getHints(final String in) {

		final String[] inA = in.trim().split("\\s");
		final List<String> sTs = new Vector<String>();
		
		ConsoleParameterTree t = getTree(inA, 0);
		
		final boolean addAll = in.length() == 0 || in.endsWith(" ");
		
		if (t == null && in.length() <= 1) {
			t = this;
		}
		
		if (t != null && t.fSubCommands != null) {
			for (final ConsoleParameterTree sT : t.fSubCommands) {
				if (addAll) {
					sTs.add(sT.fCommandName);
				} else {
					if (sT.isCommand(inA[inA.length - 1])) {
						sTs.add(sT.fCommandName);
					}
				}
				if (sTs.size() == MAX_HINTS) {
					break;
				}
			}
		}
		
		return sTs.toArray(new String[sTs.size()]);
	}
	
	public void addCommands(final ConsoleParameterTree commandTree) {
		final boolean isInitTree = (commandTree instanceof InitConsoleParameterTree);
		final int add = isInitTree ? commandTree.fSubCommands.length : 1;
		fSubCommands = Arrays.copyOf(fSubCommands, fSubCommands.length + add);
		if (isInitTree) {
			for (int i = 0; i < add; i++) {
				fSubCommands[fSubCommands.length - 1 + i] = commandTree.fSubCommands[i];
			}
		} else {
			fSubCommands[fSubCommands.length - 1] = commandTree;
		}
		
	}
	
	protected ConsoleParameterTree getTree(final String in) {
		final String[] inA = in.trim().split("\\s");
		return getTree(inA, 0);
	}
	protected ConsoleParameterTree getTree(final String[] in, final int offset) {
		if (offset < in.length) {
			if (fCommandName.equals(in[offset])) {
				if (offset == in.length -1) {
					return this;
				}
				
				for (final ConsoleParameterTree t : fSubCommands) {
					final ConsoleParameterTree sT = t.getTree(in, offset + 1);
					if (sT != null) {
						return sT;
					}
				}
			}
		}
		if (isCommand(in[offset]) && offset == in.length - 2) {
			return this;
		}
		return null;
	}
	
	protected boolean isCommand(final String in) {
		return fCommandName.startsWith(in.trim());
	}
	
	@Override
	public String toString() {
		return toString("");
	}
	protected String toString(final String pre) {
		final StringBuilder b = new StringBuilder();
		b.append(pre + fCommandName);
		if (fSubCommands != null) {
			for (final ConsoleParameterTree t : fSubCommands) {
				b.append("\r\n" + t.toString(pre + "  "));
			}
		}
		return b.toString();
	}
}
