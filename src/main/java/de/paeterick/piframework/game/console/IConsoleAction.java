package de.paeterick.piframework.game.console;

public interface IConsoleAction {
	void doAction(final ConsoleActionArguments args) throws Exception;
}