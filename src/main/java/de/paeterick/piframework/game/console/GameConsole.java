package de.paeterick.piframework.game.console;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import piresources.PiResourceLoader;
import de.paeterick.piframework.game.IRenderable;
import de.paeterick.piframework.game.IRenderingTarget;
import de.paeterick.piframework.game.console.parameters.ConsoleParameterTree;
import de.paeterick.piframework.game.console.parameters.ConsoleParameterTreeFactory;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.logic.UpdateLoop;
import de.paeterick.piframework.math.MathHelper;

/**
 * 
 * @author Erquu
 */
public class GameConsole extends KeyAdapter implements IRenderable, IConsoleAction {
	
	/** Key code for toggling the console */
	private int fConsoleTriggerKey = 130;
	/** The minimum vertical size of the console in pixel */
	private final int fMinimumSize = 400;
	/** The timout for the cursor flash in ms */
	private final int fCursorTimeout = 600;
	/** The last cursor (flash) update time */
	private long fLastCursorUpdate = 0;
	/** Indicator if showing or hiding the cursor (flash) */
	private boolean fShowCursor = true;
	/** The maximum lines to save */
	private int fLineBuffer = 100;
	/** The console background color and transparency */
	private Color fConsoleBackground = new Color(0.1f, 0.1f, 0.1f, 0.8f);
	/** The console input line color and transparency */
	private Color fConsoleLineColor = new Color(1.0f, 1.0f, 1.0f, 0.5f);
	
	/** Error messages which will be selected randomly */
	private final String[] fErrorMessages = new String[] {
		"(Error) Wutt? What is that? I can't process a command like that...",
		"(Error) I can't do that!",
		"(Error) No Way I'm gonna do that."
	};
	
	/** List holding the messages */
	private final LinkedList<String> fLines = new LinkedList<String>();
	/** Map holding all registered console commands and actions */
	private final Map<String, IConsoleAction> fActions = new HashMap<String, IConsoleAction>();
	/** Tree containing all registered parameters (for context sensitive hints) */
	private ConsoleParameterTree fParameterTree = null;
	/** List holding all commands entered */
	private final LinkedList<String> fCommandHistory = new LinkedList<String>();
	private int fCommandHistoryPointer = 0;
	
	/** The target to render the console on */
	private IRenderingTarget fRenderingTarget;
	/** If false, the console can not be shown */
	private boolean fEnabled = false;
	/** Indicates if the console is active */
	private boolean fActive = false;
	
	/** String for holding the current input */
	private String fCurrentInput = "";
	/** String for suggestion */
	private String fCurrentSuggestion = "";
	/** String array holding all current hints */
	private String[] fCurrentHints = null;
	
	public GameConsole() {
		PiResourceLoader.instance.setFile("console.xml");
		try {
			fParameterTree = ConsoleParameterTreeFactory.fromXml(PiResourceLoader.instance.getResourceAsStream());
		} catch (final Exception e) {
			e.printStackTrace();
		}
		
		addAction("co", this);
	}
	
	/**
	 * Sets the target to render the console to
	 * @param renderingTarget The new rendering target
	 */
	public void setRenderingTarget(final IRenderingTarget renderingTarget) {
		/** Temporarily handled by engine
		if (this.fRenderingTarget != null && this.fRenderingTarget.getRenderingComponent() != null) {
			this.fRenderingTarget.getRenderingComponent().removeKeyListener(this);
		}
		*/
		this.fRenderingTarget = renderingTarget;
		/**
		this.fRenderingTarget.getRenderingComponent().addKeyListener(this);
		*/
	}

	/**
	 * Adds an console action to this instance
	 * @param key The command for the given action
	 * @param action The action to run
	 */
	public void addAction(final String key, final IConsoleAction action) {
		if (key != null && action != null) {
			fActions.put(key, action);
		}
	}
	/**
	 * Adds an console action to this instance
	 * @param key The command for the given action
	 * @param action The action to run
	 * @param parameterTree The argument tree for the console suggestion
	 */
	public void addAction(final String key, final IConsoleAction action, final ConsoleParameterTree parameterTree) {
		addAction(key, action);
		fParameterTree.addCommands(parameterTree);
	}
	
	/**
	 * Prints a message to the console<br />
	 * You can use color keys to print in different colors<br />
	 * <table>
	 * <tr><td>Key</td><td>Number</td><td>Color</td></tr>
	 * <tr><td>[^0]</td><td>0</td><td>Black</td></tr>
	 * <tr><td>[^1]</td><td>1</td><td>White</td></tr>
	 * <tr><td>[^2]</td><td>2</td><td>Red</td></tr>
	 * <tr><td>[^3]</td><td>3</td><td>Green</td></tr>
	 * <tr><td>[^4]</td><td>4</td><td>Yellow</td></tr>
	 * <tr><td>[^5]</td><td>5</td><td>Blue</td></tr>
	 * <tr><td>[^6]</td><td>6</td><td>Magenta</td></tr>
	 * <tr><td>[^7]</td><td>7</td><td>Cyan</td></tr>
	 * <tr><td>[^8]</td><td>8</td><td>Gray</td></tr>
	 * <tr><td>[^9]</td><td>9</td><td>Dark Gray</td></tr>
	 * <br />
	 * Examples
	 * &nbsp;&nbsp;Red Text: [^2]Error: Lorem Ipsum
	 * &nbsp;&nbsp;Multicolored Text: [^2]Error: [^4]Lorem [^9]Ipsum
	 * @param message The Message you want to add to the console
	 */
	public void addMessage(final String message) {
		if (message != null) {
			fLines.add(message);
			if (fLines.size() > fLineBuffer) {
				for (int i = 0, length = fLines.size() - fLineBuffer; i < length; i++) {
					fLines.poll();
				}
			}
		}
	}
	/**
	 * Prints a message to the console<br />
	 * You can use color keys to print in different colors<br />
	 * <table>
	 * <tr><td>Key</td><td>Number</td><td>Color</td></tr>
	 * <tr><td>[^0]</td><td>0</td><td>Black</td></tr>
	 * <tr><td>[^1]</td><td>1</td><td>White</td></tr>
	 * <tr><td>[^2]</td><td>2</td><td>Red</td></tr>
	 * <tr><td>[^3]</td><td>3</td><td>Green</td></tr>
	 * <tr><td>[^4]</td><td>4</td><td>Yellow</td></tr>
	 * <tr><td>[^5]</td><td>5</td><td>Blue</td></tr>
	 * <tr><td>[^6]</td><td>6</td><td>Magenta</td></tr>
	 * <tr><td>[^7]</td><td>7</td><td>Cyan</td></tr>
	 * <tr><td>[^8]</td><td>8</td><td>Gray</td></tr>
	 * <tr><td>[^9]</td><td>9</td><td>Dark Gray</td></tr>
	 * <br />
	 * Examples
	 * &nbsp;&nbsp;Red Text: [^2]Error: Lorem Ipsum
	 * &nbsp;&nbsp;Multicolored Text: [^2]Error: [^4]Lorem [^9]Ipsum
	 * @param format The format of the message
	 * @param args The format args
	 */
	public void addMessage(final String format, final Object... args) {
		addMessage(String.format(format, args));
	}
	
	/**
	 * Enables or disables the console
	 * @param enabled True to enable
	 */
	public void setEnabled(final boolean enabled) {
		this.fEnabled = enabled;
	}
	
	/**
	 * Shows or hides the console
	 * @param activate True to activate / show
	 */
	public void setActive(final boolean activate) {
		fActive = fEnabled && activate;
	}
	
	public void evaluate(final String input) {
		this.evaluate(this.toInputArguments(input));
	}
	private void evaluate(final ConsoleActionArguments args) {
		if (fActions.containsKey(args.getCommand())) {
			try {
				fActions.get(args.getCommand()).doAction(args);
			} catch (Exception e1) {
				addMessage(fCurrentInput);
				addMessage("[^2]    %s", fErrorMessages[MathHelper.randomInteger(0, fErrorMessages.length)]);
			}
		} else {
			fLines.add(fCurrentInput);
			addMessage("  [^2]Command %s [^2]could not be found!", fCurrentInput);
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println(e.getKeyCode());
		if (fEnabled) {
			
			if (e.getKeyCode() == fConsoleTriggerKey) {
				fActive = !fActive;
				if (fConsoleTriggerKey == 130) {
					fCurrentInput += '^';
				}
			} else if (fActive) {
				switch (e.getKeyCode()) {
					case 8: // Backspace
						if (fCurrentInput.length() > 0) {
							fCurrentInput = fCurrentInput.substring(0, fCurrentInput.length() - 1);
							fCurrentSuggestion = getSuggestion(fCurrentInput);
							fCurrentHints = fParameterTree.getHints(fCurrentInput);
						}
						break;
					//case 39: // Arrow right
					case 9: // Tab
						autoComplete();
						break;
					case 10: // Enter
						fCommandHistory.add(fCurrentInput);
						fCommandHistoryPointer = fCommandHistory.size();
						this.evaluate(this.getCurrentInputArguments());
						
						fCurrentInput = "";
						fCurrentSuggestion = "";
						break;
					case 27: // Escape
						fCurrentInput = "";
						fCurrentSuggestion = "";
						break;
					case 38: // Up
						if (fCommandHistory.size() > 0) {
							fCommandHistoryPointer--;
							
							if (fCommandHistoryPointer < 0)
								fCommandHistoryPointer = fCommandHistory.size() - 1;
							
							fCurrentInput = fCommandHistory.get(fCommandHistoryPointer);
						}
						break;
					case 40: // Down
						if (fCommandHistory.size() > 0) {
							fCommandHistoryPointer++;
							
							if (fCommandHistoryPointer == fCommandHistory.size())
								fCommandHistoryPointer = 0;
							
							fCurrentInput = fCommandHistory.get(fCommandHistoryPointer);
						}
						break;
					case 130:
						fCurrentInput += '^';
						break;
					default:
						if (Character.isDefined(e.getKeyChar())) {
							fCurrentInput += e.getKeyChar();
							
							updateHintsAndSuggestion();
						}
						break;
				}
			}
		}
	}

	public void render(final Graphics2D context, final Scene scene) {
		if (fActive) {
			final Rectangle clientRect = fRenderingTarget.getClientRect();
			int size = (int)((float)clientRect.height * 0.2f);
			size = MathHelper.clamp(size, fMinimumSize, 1000);
		
			context.setColor(fConsoleBackground);
			context.fillRect(0, 0, clientRect.width, size);
			
			final int height = context.getFontMetrics().getHeight();
			final int maxLines = (int)Math.floor(size / height) - 1;
			final int start = MathHelper.clamp(fLines.size() - maxLines, 0, fLines.size());
			int offsetY = height;
			
			for (int i = start; i < fLines.size(); i++) {
				renderString(context, fLines.get(i), 5, offsetY);
				offsetY += height;
			}
			
			context.setColor(fConsoleLineColor);
			context.drawLine(0, (size - height + 4), clientRect.width, (size - height + 4));
			
			final String in = String.format("> %s", fCurrentInput);
			final Rectangle2D lineMetrics = context.getFontMetrics().getStringBounds(in, context);
			renderString(context, in, 5, size);

			if (fLastCursorUpdate + fCursorTimeout < UpdateLoop.getCurrentTime()) {
				fLastCursorUpdate = UpdateLoop.getCurrentTime();
				fShowCursor = !fShowCursor;
			}
			if (fShowCursor) {
				context.setColor(Color.white);
				final int cursorX = (int)lineMetrics.getWidth() + 6;
				context.drawLine(cursorX, size - height + 5, cursorX, size);
			}
			
			if (fCurrentSuggestion.length() > 0) {
				context.setColor(fConsoleLineColor);
				context.drawString(
					fCurrentSuggestion.substring(
						Math.max(0,
							Math.min(fCurrentSuggestion.length(), (in.length() - 2)
						)
					)
				), (int)(5.0d + lineMetrics.getWidth()), size);
			}
			
			if (fCurrentHints != null && fCurrentHints.length > 0) {
				
				double maxWidth = 0;
				for (final String hint : fCurrentHints) {
					final Rectangle2D hintMetrics = context.getFontMetrics().getStringBounds(hint, context);
					if (hintMetrics.getWidth() > maxWidth) {
						maxWidth = hintMetrics.getWidth();
					}
				}
				
				context.setColor(fConsoleBackground);
				context.fillRect(
					(int)(5.0d + lineMetrics.getWidth()),
					size,
					(int)(maxWidth + 5.0d),
					(fCurrentHints.length * height + 3));

				offsetY = size;

				context.setColor(fConsoleLineColor);
				for (final String hint : fCurrentHints) {
					offsetY += height;
					context.drawString(hint, (int)(7.5d + lineMetrics.getWidth()), offsetY);
				}
			}
		}
	}
	
	private void renderString(final Graphics2D context, final String input, int offsetX, final int offsetY) {
		
		final StringBuilder builder = new StringBuilder();
		String buffer;
		
		context.setColor(Color.WHITE);
		
		for (int j = 0; j < input.length(); j++) {
			if (j + 3 < input.length() && input.charAt(j) == '[' && input.charAt(j+1) == '^' && Character.isDigit(input.charAt(j+2)) && input.charAt(j+3) == ']') {
				buffer = builder.toString();
				context.drawString(buffer, offsetX, offsetY);
				
				final Rectangle2D lineMetrics = context.getFontMetrics().getStringBounds(buffer, context);
				offsetX += lineMetrics.getWidth();
				
				builder.delete(0, builder.length());
				
				switch (input.charAt(j+2)) {
					case '0': context.setColor(Color.BLACK); break;
					case '1': context.setColor(Color.WHITE); break;
					case '2': context.setColor(Color.RED); break;
					case '3': context.setColor(Color.GREEN); break;
					case '4': context.setColor(Color.YELLOW); break;
					case '5': context.setColor(Color.BLUE); break;
					case '6': context.setColor(Color.MAGENTA); break;
					case '7': context.setColor(Color.CYAN); break;
					case '8': context.setColor(Color.GRAY); break;
					case '9': context.setColor(Color.DARK_GRAY); break;
				}
				j += 3;
			} else {
				builder.append(input.charAt(j));
			}
		}
		if (builder.length() > 0) {
			context.drawString(builder.toString(), offsetX, offsetY);
		}
	}

	private void updateHintsAndSuggestion() {
		if (fCurrentInput.length() > 0) {
			fCurrentSuggestion = getSuggestion(fCurrentInput);
			fCurrentHints = fParameterTree.getHints(fCurrentInput);
		} else {
			if (fCurrentHints.length == 1)
				fCurrentSuggestion = fCurrentHints[0];
			else
				fCurrentSuggestion = "";
		}
	}
	
	private void autoComplete() {
		if (fCurrentInput.length() > 0 && fCurrentSuggestion.length() > 0) {
			fCurrentInput = fCurrentSuggestion + " ";
		}
		updateHintsAndSuggestion();
		if (fCurrentHints.length == 1) {
			fCurrentInput += fCurrentHints[0] + " ";
			updateHintsAndSuggestion();
			//autoComplete();
		}
	}
	
	private String getSuggestion(final String input) {
		if (fParameterTree != null) {
			final String suggestion = fParameterTree.getSuggestion(input);
			if (suggestion != null) {
				return suggestion;
			}
		}
		return input;
	}

	private ConsoleActionArguments getCurrentInputArguments() {
		return toInputArguments(fCurrentInput);
	}

	private ConsoleActionArguments toInputArguments(final String input) {
		String command = null;
		boolean strOpen = false;
		final List<String> args = new Vector<String>();
		final StringBuilder builder = new StringBuilder();
		for (int charIndex = 0; charIndex < input.length(); charIndex++) {
			char c = input.charAt(charIndex);
			if ('"' == c) {
				strOpen = !strOpen;
			} else {
				builder.append(c);
			}
			if ((!strOpen && Character.isWhitespace(c)) || charIndex == input.length() - 1) {
				if (Character.isWhitespace(c))
					builder.deleteCharAt(builder.length()-1);
				
				if (builder.length() > 0) {
					if (command == null) {
						command = builder.toString();
					} else {
						args.add(builder.toString());
					}
				}
				builder.setLength(0);
			}
		}
		
		return new ConsoleActionArguments(command, args.toArray((new String[args.size()])), this);
	}

	public void doAction(final ConsoleActionArguments args) throws Exception {
		if ("set".equals(args.getArg(0))) {
			if ("foreground".equals(args.getArg(1)) || "background".equals(args.getArg(1))) {
				if (args.size() >= 5) {
					float r = Float.parseFloat(args.getArgs()[2]);
					float g = Float.parseFloat(args.getArgs()[3]);
					float b = Float.parseFloat(args.getArgs()[4]);
					float a = 0.8f;
					if (args.size() == 6) {
						a = Float.parseFloat(args.getArgs()[5]);
					}
					if ("foreground".equals(args.getArg(1))) {
						fConsoleLineColor = new Color(r, g, b, a);
					} else {
						fConsoleBackground = new Color(r, g, b, a);
					}
				}
			}
		}
	}
}
