package de.paeterick.piframework.game.console.parameters;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Vector;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

@SuppressWarnings("restriction")
public class ConsoleParameterTreeFactory {
	private ConsoleParameterTreeFactory() {}
	
	public static ConsoleParameterTree fromXml(final String xmlInput) throws XMLStreamException {
		return fromXml(new ByteArrayInputStream(xmlInput.getBytes(StandardCharsets.UTF_8)));
	}
	public static ConsoleParameterTree fromXml(final InputStream xmlInputStream) throws XMLStreamException {

		final XMLInputFactory factory = XMLInputFactory.newInstance();
		final XMLStreamReader parser = factory.createXMLStreamReader(xmlInputStream);
		
		String name;
		List<ConsoleParameterTree> initTrees = new Vector<ConsoleParameterTree>();
		ConsoleParameterTree t = null;
		
		while (parser.hasNext()) {
			switch (parser.getEventType()) {
				case XMLStreamConstants.START_ELEMENT:
					name = parser.getLocalName().toLowerCase();
					if (name.equals("command")) {
						t = parseSub(parser);
						initTrees.add(t);
					}
					break;
				case XMLStreamConstants.END_DOCUMENT:
					parser.close();
					break;
			}
			parser.next();
		}

		try {
			parser.close();
			xmlInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new InitConsoleParameterTree("", initTrees.toArray(new ConsoleParameterTree[initTrees.size()]));
	}

	private static ConsoleParameterTree parseSub(final XMLStreamReader parser) throws XMLStreamException {

		final List<ConsoleParameterTree> subTrees = new Vector<ConsoleParameterTree>();
		String name = null;
		String command = null;
		
		for (int i = 0; i < parser.getAttributeCount(); i++) {
			name = parser.getAttributeLocalName(i);
			if (name.equals("name")) {
				command = parser.getAttributeValue(i);
			}
		}
		
		
		Parser:
		while(parser.hasNext()) {
			parser.next();
			switch (parser.getEventType()) {
				case XMLStreamConstants.START_ELEMENT:
					name = parser.getLocalName().toLowerCase();
					if (name.equals("command")) {
						subTrees.add(parseSub(parser));
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = parser.getLocalName().toLowerCase();
					if (name.equals("command")) {
						break Parser;
					}
					break;
			}
		}
		
		return new ConsoleParameterTree(command, subTrees.toArray(new ConsoleParameterTree[subTrees.size()]));
	}
}
