package de.paeterick.piframework.game;

import java.awt.Component;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;

/**
 * The rendering target interface is needed to create a custom, by the engine usable component
 * 
 * @author csp8fe
 */
public interface IRenderingTarget {
	/** {@inheritDoc Canvas#createBufferStrategy(int)} */
	void createBufferStrategy(final int buffercount);
	/** {@inheritDoc Canvas#getBufferStrategy()} */
	BufferStrategy getBufferStrategy();
	/**
	 * Returns the component that is used for rendering on
	 * @return The component that is used for rendering on
	 */
	Component getRenderingComponent();
	
	/** 
	 * {@inheritDoc Component#getBounds()}
	 */
	Rectangle getClientRect();
	/**
	 * {@inheritDoc Component#isEnabled()}
	 */
	boolean isEnabled();
	/**
	 * Returns if the rendering target is ready to draw on
	 * @return true if the rendering target is ready to draw on
	 */
	boolean ready();
	/**
	 * {@inheritDoc JFrame#dispose()}
	 */
	void dispose();
}
