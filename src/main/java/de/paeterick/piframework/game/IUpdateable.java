package de.paeterick.piframework.game;

/**
 * Interface for Game objects that can be updated
 * @author Erquu
 */
public interface IUpdateable {
	/**
	 * Called after each iteration from the main loop
	 * @param time the time when the loop called the update function
	 */
	void update(final long deltaTime, final long totalTime);
}
