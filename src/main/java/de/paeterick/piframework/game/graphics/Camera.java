package de.paeterick.piframework.game.graphics;

import java.awt.Rectangle;

import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;

public class Camera {

	private Vector2 position;
	private Viewport viewport;
	
	private Rectangle clampBounds = null;
	
	public Camera(final Viewport viewport) {
		this(viewport, new Vector2((float)viewport.width * .5f, (float)viewport.height * .5f));
	}
	public Camera(final Viewport viewport, final float x, final float y) {
		this(viewport, new Vector2(x, y));
	}
	public Camera(final Viewport viewport, final Vector2 position) {
		this.viewport = viewport;
		this.position = position;
		moveTo(position.x, position.y);
	}
	
	public void moveTo(final float x, final float y) {
		this.position.x = x;
		this.position.y = y;
		
		float halfWidth = (float)this.viewport.width * .5f;
		float halfHeight = (float)this.viewport.height * .5f;
		
		this.viewport.x = (int)(x - halfWidth);
		this.viewport.y = (int)(y - halfHeight);
		
		if (this.clampBounds != null) {
			this.viewport.x = MathHelper.clamp(this.viewport.x, this.clampBounds.x, this.clampBounds.width - this.viewport.width);
			this.viewport.y = MathHelper.clamp(this.viewport.y, this.clampBounds.x, this.clampBounds.height - this.viewport.height);
		}
	}
	
	public void clamp(final Rectangle bounds) {
		this.clampBounds = bounds;
	}
	
	public void translate(final float x, final float y) {
		position.x += x;
		position.y += y;
		viewport.x += x;
		viewport.y += y;
	}
	
	public Viewport getViewport() {
		return this.viewport;
	}
	
	public void setViewport(final Viewport viewport) {
		this.viewport = viewport;
	}

}
