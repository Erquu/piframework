package de.paeterick.piframework.game.graphics;

import java.awt.Rectangle;

import de.paeterick.piframework.math.Vector2;

public class Scene {
	public final Viewport viewport;
	public Camera camera;
	
	public Scene(final Rectangle bounds) {
		viewport = new Viewport(new Rectangle(0, 0, bounds.width, bounds.height));
		camera = new Camera(viewport, bounds.x, bounds.y);
	}
	
	public Vector2 toWorld(final Vector2 v) {
		return (new Vector2(viewport.x, viewport.y)).add(v);
	}
	
	public Vector2 toViewport(final Vector2 v) {
		return v.subtractNew(new Vector2(viewport.x, viewport.y));
	}
}
