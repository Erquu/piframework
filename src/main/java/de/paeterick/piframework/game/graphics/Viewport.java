package de.paeterick.piframework.game.graphics;

import java.awt.Rectangle;

public class Viewport {
	public int x;
	public int y;
	public int width;
	public int height;
	
	public Viewport() {
		x = y = width = height = 0;
	}
	public Viewport(Rectangle bounds) {
		setBounds(bounds);
	}
	public Viewport(final int x, final int y, final int width, final int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
	
	public void setBounds(final Rectangle bounds) {
		x = bounds.x;
		y = bounds.y;
		width = bounds.width;
		height = bounds.height;
	}
	public void setBounds(final int x, final int y, final int width, final int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public float getAspectRatio() {
		if (height != 0 && width != 0) {
			return ((float)width / (float)height);
		}
		return 0f;
	}
	
	@Override
	public String toString() {
		return String.format("Viewport[x=%s,y=%s,width=%s,height=%s,aspectRatio=%s]", x, y, width, height, getAspectRatio());
	}
}
