package de.paeterick.piframework.game.exceptions;

public class EngineException extends Exception {
	private static final long serialVersionUID = 3387789265925300296L;
	
	public EngineException() {
		super();
	}
	public EngineException(final String message) {
		super(message);
	}
}
