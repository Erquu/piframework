package de.paeterick.piframework.game.exceptions;

public class EngineAlreadyStartedException extends EngineException {
	private static final long serialVersionUID = 931027163460603098L;
	
	public EngineAlreadyStartedException() {
		super();
	}
	public EngineAlreadyStartedException(final String message) {
		super(message);
	}
}
