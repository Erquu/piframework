package de.paeterick.piframework.game.exceptions;

public class EngineAlreadyInitializedException extends EngineException {
	private static final long serialVersionUID = -647221225433332488L;
	
	public EngineAlreadyInitializedException() {
		super();
	}
	public EngineAlreadyInitializedException(final String message) {
		super(message);
	}
}
