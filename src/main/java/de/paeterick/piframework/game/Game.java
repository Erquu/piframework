package de.paeterick.piframework.game;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import de.paeterick.piframework.hid.KeyboardState;
import de.paeterick.piframework.hid.MouseState;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.ui.UI;

/**
 * Basic class for Games running on this engine
 * @author Erquu
 */
public abstract class Game implements IUpdateable, IRenderable {
	/** Game Engine instance */
	private GameEngine fEngine;
	/** HID Keyboard state instance */
	private KeyboardState fKeyboardState;
	/** HID Mouse state instance */
	private MouseState fMouseState;
	
	/**
	 * Returns the game engine instance
	 * @return the game engine instance
	 */
	protected final GameEngine getEngine() {
		return fEngine;
	}
	/**
	 * Used to set or update the engine
	 * @param engine the instance of the Game Engine
	 */
	public final void setEngine(final GameEngine engine) {
		this.fEngine = engine;
	}
	
	/**
	 * Returns the keyboard state
	 * @return the keyboard state
	 */
	protected final KeyboardState getKeyboardState() {
		return fKeyboardState;
	}
	/**
	 * Updates the KeyboadState instance
	 * @param keyboard the new KeyboardState instance
	 */
	public final void setKeyboardState(KeyboardState keyboard) {
		this.fKeyboardState = keyboard;
	}
	
	/**
	 * Returns the mouse state
	 * @return the mouse state
	 */
	protected final MouseState getMouseState() {
		return fMouseState;
	}
	/**
	 * Updates the MouseState instance
	 * @param mouse the new MouseState instance
	 */
	public final void setMouseState(final MouseState mouse) {
		this.fMouseState = mouse;
	}
	
	/**
	 * Returns the frame boundaries
	 * @return the frame boundaries
	 */
	public final Rectangle getClientRect() {
		return getEngine().getClientRect();
	}
	
	/**
	 * Adds an ui to the ui-update and rendering queue
	 * @param ui The ui to add
	 */
	public final void addUi(final UI ui) {
		this.fEngine.getUiManager().addUiToRenderingQueue(ui);
	}
	
	/** Called before essential engine objects are being initiated */
	public abstract void init();
	/** Called when engine loaded all needed objects and is ready to run */
	public abstract void load();
	/** Called when the engine is about to stop and dispose */
	public abstract void unload();
	
	// Functions to be overwritten
	/**
	 * Called when the engine has finished initializing
	 */
	public void postinit() {}
	/**
	 * Called before the game renders
	 * @param context the graphics context
	 */
	public void prerender(final Graphics2D context) {}
	/**
	 * Called after the engine rendered
	 * @param context the graphics context
	 */
	public void postrender(final Graphics2D context) {}
	/**
	 * Called after the frame is resized<br />
	 * Requires GameEngine.setResizable(true);
	 * @param newSize the new size of the frame
	 */
	public void resize(final SizeF newSize) {}
}
