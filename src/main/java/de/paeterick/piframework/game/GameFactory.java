package de.paeterick.piframework.game;

import de.paeterick.piframework.game.exceptions.EngineAlreadyInitializedException;
import de.paeterick.piframework.game.exceptions.EngineAlreadyStartedException;

/**
 * The GameFactory is used to create and start a new Game with less lines of code
 * 
 * @author Erquu
 */
public class GameFactory {
	/**
	 * Creates a new GameEngine instance with a given game content
	 * @param gameInstance The game content
	 * @return The GameEngine instance
	 */
	public static GameEngine create(final Game gameInstance) {
		return GameFactory.create(gameInstance, null);
	}
	/**
	 * Creates a new GameEngine instance with a given game content and present paramenters
	 * @param gameInstance The game content
	 * @param presentParameters The present parameters for the game engine to initialize
	 * @return The GameEngine instance
	 */
	public static GameEngine create(final Game gameInstance, final PresentParameters presentParameters) {
		final GameEngine engine = new GameEngine(presentParameters);
		try {
			engine.init(gameInstance);
		} catch (EngineAlreadyInitializedException e) {
			e.printStackTrace();
		}
		
		return engine;
	}

	/**
	 * Creates and starts a new GameEngine instance with a given game content
	 * @param gameInstance The game content
	 * @return The GameEngine instance
	 */
	public static GameEngine start(final Game gameInstance) {
		return GameFactory.start(gameInstance, null);
	}
	/**
	 * Creates and starts a new GameEngine instance with a given game content and present paramenters
	 * @param gameInstance The game content
	 * @param presentParameters The present parameters for the game engine to initialize
	 * @return The GameEngine instance
	 */
	public static GameEngine start(final Game gameInstance, final PresentParameters presentParameters) {
		final GameEngine engine = GameFactory.create(gameInstance, presentParameters);
		try {
			engine.start();
		} catch (EngineAlreadyStartedException e) {
			e.printStackTrace();
		}
		
		return engine;
	}
}
