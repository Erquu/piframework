package de.paeterick.piframework.game;

/**
 * The present parameters are used to change the default rendering settings used by the engine
 * @author Erquu
 */
public class PresentParameters {
	
	/** The count of the Buffer Strategies back buffers */
	private int fBackbufferCount = 3;
	/** The target for the engine to render on */
	private IRenderingTarget fRenderingTarget = null;

	/**
	 * Initializes with the default settings
	 */
	public PresentParameters() { }
	/**
	 * Copies the present parameters fields into this instance
	 * @param p The present parameters to copy
	 */
	public PresentParameters(final PresentParameters p) {
		setBackbufferCount(p.getBackBufferCount());
		setRenderingTarget(p.getRenderingTarget());
	}

	/**
	 * Returns the set backbuffer count
	 * @return The set backbuffer count
	 */
	public int getBackBufferCount() {
		return fBackbufferCount;
	}

	/**
	 * Changes the backbuffer count
	 * @param fBackBufferCount the new number of back buffers
	 */
	public void setBackbufferCount(final int fBackBufferCount) {
		this.fBackbufferCount = fBackBufferCount;
	}

	/**
	 * Returns the rendering target
	 * @return The rendering target
	 */
	public IRenderingTarget getRenderingTarget() {
		return fRenderingTarget;
	}

	/**
	 * Changes the rendering target
	 * @param fRenderingTarget The new rendering target
	 */
	public void setRenderingTarget(final IRenderingTarget fRenderingTarget) {
		this.fRenderingTarget = fRenderingTarget;
	}

}
