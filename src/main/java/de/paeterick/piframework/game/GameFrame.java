package de.paeterick.piframework.game;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;


/**
 * The basic frame of a game
 * @author Erquu
 */
public class GameFrame extends JFrame implements WindowListener {
	private static final long serialVersionUID = 3265222156297583416L;
	
	/** The boundaries of the frame */
	private Rectangle clientRect;
	/** Indicator showing if frame is fully loaded */
	private boolean loaded = false;
	
	private final GameCanvas canvas = new GameCanvas();
	
	/**
	 * Returns the boundaries of the frame
	 * @return the boundaries of the frame
	 */
	public Rectangle getClientRect() {
		return clientRect;
	}
	
	/**
	 * Returns if the frame is ready
	 * @return frame is ready and fully loaded
	 */
	public boolean ready() {
		return loaded;
	}
	
	/**
	 * Initializes the frame
	 */
	public GameFrame() {
		
		this.addWindowListener(this);
		
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				updateClientRect(((JFrame)e.getComponent()).getContentPane());
				super.componentResized(e);
			}
		});
		
		this.getContentPane().add(canvas);
		
		final BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		final Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
		this.getContentPane().setCursor(blankCursor);
	}
	
	private void updateClientRect(final Container container) {
		final Rectangle tmpRect = container.getBounds();
		if (tmpRect != null) {
			final Point location = getLocationOnFrame(container);
			if (location != null) {
				tmpRect.translate(location.x, location.y);
				clientRect = tmpRect;
			}
		}
	}
	
	private Point getLocationOnFrame(final Container container) {
		Point componentLocation = null;
		
		if (container != null) {
			try {
				componentLocation = container.getLocationOnScreen();
				final Point frameLocation = this.getLocationOnScreen();
				componentLocation.translate(-frameLocation.x, -frameLocation.y);
				//System.out.println(componentLocation);
			} catch (IllegalStateException ex) {
				
			}
		}
		
		return componentLocation;
	}

	public void windowActivated(final WindowEvent e) {
		canvas.requestFocusInWindow();
	}

	public void windowClosed(final WindowEvent e) {}
	public void windowClosing(final WindowEvent e) {}
	public void windowDeactivated(final WindowEvent e) {}
	public void windowDeiconified(final WindowEvent e) {}
	public void windowIconified(final WindowEvent e) {}

	public void windowOpened(final WindowEvent e) {
		System.out.println("WindowOpened");
		updateClientRect(((JFrame)e.getComponent()).getContentPane());
		System.out.println(getClientRect());
		
		loaded = true;
	}

	/**
	 * Returns the rendering target for the engine to draw onto
	 * @return The rendering target for the engine to draw onto
	 */
	public IRenderingTarget getRenderingTarget() {
		//return this.getContentPane();
		return canvas;
	}
}
