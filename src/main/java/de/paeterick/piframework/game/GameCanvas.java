package de.paeterick.piframework.game;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * The GameCanvas is used to create a component that can be put in a custom swing container<br />
 * and the engine can still draw onto
 * 
 * @author Erquu
 */
public class GameCanvas extends Canvas implements IRenderingTarget, ComponentListener {
	private static final long serialVersionUID = -1786778613242939634L;
	
	/** The canvas boundaries */
	private final Rectangle clientRect = new Rectangle(0, 0, 0, 0);
	
	public GameCanvas() {
		this.addComponentListener(this);
		componentResized(null);
	}

	public Component getRenderingComponent() {
		return this;
	}

	public Rectangle getClientRect() {
		return clientRect;
	}

	public boolean ready() {
		return this.isEnabled();
	}

	public void dispose() {
		
	}

	public void componentHidden(final ComponentEvent arg0) { }
	public void componentMoved(final ComponentEvent arg0) { }

	public void componentResized(final ComponentEvent arg0) {
		clientRect.width = getWidth();
		clientRect.height = getHeight();
	}

	public void componentShown(final ComponentEvent arg0) { }
}
