package de.paeterick.piframework.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import de.paeterick.piframework.game.console.ConsoleActionArguments;
import de.paeterick.piframework.game.console.GameConsole;
import de.paeterick.piframework.game.console.IConsoleAction;
import de.paeterick.piframework.game.exceptions.EngineAlreadyInitializedException;
import de.paeterick.piframework.game.exceptions.EngineAlreadyStartedException;
import de.paeterick.piframework.game.graphics.Scene;
import de.paeterick.piframework.hid.KeyboardState;
import de.paeterick.piframework.hid.MouseState;
import de.paeterick.piframework.logic.UpdateListener;
import de.paeterick.piframework.logic.UpdateLoop;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.timing.NanoTimer;
import de.paeterick.piframework.timing.Timer;
import de.paeterick.piframework.timing.UpsCounter;
import de.paeterick.piframework.ui.UIManager;

/**
 * The main engine 
 * @author Erquu
 */
public class GameEngine implements MouseListener, KeyListener, UpdateListener, Runnable, IConsoleAction {
	
	/** Static instance of the created GameEngine */
	private static GameEngine staticInstance = null;
	
	/**
	 * Returns the instance of the used GameEngine
	 * @return The instance of the used GameEngine
	 */
	public static GameEngine getInstance() {
		return staticInstance;
	}
	
	private enum LogLevel {
		LOW, HIGH
	}
	
	/** True if the Engine has a custom component to render to, no frame will be created if true */
	private boolean fCustomeFrame = false;
	/** The rendering target, will be set automatically if null when starting the engine */
	private IRenderingTarget fRenderingTarget = null;
	/** The PresentParameters for this instance */
	private PresentParameters fPresentParameters = null;
	/** The name of the game */
	private String fGameName = "";
	/** The frame of the game */
	private GameFrame fGameFrame;
	/** Manager for the user interface */
	private UIManager fUiManager;
	/** The scene of the game */
	private Scene fGameScene;
	/** Ensures double/triple buffering */
	private BufferStrategy fBufferStrategy;
	/** True if the Engine has been initialized successfully */
	private boolean fEngineInitialized = false;
	/** True if the Engine has been started successfully */
	private boolean fEngineStarted = false;
	
	/** The Game Console */
	private GameConsole fGameConsole;
	
	/** The main update loop */
	private UpdateLoop fUpdateLoop;
	/** The renering loop counter */
	private UpsCounter fFpsCounter;
	/** The update loop counter */
	private UpsCounter fUpsCounter;
	
	//
	// Rhino js scripting
	//
	public Context fJsContext;
	public Scriptable fJsScope;
	
	//
	// Time log fields
	//
	private LogLevel fLogLevel = LogLevel.LOW;
	
	/** Timer for general tasks */
	private final Timer fTimer = new Timer();
	private final Timer fNanoTimer = new NanoTimer();
	
	private long fUpsCounterTime = 0;
	private long fMouseUpdateTime = 0;
	private long fInstanceUpdateTime = 0;
	private long fUiUpdateTime = 0;
	private long fPreRenderTime = 0;
	private long fPreRenderEngineTime = 0;
	private long fPreRenderInstanceTime = 0;
	private long fRenderTime = 0;
	private long fPostRenderTime = 0;
	private long fPostRenderDebugTime = 0;
	private long fPostRenderEngineTime = 0;
	private long fPostRenderInstanceTime = 0;
	private long fPostRenderConsoleTime = 0;
	private long fPostRenderUiTime = 0;
	
	private long fRenderCleanup = 0;
	
	/** The instance of the game */
	private Game fGameInstance;
	/** Game instance finished loading */
	private boolean fInstanceLoaded = false;
	
	/** The current mouse state */
	private MouseState fMouseState;
	/** The current keyboard state */
	private KeyboardState fKeyboardState;
	
	/** indicator for double buffering */
	private boolean fAntiAliaseEnabled = true;
	/** GameFrame is resizable or not */
	private boolean fResizable = false;
	/** Showing debug messages is allowed or not */
	private boolean fAllowDebug = true;
	/** Debug messages are shown or not */
	private boolean fDebug = false;
	
	/** Debugging only, if this is set to true, update will be called by pressing a key */
	private boolean fStepByStepUpdate = false;
	private boolean fMakeNextStep = false;
	
	/** If true, the update loop will take care of the render timing */
	private boolean fSynchronizedRendering = true;
	/** Holds all string to be printed when debug mode is enabled */
	private List<String> fDebugStrings;

	public GameEngine() {
		this(null);
	}
	public GameEngine(final PresentParameters presentParameters) {
		GameEngine.staticInstance = this;
		
		if (presentParameters != null) {
			this.fPresentParameters = presentParameters;
			final IRenderingTarget renderingTarget = presentParameters.getRenderingTarget();
			if (renderingTarget != null) {
				this.fRenderingTarget = renderingTarget;
				this.fCustomeFrame = true;
			}
		} else {
			this.fPresentParameters = new PresentParameters();
		}
	}
	
	/**
	 * Sets the name of the game/frame
	 * @param name the name of the game/frame
	 */
	public void setGameName(final String name) {
		fGameName = name;
		
		if (fGameFrame != null)
			fGameFrame.setTitle(name);
	}
	
	/**
	 * Shows or hides the debug mode
	 * @param on true to show
	 */
	public void showDebugOuput(final boolean show) {
		this.fDebug = (this.fAllowDebug && show);
		//this.fGameConsole.setEnabled(on);
	}
	
	/**
	 * Allows or disallows the debug output<br />
	 * If true, the debug mode can be toggled by the console<br />
	 * <br />
	 * If the debug mode is disallowed, it will be hidden directly
	 * @param allow True to allow showing the debug output
	 */
	public void allowDebugMode(final boolean allow) {
		this.fAllowDebug = allow;
		this.fDebug = (allow && this.fDebug);
	}
	
	/**
	 * Enables or disables the console
	 * @param enable True to enable console
	 */
	public void enableConsole(final boolean enable) {
		this.fGameConsole.setEnabled(enable);
	}
	
	/**
	 * Adds a String to be debugged<br />
	 * All added strings will be cleared after each update loop
	 * @param s the string to be added
	 */
	public void addDebugString(final String s) {
		//synchronized (fDebugStrings) {
		if (this.fAllowDebug) {
			this.fDebugStrings.add(s);
		}
		//}
	}
	/**
	 * Adds a String to be debugged<br />
	 * All added strings will be cleared after each update loop
	 * @param format A format string
	 * @param args Arguments referenced by the format specifiers in the format string. If there are more arguments than format specifiers, the extra arguments are ignored. The number of arguments is variable and may be zero. The maximum number of arguments is limited by the maximum dimension of a Java array as defined by The Java� Virtual Machine Specification. The behaviour on a null argument depends on the conversion.
	 */
	public void addDebugString(final String format, final Object... args) {
		addDebugString(String.format(format, args));
	}
	
	/**
	 * Returns the game console instance
	 * @return The game console instance
	 */
	public GameConsole getConsole() {
		return this.fGameConsole;
	}
	
	/**
	 * Returns the ui manager
	 * @return The ui manager
	 */
	public UIManager getUiManager() {
		return this.fUiManager;
	}
	
	/**
	 * Enables or disables anti aliasing
	 * @param enable true if anti aliasing should be enabled
	 */
	public void enableAntiAlias(final boolean enable) {
		this.fAntiAliaseEnabled = enable;
	}
	
	/**
	 * Enables or disables synchronized rendering<br />
	 * When enabled, the engine will always render after a game update
	 * @param enable True to enable synchronized rendering
	 */
	public void enableSynchronizedRendering(final boolean enable) {
		fSynchronizedRendering = enable;
	}
	
	//TODO: This should not be in the engine
	/**
	 * Used to set the GameFrame resizable
	 * @param resizable true if frame should be resizable
	 */
	public void setResizable(final boolean resizable) {
		this.fResizable = resizable;
		if (this.fGameFrame != null) {
			this.fGameFrame.setResizable(resizable);
		}
	}
	
	//TODO: This should not be in the engine
	/**
	 * Returns the frame boundaries
	 * @return the frame boundaries
	 */
	public Rectangle getClientRect() {
		return this.fRenderingTarget.getClientRect();
	}
	
	/**
	 * Returns the game scene
	 * @return The game scene
	 */
	public Scene getScene() {
		return this.fGameScene;
	}

	// TODO: REMOVE, JUST A TEST!
	public Object evaluateScript(final String source) {
		return this.fJsContext.evaluateString(fJsScope, source, "<cmd>", 1, null);
	}
	
	/**
	 * Initializes the engine
	 * @param gameinstance the game to be run
	 * @throws EngineAlreadyInitializedException 
	 */
	public void init(final Game gameinstance) throws EngineAlreadyInitializedException {
		if (!fEngineInitialized) {
			final long loadingStartTime = System.currentTimeMillis();
			
			this.fGameInstance = gameinstance;
			this.fGameInstance.setEngine(this);
			
			this.fUiManager = new UIManager();
			
			this.fJsContext = Context.enter();
			this.fJsScope = this.fJsContext.initStandardObjects();
	
			this.fGameConsole = new GameConsole();
			this.fGameConsole.setEnabled(true);
			this.fGameConsole.addAction("exit", this);
			this.fGameConsole.addAction("en", this);
			
			this.fGameInstance.init();
			
			this.fDebugStrings = new Vector<String>();
			
			this.fFpsCounter = new UpsCounter();
			this.fUpsCounter = new UpsCounter();
			this.fUpdateLoop = new UpdateLoop();
			
			this.fUpdateLoop.addUpdateEventListener(this);
			this.fUpdateLoop.addRenderEventListener(this);
			
			if (!this.fCustomeFrame) {
				this.fGameFrame = new GameFrame();
				this.fGameFrame.setTitle(this.fGameName);
				this.fRenderingTarget = this.fGameFrame.getRenderingTarget();
			}
			// Enable Tab key event
			this.fRenderingTarget.getRenderingComponent().setFocusTraversalKeysEnabled(false);
			this.fRenderingTarget.getRenderingComponent().addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					if (fRenderingTarget.ready() && fEngineStarted && fInstanceLoaded) {
						fGameInstance.resize(new SizeF(e.getComponent().getWidth(), e.getComponent().getHeight()));
						fUiManager.updateViewport(fGameScene.viewport);
					}
				}
			});

			this.fRenderingTarget.getRenderingComponent().addKeyListener(this);
			this.fRenderingTarget.getRenderingComponent().addMouseListener(this);
			this.fGameConsole.setRenderingTarget(this.fRenderingTarget);
			
			if (!this.fCustomeFrame) {
				final Dimension size = new Dimension(1000, 800);
				this.fGameFrame.setMinimumSize(size);
				this.fGameFrame.setPreferredSize(size);
				this.fGameFrame.setMaximumSize(size);
				this.fGameFrame.setResizable(this.fResizable);
			
				try {
					SwingUtilities.invokeAndWait(this);
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			
			final Rectangle worldSize = new Rectangle(0, 0, this.fGameFrame.getWidth(), this.fGameFrame.getHeight());
			this.fGameScene = new Scene(worldSize);
			
			this.fGameInstance.postinit();
			
			fEngineInitialized = true;
	
			long loadingEndTime = System.currentTimeMillis();
			this.fGameConsole.addMessage("Time needed to initialize: %sms", (loadingEndTime - loadingStartTime));
		} else {
			throw new EngineAlreadyInitializedException("The engine has already been initialized!");
		}
	}
	
	/**
	 * Starts the engine
	 * @throws EngineAlreadyStartedException 
	 */
	public void start() throws EngineAlreadyStartedException {
		if (!fEngineStarted) {
			while (!this.fRenderingTarget.ready()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			this.fKeyboardState = new KeyboardState(this.fRenderingTarget.getRenderingComponent());
			this.fGameInstance.setKeyboardState(this.fKeyboardState);
			this.fMouseState = new MouseState(this.fRenderingTarget.getRenderingComponent());
			this.fGameInstance.setMouseState(this.fMouseState);

			this.fUiManager.updateViewport(this.fGameScene.viewport);
	
			final Thread loadingThread = new Thread(new Runnable() {
				public void run() {
					long loadingStartTime = System.currentTimeMillis();
					fGameInstance.load();
					fInstanceLoaded = true;
					long loadingEndTime = System.currentTimeMillis();
					fGameConsole.addMessage("Time needed for game to load: %sms", (loadingEndTime - loadingStartTime));
				}
			});
			loadingThread.start();
	
			this.fRenderingTarget.createBufferStrategy(this.fPresentParameters.getBackBufferCount());
			this.fBufferStrategy = this.fRenderingTarget.getBufferStrategy();
			
			this.fUpdateLoop.Start(60);
			
			this.fEngineStarted = true;
			
			if (!this.fSynchronizedRendering) {
				// TODO: Move into new Thread
				while (this.fRenderingTarget.isEnabled()) {
					render();
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
						break;
					}
				}
			}
		} else {
			throw new EngineAlreadyStartedException("The engine has already been started!");
		}
	}
	
	/**
	 * Stops the engine
	 */
	public void stop() {
		this.fUpdateLoop.Stop();
		this.fGameInstance.unload();
		
		Context.exit();
		
		if (!fCustomeFrame)
			this.fGameFrame.setEnabled(false);
		
		this.fRenderingTarget.dispose();
	}
	
	/**
	 * Called after each update
	 */
	public void update(final long deltaTime, final long totalTime) {
		if (!this.fStepByStepUpdate || this.fMakeNextStep) {
			if (this.fDebug) {
				synchronized (this.fDebugStrings) {
					this.fDebugStrings.clear();
				}
				addDebugString("FPS: %s", this.fFpsCounter.getUPS());
				addDebugString("UPS: %s", this.fUpsCounter.getUPS());
			}
	
			if (this.fLogLevel == LogLevel.HIGH) {
				addDebugString("Update:");
				addDebugString("  FPS Counter: %sns", this.fUpsCounterTime);
				addDebugString("  Mouse Update: %sns", this.fMouseUpdateTime);
				addDebugString("  UI Update: %sms", this.fUiUpdateTime);
				addDebugString("  Game Update: %sms", this.fInstanceUpdateTime);
				addDebugString("Render:");
				addDebugString("  Prerender: %sms", this.fPreRenderTime);
				addDebugString("    Engine: %sns", this.fPreRenderEngineTime);
				addDebugString("    Instance: %sns", this.fPreRenderInstanceTime);
				addDebugString("  Render: %sms", this.fRenderTime);
				addDebugString("  Postrender: %sms", this.fPostRenderTime);
				addDebugString("    Debug: %sns", this.fPostRenderDebugTime);
				addDebugString("    Engine: %sns", this.fPostRenderEngineTime);
				addDebugString("    Console: %sns", this.fPostRenderConsoleTime);
				addDebugString("    Ui: %sns", this.fPostRenderUiTime);
				addDebugString("    Instance: %sns", this.fPostRenderInstanceTime);
				addDebugString("  Cleanup: %sms", this.fRenderCleanup);
				addDebugString("-");
			}
	
			this.fNanoTimer.start();
			this.fUpsCounter.update(totalTime);
			this.fUpsCounterTime = this.fNanoTimer.stop();
			
			this.fNanoTimer.start();
			this.fMouseState.update(deltaTime, totalTime);
			this.fMouseUpdateTime = this.fNanoTimer.stop();
			
			this.fNanoTimer.start();
			this.fUiManager.update(deltaTime, totalTime);
			this.fUiUpdateTime = this.fNanoTimer.stop();
			
			if (this.fInstanceLoaded && !this.fUiManager.isBlocking()) {
				this.fTimer.start();
				this.fGameInstance.update(deltaTime, totalTime);
				this.fMakeNextStep = false;
				this.fInstanceUpdateTime = this.fTimer.stop();
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.logic.UpdateListener#render(long, long)
	 */
	public void render(final long deltaTime, final long totalTime) {
		if (fSynchronizedRendering)
			render();
	}
	
	/**
	 * Called to render game
	 */
	public void render() {
		do {
			do {
				final Graphics2D context = (Graphics2D)this.fBufferStrategy.getDrawGraphics();
					
				if (fAntiAliaseEnabled) {
					context.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				} else {
					context.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
				}
				
				fTimer.start();
				prerender(context);
				fPreRenderTime = fTimer.stop();
				
				if (fInstanceLoaded) {
					fTimer.start();
					this.fGameInstance.render(context, this.fGameScene);
					fRenderTime = fTimer.stop();
				}
				
				fTimer.start();
				postrender(context);
				fPostRenderTime = fTimer.stop();
				

				fTimer.start();
				Toolkit.getDefaultToolkit().sync();
				context.dispose();
				fRenderCleanup = fTimer.stop();
				
			} while (this.fBufferStrategy.contentsRestored());
				
			this.fBufferStrategy.show();
		} while (this.fBufferStrategy.contentsLost());
		
		this.fFpsCounter.update(System.currentTimeMillis()); //TODO: Create renderer to get time
	}
	
	/**
	 * Will be called before the game rendering function
	 * @param context the 2D context to be drawn in
	 */
	private void prerender(final Graphics2D context) {
		fNanoTimer.start();
		context.translate(getClientRect().x, getClientRect().y);
		
		context.setBackground(Color.black);
		context.clearRect(0, 0, getClientRect().width, getClientRect().height);
		fPreRenderEngineTime = fNanoTimer.stop();

		if (fInstanceLoaded) {
			fNanoTimer.start();
			fGameInstance.prerender(context);
			fPreRenderInstanceTime = fNanoTimer.stop();
		}
	}
	
	/**
	 * Will be called after the game rendering function
	 * @param context the 2D context to be drawn in
	 */
	private void postrender(final Graphics2D context) {
		//
		// RENDER DEBUG INFO
		//
		if (this.fAllowDebug && this.fDebug) {
			fNanoTimer.start();
			context.setColor(Color.white);
			int y = 20;
			String[] debugStrings;
			
			synchronized (fDebugStrings) {
				debugStrings = new String[fDebugStrings.size()];
				fDebugStrings.toArray(debugStrings);
			}
			
			for (String debugString : debugStrings) {
				context.drawString(debugString, 10, y);
				y += 20;
			}
			
			// Draw coords
			context.setColor(Color.green);
			context.drawLine(5, getClientRect().height - 25, 25, getClientRect().height - 25);
			context.drawLine(23, getClientRect().height - 23, 25, getClientRect().height - 25);
			context.drawLine(23, getClientRect().height - 27, 25, getClientRect().height - 25);
			context.drawString("x", 20, getClientRect().height - 13);
			context.setColor(Color.red);
			context.drawLine(5, getClientRect().height - 25, 5, getClientRect().height - 5);
			context.drawLine(3, getClientRect().height - 7, 5, getClientRect().height - 5);
			context.drawLine(7, getClientRect().height - 7, 5, getClientRect().height - 5);
			context.drawString("y", 8, getClientRect().height - 5);
			
			fPostRenderDebugTime = fNanoTimer.stop();
		}
		
		//
		// RENDER UI
		//
		fNanoTimer.start();
		this.fUiManager.render(context, this.fGameScene);
		fPostRenderUiTime = fNanoTimer.stop();
		
		//
		// RENDER CONSOLE
		//
		fNanoTimer.start();
		fGameConsole.render(context, this.fGameScene);
		fPostRenderConsoleTime = fNanoTimer.stop();

		//
		// POSTRENDER GAME INSTANCE
		//
		if (fInstanceLoaded) {
			fNanoTimer.start();
			fGameInstance.postrender(context);
			fPostRenderInstanceTime = fNanoTimer.stop();
		}
		
		//
		// TRANSLATE WORLD
		//
		fNanoTimer.start();
		context.translate(-getClientRect().x, -getClientRect().y);
		fPostRenderEngineTime = fNanoTimer.stop();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		this.fGameFrame.setIgnoreRepaint(true);
		this.fGameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.fGameFrame.setEnabled(true);
		this.fGameFrame.setVisible(true);
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.paeterick.piframework.game.console.IConsoleAction#doAction(de.paeterick.piframework.game.console.ConsoleActionArguments)
	 */
	public void doAction(final ConsoleActionArguments args) throws Exception {
		final String command = args.getCommand();
		final String[] parameters = args.getArgs();
		
		if ("exit".equals(command)) {
			this.fGameConsole.setActive(false);
		} else if ("en".equals(command)) {
			if ("debug".equals(args.getArg(0))) {
				if ("level".equals(args.getArg(1))) {
					if (args.size() > 2) {
						fLogLevel = parameters[2].equalsIgnoreCase("high") ? LogLevel.HIGH : LogLevel.LOW;
					} else {
						fLogLevel = LogLevel.LOW;
					}
					fGameConsole.addMessage("Setting log level to [^7]%s", fLogLevel.toString());
					this.showDebugOuput(true);
				} else {
					this.showDebugOuput((args.size() > 1 ? Boolean.parseBoolean(args.getArg(1)) : true));
				}
			} else if ("step".equals(args.getArg(0))) {
				fStepByStepUpdate = (args.size() > 1 ? Boolean.parseBoolean(args.getArg(1)) : true);
			}
		}
	}
	
	public void keyTyped(final KeyEvent e) {
		this.fUiManager.onKeyUp(e.getKeyCode());
		this.fGameConsole.keyTyped(e);
	}
	
	public void keyPressed(final KeyEvent e) {
		this.fUiManager.onKeyDown(e.getKeyCode());
		this.fGameConsole.keyPressed(e);
		
		if (this.fStepByStepUpdate)
			fMakeNextStep = true;
	}
	
	public void keyReleased(final KeyEvent e) {
		this.fUiManager.onKeyUp(e.getKeyCode());
		this.fGameConsole.keyReleased(e);
	}
	
	public void mouseClicked(final MouseEvent e) {
		this.fUiManager.onMouseClick(e);
	}
	
	public void mousePressed(final MouseEvent e) {
		this.fUiManager.onMouseDown(e);
	}
	
	public void mouseReleased(final MouseEvent e) {
		this.fUiManager.onMouseUp(e);
	}
	
	public void mouseEntered(final MouseEvent e) { }
	public void mouseExited(final MouseEvent e) { }
}
