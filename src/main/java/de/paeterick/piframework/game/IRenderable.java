package de.paeterick.piframework.game;

import java.awt.Graphics2D;

import de.paeterick.piframework.game.graphics.Scene;

/**
 * Interface for Game objects that can be rendered
 * @author Erquu
 */
public interface IRenderable {
	/**
	 * Called after each iteration of the drawing loop
	 * @param context The 2D context to draw in
	 * @param scene The 2D scene
	 */
	void render(final Graphics2D context, final Scene scene);
}
