package de.paeterick.piframework.timing;

import java.awt.Graphics2D;
import java.awt.font.LineMetrics;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.paeterick.piframework.game.IRenderable;
import de.paeterick.piframework.game.graphics.Scene;

public class UpsRenderer implements IRenderable {

	private final Map<String, UpsCounter> counter = new HashMap<String, UpsCounter>();
	
	public void addCounter(final String name, final UpsCounter counter) {
		this.counter.put(name, counter);
	}

	public void render(final Graphics2D context, final Scene scene) {
		final LineMetrics m = context.getFontMetrics(context.getFont()).getLineMetrics("ABC", context);
		
		float offset = 10.0f;
		
		Iterator<String> keys = counter.keySet().iterator();
		while (keys.hasNext()) {
			final String name = keys.next();
			final int ups = counter.get(name).getUPS();
			
			context.drawString(String.format("%s: %s", name, ups), 10.0f, offset);
			offset += m.getHeight();
		}
	}

}
