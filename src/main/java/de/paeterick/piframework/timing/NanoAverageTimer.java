/**
 * 
 */
package de.paeterick.piframework.timing;

/**
 * @author csp8fe
 *
 */
public class NanoAverageTimer extends NanoTimer {
	private final long[] fLastValues;
	private long fAverageTime = 0;
	
	public NanoAverageTimer() {
		fLastValues = new long[50];
	}
	/**
	 * Initiates a new NanoAverageTimer
	 * @param saveValueLength the amount of data that should be saved
	 */
	public NanoAverageTimer(final int saveValueLength) {
		fLastValues = new long[saveValueLength];
	}
	
	@Override
	public long stop() {
		final long value = super.stop();
		for (int i = 1; i < fLastValues.length; i++) {
			fLastValues[i-1] = fLastValues[i];
		}
		fLastValues[fLastValues.length-1] = value;
		compute();
		return value;
	}
	
	@Override
	public long getTime() {
		return fAverageTime;
	}
	
	private void compute() {
		long value = 0;
		for (int i = 0; i < fLastValues.length; i++) {
			value += fLastValues[i];
		}
		fAverageTime = (value / fLastValues.length);
	}
}
