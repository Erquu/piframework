package de.paeterick.piframework.timing;

import de.paeterick.piframework.logic.UpdateLoop;

/**
 * Counter for Updates per Second
 * @author Erquu
 */
public class UpsCounter {
	/** The amount of ups from the last run */
	private int fUps;
	/** The amound of updates since last second */
	private int fUpsCount;
	/** The last update time stamp */
	private long fLastUpdate;
	
	/**
	 * Returns the counted Updates within the last second 
	 * @return the counted Updates within the last second
	 */
	public int getUPS() {
		return fUps;
	}
	
	/**
	 * Updates the counter
	 * @param currentTime the current time in ms
	 * @see UpdateLoop.TIMER_RESOLUTION
	 */
	public void update(long currentTime) {
		fUpsCount++;
		if ((fLastUpdate + UpdateLoop.TIMER_RESOLUTION) < currentTime) {
			fUps = fUpsCount;
			fUpsCount = 0;
			fLastUpdate = currentTime;
		}
	}
}
