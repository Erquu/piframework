/**
 * 
 */
package de.paeterick.piframework.timing;

/**
 * Counts the time difference between a start time and an end time<br />
 * Resolution is in nano seconds
 * @author csp8fe
 */
public class NanoTimer extends Timer {
	@Override
	protected long getCurrentTime() {
		return System.nanoTime();
	}
}
