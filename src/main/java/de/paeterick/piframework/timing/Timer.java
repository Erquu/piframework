package de.paeterick.piframework.timing;

import de.paeterick.piframework.logic.UpdateLoop;

/**
 * Counts the time difference between a start time and an end time
 * @author Erquu
 */
public class Timer {
	/** The timestamp for the last start function call */
	private long fStartTime = 0;
	/** Stores the last needed time */
	private long fNeededTime = -1;
	
	/**
	 * Provides the current time for the start and stop method
	 * @return the current time;
	 */
	protected long getCurrentTime() {
		return UpdateLoop.getCurrentTime();
	}
	
	/**
	 * Starts the timer
	 * @param currentTime the current time
	 */
	public void start() {
		fStartTime = getCurrentTime();
	}
	
	/**
	 * Stops the timer
	 * @param currentTime the current time
	 * @return the time difference from start to stop
	 */
	public long stop() {
		return (fNeededTime = (getCurrentTime() - fStartTime));
	}
	
	/**
	 * Returns the needed time<br />
	 * Returns -1 if start and stop were not called
	 * @return the needed time
	 */
	public long getTime() {
		return fNeededTime;
	}
}
