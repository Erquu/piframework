package de.paeterick.piframework.hid;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

public class KeyboardState {	
	
	private final Map<Integer, Boolean> fKeyStore = new HashMap<Integer, Boolean>();
	
	public KeyboardState(final Component component) {
		component.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				fKeyStore.put(e.getKeyCode(), true);
			}
			
			@Override
			public void keyReleased(final KeyEvent e) {
				fKeyStore.put(e.getKeyCode(), false);
			}
		});
	}
	
	public boolean isPressed(final int keyCode) {
		if (fKeyStore.containsKey(keyCode)) {
			return fKeyStore.get(keyCode).booleanValue();
		}
		return false;
	}
	
	public boolean isReleased(final int keyCode) {
		return (fKeyStore.containsKey(keyCode) && !fKeyStore.get(keyCode).booleanValue());
	}
}
