package de.paeterick.piframework.hid;

import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingUtilities;

import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.math.Vector2;

public class MouseState implements IUpdateable {
	private final Component component;
	private float x;
	private float y;
	private final boolean[] buttonState = new boolean[] { false, false, false };
	
	public void addMouseListener(final MouseListener mouseListener) {
		component.addMouseListener(mouseListener);
	}
	public void removeMouseListener(final MouseListener mouseListener) {
		component.removeMouseListener(mouseListener);
	}
	
	public MouseState(final Component component) {
		this.component = component;
		
		this.component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				int button = e.getButton();
				if (button > 0 && button < 4) {
					buttonState[button-1] = true;
				}
			}
			
			@Override
			public void mouseReleased(final MouseEvent e) {
				int button = e.getButton();
				if (button > 0 && button < 4) {
					buttonState[button-1] = false;
				}
			}
		});
	}
	
	public boolean isPressed(final int num) {
		return (num < buttonState.length && buttonState[num]);
	}
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public Vector2 getPosition() {
		return new Vector2(x, y);
	}

	public void update(final long deltaTime, final long totalTime) {
		PointerInfo a = MouseInfo.getPointerInfo();
		Point point = new Point(a.getLocation());
		SwingUtilities.convertPointFromScreen(point, component);
		x = point.x;
		y = point.y;
	}
}
